//
//  BCMomentsNewsCell.swift
//  BCB
//
//  Created by Zrocky on 2018/4/11.
//  Copyright © 2018年 zrocky. All rights reserved.
//

import UIKit
import SnapKit

class BCMomentsNewsCell: UITableViewCell {
    // MARK: - property
    var data: [String: Any]? {
        didSet {
            titleLabel.text = "腾讯区块链落地物流场景，与中物联签署战略合作协议"
            subtitleLabel.text = "3月15日 10:45  来自腾讯科技"
            contentLabel.text = "第三届全球物流技术大会上，腾讯公司与中国物流与采购联合会（以下简称“中物联”）签署了战略合作协议，并联合发布了双方首个重要合作项目——区块供应链联盟链及云单平台。"
            if (data?["BCOrder"] as? Int ?? 0) % 2 == 0  {
                illustration.isHidden = true
                toolBarTopConstraint?.deactivate()
                toolbar.snp.makeConstraints { (make) in
                    toolBarTopConstraint = make.top.equalTo(contentLabel.snp.bottom).offset(8).constraint
                }
            }else {
                illustration.isHidden = false
                toolBarTopConstraint?.deactivate()
                toolbar.snp.makeConstraints { (make) in
                    toolBarTopConstraint = make.top.equalTo(illustration.snp.bottom).offset(8).constraint
                }
            }
            illustration.setImage(url: "https://pbs.twimg.com/media/DafRlB3W4AAZonw.jpg")
            
        }
    }
    
    fileprivate var toolBarTopConstraint: Constraint?
    
    // MARK: - life cycle
    static func generateCell(tableView: UITableView) -> BCMomentsNewsCell {
        let reuseIdentifier = String(describing: self)
        var cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier) as? BCMomentsNewsCell
        if cell == nil {
            cell = BCMomentsNewsCell(style: UITableViewCellStyle.default, reuseIdentifier: reuseIdentifier)
        }
        assert(cell != nil, "cell can't be nil")
        
        return cell!
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupSubviews()
        setupLayouts()
        
        layoutIfNeeded()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - event response
    
    // MARK: - public methods
    
    // MARK: - private methods
    
    private func setupSubviews() {
        contentView.addSubview(separatorView)
        contentView.addSubview(titleLabel)
        contentView.addSubview(subtitleLabel)
        contentView.addSubview(contentLabel)
        contentView.addSubview(illustration)
        contentView.addSubview(lineView)
        contentView.addSubview(toolbar)
    }
    
    private func setupLayouts() {
        separatorView.snp.makeConstraints { (make) in
            make.top.leading.trailing.equalToSuperview()
            make.height.equalTo(8)
        }
        
        titleLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(16)
            make.trailing.equalTo(-16)
            make.top.equalTo(separatorView.snp.bottom).offset(10)
        }
        
        subtitleLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(16)
            make.trailing.equalTo(-16)
            make.top.equalTo(titleLabel.snp.bottom).offset(4)
        }
        
        contentLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(16)
            make.trailing.equalTo(-16)
            make.top.equalTo(subtitleLabel.snp.bottom).offset(8)
        }
        
        illustration.snp.makeConstraints { (make) in
            make.leading.equalTo(16)
            make.trailing.equalTo(-16)
            make.top.equalTo(contentLabel.snp.bottom).offset(6)
            make.height.equalTo(142)
        }
        
        lineView.snp.makeConstraints { (make) in
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(1/UIScreen.main.scale)
            make.bottom.equalTo(toolbar.snp.top)
        }
        
        toolbar.snp.makeConstraints { (make) in
            toolBarTopConstraint = make.top.equalTo(illustration.snp.bottom).offset(8).constraint
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(34)
            make.bottom.equalToSuperview().priority(900)
        }
    }
    
    // MARK: - UI property
    fileprivate lazy var separatorView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.background
        return view
    }()
    
    fileprivate lazy var titleLabel: UILabel = {
        let view = UILabel()
        view.textColor = UIColor.title
        view.font = UIFont.bold(size: 16)
        view.numberOfLines = 0
        return view
    }()
    
    fileprivate lazy var subtitleLabel: UILabel = {
        let view = UILabel()
        view.textColor = UIColor.subTitle
        view.font = UIFont.normal(size: 12)
        return view
    }()
    
    fileprivate lazy var contentLabel: UILabel = {
        let view = UILabel()
        view.textColor = UIColor.title
        view.font = UIFont.normal(size: 14)
        view.numberOfLines = 0
        return view
    }()
    
    fileprivate lazy var illustration: UIImageView = {
        let view = UIImageView()
        view.isHidden = true
        view.contentMode = UIViewContentMode.scaleAspectFill
        view.clipsToBounds = true
        return view
    }()
    
    fileprivate lazy var lineView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.background
        return view
    }()
    
    fileprivate lazy var toolbar: BCMomentsCellToolbar = {
        let view = BCMomentsCellToolbar()
        view.delegate = self
        return view
    }()
}

extension BCMomentsNewsCell: BCMomentsCellToolbarDelegate {
    func momentsCellToolbar(view: BCMomentsCellToolbar, didClick index: Int) {
        // TODO:
    }
}
