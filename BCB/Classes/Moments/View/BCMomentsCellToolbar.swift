//
//  BCMomentsCellToolbar.swift
//  BCB
//
//  Created by Zrocky on 2018/4/11.
//  Copyright © 2018年 zrocky. All rights reserved.
//

import UIKit

protocol BCMomentsCellToolbarDelegate: class {
    func momentsCellToolbar(view: BCMomentsCellToolbar, didClick index: Int)
}

class BCMomentsCellToolbar: UIView {
    // MARK: - property
    var delegate: BCMomentsCellToolbarDelegate?
    var data: [String: Any]? {
        didSet {
            commentBtn.setTitle("128", for: UIControlState.normal)
        }
    }
    
    // MARK: - life cycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupSubviews()
        setupLayouts()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - event response
    func btnClick(_ btn: UIButton) {
        delegate?.momentsCellToolbar(view: self, didClick: btn.tag)
    }
    
    // MARK: - public methods
    
    // MARK: - private methods
    private func setupSubviews() {
        addSubview(retweetBtn)
        addSubview(commentBtn)
        addSubview(likeBtn)
        addSubview(firstLine)
        addSubview(secondLine)
        
        setupBtn(view: retweetBtn, title: "转发", image: #imageLiteral(resourceName: "retweet"))
        setupBtn(view: commentBtn, title: "评论", image: #imageLiteral(resourceName: "comment"))
        setupBtn(view: likeBtn, title: "点赞", image: #imageLiteral(resourceName: "like"))
    }
    
    fileprivate func setupBtn(view: UIButton, title: String, image: UIImage) {
        view.addTarget(self, action: #selector(btnClick(_:)), for: UIControlEvents.touchUpInside)
        view.setTitle(title, for: UIControlState.normal)
        view.setTitleColor(UIColor.subTitle, for: UIControlState.normal)
        view.titleLabel?.font = UIFont.normal(size: 13)
        view.setImage(image, for: UIControlState.normal)
        view.imageView?.contentMode = UIViewContentMode.scaleAspectFit
        view.imageEdgeInsets = UIEdgeInsets(top: 9, left: 10, bottom: 9, right: 3)
    }
    
    private func setupLayouts() {
        retweetBtn.snp.makeConstraints { (make) in
            make.leading.equalToSuperview()
            make.top.bottom.equalToSuperview()
            make.width.equalToSuperview().multipliedBy(CGFloat(1/3.0))
        }
        
        commentBtn.snp.makeConstraints { (make) in
            make.leading.equalTo(retweetBtn.snp.trailing)
            make.width.equalTo(retweetBtn)
            make.top.bottom.equalToSuperview()
        }
        
        likeBtn.snp.makeConstraints { (make) in
            make.leading.equalTo(commentBtn.snp.trailing)
            make.width.equalTo(retweetBtn)
            make.top.bottom.equalToSuperview()
        }
        
        firstLine.snp.makeConstraints { (make) in
            make.centerX.equalTo(retweetBtn.snp.trailing)
            make.width.equalTo(1/UIScreen.main.scale)
            make.height.equalTo(34)
            make.centerY.equalToSuperview()
        }
        
        secondLine.snp.makeConstraints { (make) in
            make.centerX.equalTo(commentBtn.snp.trailing)
            make.width.equalTo(1)
            make.height.equalTo(34)
            make.centerY.equalToSuperview()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        
    }
    
    // MARK: - UI property
    fileprivate lazy var retweetBtn: UIButton = {
        let view = UIButton(type: UIButtonType.custom)
        view.tag = 0
        return view
    }()
    
    fileprivate lazy var commentBtn: UIButton = {
        let view = UIButton(type: UIButtonType.custom)
        view.tag = 1
        return view
    }()
    
    fileprivate lazy var likeBtn: UIButton = {
        let view = UIButton(type: UIButtonType.custom)
        view.tag = 2
        return view
    }()
    
    fileprivate lazy var firstLine: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.background
        return view
    }()
    
    fileprivate lazy var secondLine: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.background
        return view
    }()
}
