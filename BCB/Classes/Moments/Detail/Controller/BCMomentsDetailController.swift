//
//  BCMomentsDetailController.swift
//  BCB
//
//  Created by Zrocky on 2018/4/11.
//  Copyright © 2018年 zrocky. All rights reserved.
//

import UIKit

enum BCMomentsDetailControllerType {
    case discuss
    case news
}

class BCMomentsDetailController: UIViewController {
    // MARK: - property
    var type: BCMomentsDetailControllerType = .discuss
    
    // MARK: - life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        
        setupSubviews()
        setupLayouts()
        
        discussView.data = [:]
        newsView.data = [:]
        
        view.layoutIfNeeded()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setupNavi()
    }
    
    private func setupNavi() {
        navigationItem.title = "讨论"
    }
    
    // MARK: - event response
    
    // MARK: - public methods
    
    // MARK: - private methods
    private func setupSubviews() {
        view.addSubview(tableView)
        view.addSubview(toolBar)

        tableView.tableHeaderView?.layoutIfNeeded()
        if type == BCMomentsDetailControllerType.discuss {
            tableView.tableHeaderView = discussView
        }else if type == BCMomentsDetailControllerType.news {
            tableView.tableHeaderView = newsView
        }
    }
    
    private func setupLayouts() {
        tableView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }

        if type == BCMomentsDetailControllerType.discuss {
            discussView.snp.makeConstraints { (make) in
                make.width.equalTo(tableView)
            }
        }else if type == BCMomentsDetailControllerType.news {
            newsView.snp.makeConstraints { (make) in
                make.width.equalTo(tableView)
            }
        }
        
        
        toolBar.snp.makeConstraints { (make) in
            make.leading.trailing.equalToSuperview()
            make.bottom.equalToSuperview()
            make.height.equalTo(40)
        }
    
    }
    
    // MARK: - UI property
    fileprivate lazy var tableView: UITableView = {
        let view = UITableView()
        view.backgroundColor = UIColor.background
        view.rowHeight = UITableViewAutomaticDimension
        view.estimatedRowHeight = 44
        view.dataSource = self
        view.delegate = self
        return view
    }()
    
    fileprivate lazy var discussView: BCMomentsDetailDiscussView = {
        let view = BCMomentsDetailDiscussView()
        view.backgroundColor = UIColor.background
        return view
    }()
    
    fileprivate lazy var newsView: BCMomentsDetailNewsView = {
        let view = BCMomentsDetailNewsView()
        view.backgroundColor = UIColor.background
        return view
    }()
    
    fileprivate lazy var toolBar: BCMomentsCellToolbar = {
        let view = BCMomentsCellToolbar()
        view.backgroundColor = UIColor.white
        return view
    }()
}

// MARK: - UITableViewDataSource, UITableViewDelegate
extension BCMomentsDetailController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 20
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = BCMomentsDetailCommentCell.generateCell(tableView: tableView)
        cell.data = [:]
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = BCMomentsDetailHeaderView()
        view.data = [:]
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 34
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}
