//
//  BCMomentsDetailHeaderView.swift
//  BCB
//
//  Created by Zrocky on 2018/4/12.
//  Copyright © 2018年 zrocky. All rights reserved.
//

import UIKit

class BCMomentsDetailHeaderView: UIView {
    // MARK: - property
    var data: [String: Any]? {
        didSet {
            discussLabel.text = "评论 12"
            retweetLabel.text = "转发 24"
        }
    }
    
    // MARK: - life cycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.white
        
        setupSubviews()
        setupLayouts()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - event response
    func sortBtnClick() {
        
    }
    
    // MARK: - public methods
    
    // MARK: - private methods
    private func setupSubviews() {
        addSubview(discussLabel)
        addSubview(retweetLabel)
        addSubview(sortBtn)
        addSubview(lineView)
    }
    
    private func setupLayouts() {
        discussLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(16)
            make.centerY.equalToSuperview()
        }
        
        retweetLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(discussLabel.snp.trailing).offset(18)
            make.centerY.equalToSuperview()
        }
        
        sortBtn.snp.makeConstraints { (make) in
            make.trailing.equalTo(-16)
            make.centerY.equalToSuperview()
            make.width.equalTo(50)
            make.height.equalTo(20)
        }
        
        lineView.snp.makeConstraints { (make) in
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(1/UIScreen.main.scale)
            make.bottom.equalToSuperview()
        }
    }
    
    // MARK: - UI property
    fileprivate lazy var discussLabel: UILabel = {
        let view = UILabel()
        view.textColor = UIColor.title
        view.font = UIFont.bold(size: 14)
        return view
    }()
    
    fileprivate lazy var retweetLabel: UILabel = {
        let view = UILabel()
        view.textColor = UIColor.title
        view.font = UIFont.bold(size: 14)
        return view
    }()
    
    fileprivate lazy var sortBtn: BCColumnNameViewButton = {
        let view = BCColumnNameViewButton(type: UIButtonType.custom)
        view.addTarget(self, action: #selector(sortBtnClick), for: UIControlEvents.touchUpInside)
        view.imageView?.contentMode = UIViewContentMode.scaleAspectFit
        view.setTitleColor(UIColor.subTitle, for: UIControlState.normal)
        view.titleLabel?.font = UIFont.normal(size: 12)
        view.setTitle("按最新", for: UIControlState.normal)
        view.setImage(#imageLiteral(resourceName: "Arrow"), for: UIControlState.normal)
        return view
    }()
    
    fileprivate lazy var lineView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.background
        return view
    }()
}
