//
//  BCMomentsDetailCommentCell.swift
//  BCB
//
//  Created by Zrocky on 2018/4/12.
//  Copyright © 2018年 zrocky. All rights reserved.
//

import UIKit
import SnapKit

class BCMomentsDetailCommentCell: UITableViewCell {
    // MARK: - property
    var data: [String: Any]? {
        didSet {
            portraitBtn.setImage(url: "https://i.pinimg.com/564x/e9/fe/83/e9fe83cd3b687239737bf2af1baca191.jpg", for: UIControlState.normal)
            nameLabel.text = "黎明Alex"
            subtitleLabel.text = "3月14日 15:30  来自Android"
            contentLabel.text = "好像有报道说，现在比特币的价格是低于成本价的。要是比特币的价格继续下行，挖矿一直亏本。矿工是否会造反？比如弄个双重支付事件，同时做空比特币获利？"
            
            let random = arc4random_uniform(10)
            if random % 2 == 0  {
                retweetView.isHidden = true
                retweetViewBottomConstrait?.deactivate()
                contentLabelBottomConstrait?.deactivate()
                contentLabel.snp.makeConstraints { (make) in
                    contentLabelBottomConstrait = make.bottom.equalTo(-8).constraint
                }
            }else {
                retweetView.isHidden = false
                retweetViewBottomConstrait?.deactivate()
                contentLabelBottomConstrait?.deactivate()
                retweetView.snp.makeConstraints { (make) in
                    retweetViewBottomConstrait = make.bottom.equalTo(-8).constraint
                }
            }
            
            retweetNameLabel.text = "CNBC"
            retweetContentLabel.text = "刚刚落幕的G20峰会发布联合公报，将加密货币定义为资产而非货币，承认其提高金融经济效率和包容性的优势，但也对其逃税、洗钱、恐怖融资等问题表示关注。加密资产缺乏主权货币的关键属性，在某些时候会影响到金融稳定性。但由于各国在是否监管加密货币的问题上存在较大争议，国际上将不会对加密货币实施统一监管。"
        }
    }
    
    fileprivate var retweetViewBottomConstrait: Constraint?
    fileprivate var contentLabelBottomConstrait: Constraint?
    
    // MARK: - life cycle
    static func generateCell(tableView: UITableView) -> BCMomentsDetailCommentCell {
        let reuseIdentifier = String(describing: self)
        var cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier) as? BCMomentsDetailCommentCell
        if cell == nil {
            cell = BCMomentsDetailCommentCell(style: UITableViewCellStyle.default, reuseIdentifier: reuseIdentifier)
        }
        assert(cell != nil, "cell can't be nil")
        
        return cell!
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupSubviews()
        setupLayouts()
        
        contentView.layoutIfNeeded()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - event response
    func portraitBtnClick() {
        
    }
    
    func retweetViewClick() {
        
    }
    
    // MARK: - public methods
    
    // MARK: - private methods
    
    private func setupSubviews() {
        contentView.addSubview(portraitBtn)
        contentView.addSubview(nameLabel)
        contentView.addSubview(subtitleLabel)
        contentView.addSubview(contentLabel)
        contentView.addSubview(retweetView)
        retweetView.addSubview(retweetNameLabel)
        retweetView.addSubview(retweetContentLabel)
    }
    
    private func setupLayouts() {
        portraitBtn.snp.makeConstraints { (make) in
            make.top.leading.equalTo(16)
            make.width.height.equalTo(40)
        }
        
        nameLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(portraitBtn.snp.trailing).offset(8)
            make.top.equalTo(portraitBtn).offset(3)
        }
        
        subtitleLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(nameLabel)
            make.top.equalTo(nameLabel.snp.bottom)
        }
        
        contentLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(nameLabel)
            make.trailing.equalTo(-16)
            make.top.equalTo(portraitBtn.snp.bottom).offset(8)
        }
        
        retweetView.snp.makeConstraints { (make) in
            make.leading.equalTo(contentLabel)
            make.trailing.equalTo(-16)
            make.top.equalTo(contentLabel.snp.bottom).offset(8)
        }
        
        retweetNameLabel.snp.makeConstraints { (make) in
            make.top.leading.equalTo(8)
            make.trailing.equalTo(-8)
        }
        
        retweetContentLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(retweetNameLabel)
            make.trailing.equalTo(-8)
            make.top.equalTo(retweetNameLabel.snp.bottom).offset(4)
            make.bottom.equalTo(-8)
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        portraitBtn.layer.cornerRadius = portraitBtn.width / 2
    }
    
    // MARK: - UI property
    fileprivate lazy var portraitBtn: UIButton = {
        let view = UIButton()
        view.addTarget(self, action: #selector(portraitBtnClick), for: UIControlEvents.touchUpInside)
        view.imageView?.contentMode = UIViewContentMode.scaleAspectFill
        view.clipsToBounds = true
        return view
    }()
    
    fileprivate lazy var nameLabel: UILabel = {
        let view = UILabel()
        view.textColor = UIColor.title
        view.font = UIFont.normal(size: 14)
        return view
    }()
    
    fileprivate lazy var subtitleLabel: UILabel = {
        let view = UILabel()
        view.textColor = UIColor.content
        view.font = UIFont.normal(size: 10)
        return view
    }()
    
    fileprivate lazy var contentLabel: UILabel = {
        let view = UILabel()
        view.textColor = UIColor.title
        view.font = UIFont.normal(size: 14)
        view.numberOfLines = 0
        return view
    }()
    
    fileprivate lazy var retweetView: UIButton = {
        let view = UIButton(type: .custom)
        view.isHidden = true
        view.addTarget(self, action: #selector(retweetViewClick), for: UIControlEvents.touchUpInside)
        view.backgroundColor = UIColor.background.alpha(0.5)
        view.layer.cornerRadius = 3
        view.clipsToBounds = true
        return view
    }()
    
    fileprivate lazy var retweetNameLabel: UILabel = {
        let view = UILabel()
        view.font = UIFont.normal(size: 13)
        view.textColor = UIColor.darkBlue
        return view
    }()
    
    fileprivate lazy var retweetContentLabel: UILabel = {
        let view = UILabel()
        view.font = UIFont.normal(size: 13)
        view.textColor = UIColor.subTitle
        view.numberOfLines = 0
        return view
    }()
}
