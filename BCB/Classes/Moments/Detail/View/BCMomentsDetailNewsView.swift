//
//  BCMomentsDetailNewsView.swift
//  BCB
//
//  Created by Zrocky on 2018/4/12.
//  Copyright © 2018年 zrocky. All rights reserved.
//

import UIKit
import SnapKit

class BCMomentsDetailNewsView: UIView {
    // MARK: - property
    var data: [String: Any]? {
        didSet {
            titleLabel.text = "腾讯区块链落地物流场景，与中物联签署战略合作协议"
            subtitleLabel.text = "3月15日 10:45  来自腾讯科技"
            contentLabel.text = "第三届全球物流技术大会上，腾讯公司与中国物流与采购联合会（以下简称“中物联”）签署了战略合作协议，并联合发布了双方首个重要合作项目——区块供应链联盟链及云单平台。"
            let random = arc4random_uniform(10)
            if random % 2 == 0  {
                illustration.isHidden = true
                illustrationBottomConstrait?.deactivate()
                contentLabelBottomConstrait?.deactivate()
                contentLabel.snp.makeConstraints { (make) in
                    contentLabelBottomConstrait = make.bottom.equalTo(-8).constraint
                }
            }else {
                illustration.isHidden = false
                illustrationBottomConstrait?.deactivate()
                contentLabelBottomConstrait?.deactivate()
                illustration.snp.makeConstraints { (make) in
                    illustrationBottomConstrait = make.bottom.equalTo(-8).constraint
                }
            }
            illustration.setImage(url: "https://pbs.twimg.com/media/DafRlB3W4AAZonw.jpg")
        }
    }
    
    fileprivate var illustrationBottomConstrait: Constraint?
    fileprivate var contentLabelBottomConstrait: Constraint?
    
    // MARK: - life cycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupSubviews()
        setupLayouts()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - event response
    
    // MARK: - public methods
    
    // MARK: - private methods
    private func setupSubviews() {
        addSubview(contentView)
        contentView.addSubview(titleLabel)
        contentView.addSubview(subtitleLabel)
        contentView.addSubview(contentLabel)
        contentView.addSubview(illustration)
    }
    
    private func setupLayouts() {
        contentView.snp.makeConstraints { (make) in
            make.top.equalTo(8)
            make.leading.trailing.equalToSuperview()
            make.bottom.equalTo(-8)
        }
        
        titleLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(16)
            make.trailing.equalTo(-16)
            make.top.equalTo(10)
        }
        
        subtitleLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(16)
            make.trailing.equalTo(-16)
            make.top.equalTo(titleLabel.snp.bottom).offset(4)
        }
        
        contentLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(16)
            make.trailing.equalTo(-16)
            make.top.equalTo(subtitleLabel.snp.bottom).offset(8)
        }
        
        illustration.snp.makeConstraints { (make) in
            make.leading.equalTo(16)
            make.trailing.equalTo(-16)
            make.top.equalTo(contentLabel.snp.bottom).offset(6)
            make.height.equalTo(142)
            illustrationBottomConstrait = make.bottom.equalTo(-8).constraint
        }
    }
    
    // MARK: - UI property
    fileprivate lazy var contentView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        return view
    }()
    
    fileprivate lazy var titleLabel: UILabel = {
        let view = UILabel()
        view.textColor = UIColor.title
        view.font = UIFont.bold(size: 16)
        view.numberOfLines = 0
        return view
    }()
    
    fileprivate lazy var subtitleLabel: UILabel = {
        let view = UILabel()
        view.textColor = UIColor.subTitle
        view.font = UIFont.normal(size: 12)
        return view
    }()
    
    fileprivate lazy var contentLabel: UILabel = {
        let view = UILabel()
        view.textColor = UIColor.title
        view.font = UIFont.normal(size: 14)
        view.numberOfLines = 0
        return view
    }()
    
    fileprivate lazy var illustration: UIImageView = {
        let view = UIImageView()
        view.isHidden = true
        view.contentMode = UIViewContentMode.scaleAspectFill
        view.clipsToBounds = true
        return view
    }()
}
