//
//  BCMomentsNewsController.swift
//  BCB
//
//  Created by Zrocky on 2018/4/8.
//  Copyright © 2018年 zrocky. All rights reserved.
//

import UIKit

class BCMomentsNewsController: UIViewController {
    // MARK: - property
    
    // MARK: - life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSubviews()
        setupLayouts()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setupNavi()
    }
    
    private func setupNavi() {
        
    }
    
    // MARK: - event response
    
    // MARK: - public methods
    
    // MARK: - private methods
    private func setupSubviews() {
        view.addSubview(tableView)
    }
    
    private func setupLayouts() {
        tableView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    }
    
    // MARK: - UI property
    fileprivate lazy var tableView: UITableView = {
        let view = UITableView()
        view.backgroundColor = UIColor.background
        view.rowHeight = UITableViewAutomaticDimension
        view.estimatedRowHeight = 44
        view.dataSource = self
        view.delegate = self
        return view
    }()
    
}

// MARK: - UITableViewDataSource, UITableViewDelegate
extension BCMomentsNewsController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 20
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = BCMomentsNewsCell.generateCell(tableView: tableView)
        cell.data = ["BCOrder": indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = BCMomentsDetailController()
        vc.type = BCMomentsDetailControllerType.news
        vc.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(vc, animated: true)
    }
}
