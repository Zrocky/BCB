//
//  Date+RZCheck.swift
//  YJZ
//
//  Created by Zrocky on 2017/12/11.
//  Copyright © 2017年 Zrocky. All rights reserved.
//

import UIKit

extension Date {
    // MARK: - property
    
    /// is it today
    var isToday: Bool {
        let compareCmps = generateCompareCmps()
        let currentCmps = compareCmps.0
        let targetCmps = compareCmps.1
        return (targetCmps.year == currentCmps.year) && (targetCmps.month == currentCmps.month) && (targetCmps.day == currentCmps.day)
    }
    
    /// is it this month
    var isThisMonth: Bool {
        let compareCmps = generateCompareCmps()
        let currentCmps = compareCmps.0
        let targetCmps = compareCmps.1
        return (targetCmps.year == currentCmps.year) && (targetCmps.month == currentCmps.month)
    }
    
    /// is it this year
    var isThisYear: Bool {
        let compareCmps = generateCompareCmps()
        let currentCmps = compareCmps.0
        let targetCmps = compareCmps.1
        return targetCmps.year == currentCmps.year
    }
    
    // MARK: - public methods
    
    // MARK: - private methods
    
    /// generate date compare parameters
    ///
    /// - Returns: (currentCmps, targetCmps)
    private func generateCompareCmps() -> (DateComponents, DateComponents) {
        let calendar = Calendar.current
        let components: Set = [Calendar.Component.day, Calendar.Component.month, Calendar.Component.year]
        let currentCmps = calendar.dateComponents(components, from: Date())
        let targetCmps = calendar.dateComponents(components, from: self)
        return (currentCmps, targetCmps)
    }
}
