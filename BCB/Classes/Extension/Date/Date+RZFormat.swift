//
//  Date+RZFormat.swift
//  YJZ
//
//  Created by Zrocky on 2017/9/21.
//  Copyright © 2017年 Zrocky. All rights reserved.
//

import Foundation

extension Date {
    // MARK: - public methods
    func formatter(_ outFormatterStr: String) -> String {
        let outFormatter = DateFormatter()
        outFormatter.setLocalizedDateFormatFromTemplate("zh_CN")
        outFormatter.dateFormat = outFormatterStr
        
        return outFormatter.string(from: self)
    }
    
    /// get friendly display date text
    func displayStr() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        if self.isThisYear {
            if self.isToday {
                let cmps = intervalFromDate(self, toDate: Date())
                let hour = cmps.hour ?? 0
                let minute = cmps.minute ?? 0
                if hour >= 1 {
                    return "\(hour)小时前"
                }else if minute > 1 {
                    return "\(minute)分钟前"
                }else {
                    return "刚刚"
                }
            }else {
                formatter.dateFormat = "MM月dd日"
            }
        }else {
            formatter.dateFormat = "yyyy-MM-dd"
        }
        return formatter.string(from: self)
    }
    
    // MARK: - private methods
    func intervalFromDate(_ fromDate: Date, toDate: Date) -> DateComponents {
        let calendar = Calendar.current
        let unit: Set = [Calendar.Component.hour, Calendar.Component.minute, Calendar.Component.second]
        return calendar.dateComponents(unit, from: fromDate, to: toDate)
    }
}
