//
//  String+RZDate.swift
//  YJZ
//
//  Created by Zrocky on 2017/8/11.
//  Copyright © 2017年 Zrocky. All rights reserved.
//

import Foundation


extension String {
    var timestampToDate: Date {
        return Date(timeIntervalSince1970: (Double(self) ?? 0) / 1000)
    }
    
    func toDate(fromFormatterStr: String) -> Date {
        let fromFormatter = DateFormatter()
        fromFormatter.setLocalizedDateFormatFromTemplate("zh_CN")
        fromFormatter.dateFormat = fromFormatterStr
        if let date = fromFormatter.date(from: self) {
            return date
        }else {
            return Date()
        }
    }
    /**
     *  date formatter str to formatter date string
     */
    func formatter(fromFormatterStr: String, outFormatterStr: String) -> String {
        let fromFormatter = DateFormatter()
        fromFormatter.dateFormat = fromFormatterStr

        let outFormatter = DateFormatter()
        outFormatter.setLocalizedDateFormatFromTemplate("zh_CN")
        outFormatter.dateFormat = outFormatterStr
        
        if let date = fromFormatter.date(from: self) {
            return outFormatter.string(from: date)
        }
        return self
    }
    
    /**
     *  timestamp string to formatter date string
     */
    func formatterFromTimestamp(outFormatterStr: String) -> String {
        let outFormatter = DateFormatter()
        outFormatter.dateFormat = outFormatterStr
        
        let date = self.timestampToDate
        return outFormatter.string(from: date)
    }
}
