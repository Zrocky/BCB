//
//  String+RZAttribute.swift
//  BCB
//
//  Created by Zrocky on 2018/4/2.
//  Copyright © 2018年 zrocky. All rights reserved.
//

import UIKit

extension String {
    func convertAttributeString(substring: String,
                                mainFont: UIFont,
                                mainColor: UIColor,
                                subFont: UIFont,
                                subColor: UIColor) -> NSAttributedString {
        let attr = [NSFontAttributeName: mainFont,
                         NSForegroundColorAttributeName: mainColor]
        let attrString = NSMutableAttributedString(string: self, attributes: attr)
        let subStringRange = (attrString.string as NSString).range(of: substring)
        attrString.addAttribute(NSFontAttributeName, value: subFont, range: subStringRange)
        attrString.addAttribute(NSForegroundColorAttributeName, value: subColor, range: subStringRange)
        return attrString
    }
}
