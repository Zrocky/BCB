//
//  String+RZBase64.swift
//  YJZ
//
//  Created by Zrocky on 2017/12/25.
//  Copyright © 2017年 Zrocky. All rights reserved.
//

import UIKit

extension String {
    func decodeBase64() -> String  {
        if let decodedData = Data(base64Encoded: self) {
            let decodedString = String(data: decodedData, encoding: .utf8) ?? ""
            return decodedString
        }else {
            return ""
        }
    }
}
