//
//  RZURL.swift
//  YJZ
//
//  Created by Zrocky on 2017/9/11.
//  Copyright © 2017年 Zrocky. All rights reserved.
//

import UIKit

extension String {
    func encodeURL() -> String {
        return self.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) ?? self
    }
    
    func decodeURL() -> String {
        return self.removingPercentEncoding ?? self
    }
}
