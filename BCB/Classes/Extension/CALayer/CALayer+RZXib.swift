//
//  CALayer+Xib.swift
//  YJZ
//
//  Created by Zrocky on 2017/8/2.
//  Copyright © 2017年 Zrocky. All rights reserved.
//

import UIKit

extension CALayer {
    var shadowUIColor: UIColor {
        get {
            return UIColor(cgColor: shadowColor!)
        }
        
        set {
            shadowColor = shadowUIColor.cgColor
        }
    }
    
    var borderUIColor: UIColor {
        get {
           return UIColor(cgColor: borderColor!)
        }
        
        set {
            borderColor = borderUIColor.cgColor
        }
    }
}

//- (void)setShadowUIColor:(UIColor *)shadowUIColor {
//    self.shadowColor = shadowUIColor.CGColor;
//    }
//
//    - (void)setBorderUIColor:(UIColor *)borderUIColor {
//        self.borderColor = borderUIColor.CGColor;
//}

