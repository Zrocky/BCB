//
//  UIViewController+RZChildContent.swift
//  BCB
//
//  Created by Zrocky on 2018/4/8.
//  Copyright © 2018年 zrocky. All rights reserved.
//

import UIKit

extension UIViewController {
    func addContentControler(_ controller: UIViewController, to contentView: UIView) {
        addChildViewController(controller)
        contentView.addSubview(controller.view)
        controller.view.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        controller.didMove(toParentViewController: self)
    }
    
    func removeContentController(_ controller: UIViewController) {
        controller.willMove(toParentViewController: nil)
        controller.view.removeFromSuperview()
        controller.removeFromParentViewController()
    }
}
