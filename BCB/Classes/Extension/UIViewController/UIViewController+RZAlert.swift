//
//  UIViewController+RZAlert.swift
//  YJZ
//
//  Created by Zrocky on 2017/6/30.
//  Copyright © 2017年 Zrocky. All rights reserved.
//

import UIKit

extension UIViewController {
    func show() {
        let window = UIWindow(frame: UIScreen.main.bounds)
        window.rootViewController = RZAlertBackgroundController()
        window.windowLevel = UIWindowLevelAlert + 1
        
        window.makeKeyAndVisible()
        window.rootViewController!.present(self, animated: true, completion: nil)
    }
}
