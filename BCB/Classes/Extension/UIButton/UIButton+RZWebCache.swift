//
//  UIButton+RZWebCache.swift
//  BCB
//
//  Created by Zrocky on 2018/3/28.
//  Copyright © 2018年 zrocky. All rights reserved.
//

import UIKit


extension UIButton {
    // MARK: - public method
    func setImage(url: String,
                  for state: UIControlState,
                  placeholder: UIImage? = nil,
                  options: UIImageView.RZOptionsInfo? = nil,
                  progressBlock: UIImageView.RZDownloadProgressBlock? = nil,
                  completionHandler: UIImageView.RZCompletionHandler? = nil) {
        
        self.kf.setImage(with: URL(string: url), for: state, placeholder: placeholder, options: options, progressBlock: progressBlock, completionHandler: completionHandler)
    }
    
    func setBackgroundImage(url: String,
                            for state: UIControlState,
                            placeholder: UIImage? = nil,
                            options: UIImageView.RZOptionsInfo? = nil,
                            progressBlock: UIImageView.RZDownloadProgressBlock? = nil,
                            completionHandler: UIImageView.RZCompletionHandler? = nil) {
        
        self.kf.setBackgroundImage(with: URL(string: url), for: state, placeholder: placeholder, options: options, progressBlock: progressBlock, completionHandler: completionHandler)
    }
}
