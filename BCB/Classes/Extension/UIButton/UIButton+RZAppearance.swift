//
//  UIButton+RZAppearance.swift
//  YJZ
//
//  Created by Zrocky on 2017/7/19.
//  Copyright © 2017年 Zrocky. All rights reserved.
//

import UIKit

extension UIButton {
    func setBackgroundColor(_ color: UIColor, for state: UIControlState) {
        self.setBackgroundImage(UIImage(color: color), for: state)
    }
}
