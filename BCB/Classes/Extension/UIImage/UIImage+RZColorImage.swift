//
//  UIImage+RZColorImage.swift
//  YJZ
//
//  Created by Zrocky on 2017/7/18.
//  Copyright © 2017年 Zrocky. All rights reserved.
//}

import UIKit

extension UIImage {
    convenience init?(color: UIColor, size: CGSize = CGSize(width: 1 / UIScreen.main.scale, height: 1 / UIScreen.main.scale)) {
        let rect = CGRect(origin: .zero, size: size)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        color.setFill()
        UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        guard let cgImage = image?.cgImage else { return nil }
            self.init(cgImage: cgImage)
    }
}
