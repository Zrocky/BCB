//
//  UILabel+RZEdgeText.swift
//  BCB
//
//  Created by Zrocky on 2018/4/8.
//  Copyright © 2018年 zrocky. All rights reserved.
//

import UIKit

class RZEdgeLabel: UILabel {
    // add edge in UIlabel
    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets.init(top: 0, left: 3, bottom: 0, right: 3)
        super.drawText(in: UIEdgeInsetsInsetRect(rect, insets))
    }
}
