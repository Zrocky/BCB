//
//  Double+RZFormat.swift
//  YJZ
//
//  Created by Zrocky on 2017/9/16.
//  Copyright © 2017年 Zrocky. All rights reserved.
//

import Foundation

extension Double {
    /**
     *  format point, eg: 14000 -> 1.4k
     */
    func formatPoints() -> String {
        let num = self
        var thousandNum = num/1000
        var millionNum = num/1000000
        if num >= 1000 && num < 1000000{
            if(floor(thousandNum) == thousandNum){
                return("\(Int(thousandNum))k")
            }
            return("\(thousandNum.roundToPlaces(1))k")
        }
        if num > 1000000{
            if(floor(millionNum) == millionNum){
                return("\(Int(thousandNum))k")
            }
            return ("\(millionNum.roundToPlaces(1))M")
        }else {
            if(floor(num) == num){
                return ("\(Int(num))")
            }
            return ("\(num)")
        }
    }
    
    private mutating func roundToPlaces(_ places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return Darwin.round(self * divisor) / divisor
    }
}
