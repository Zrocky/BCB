//
//  Int+RZNumber.swift
//  YJZ
//
//  Created by Zrocky on 2017/7/18.
//  Copyright © 2017年 Zrocky. All rights reserved.
//

import UIKit

extension Int {
    func float() -> CGFloat {
        return CGFloat(self)
    }
}
