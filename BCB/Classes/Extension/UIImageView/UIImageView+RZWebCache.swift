//
//  UIImageView+RZWebCache.swift
//  YJZ
//
//  Created by Zrocky on 2017/6/29.
//  Copyright © 2017年 Zrocky. All rights reserved.
//

import Kingfisher


extension UIImageView {
    typealias RZOptionsInfo = KingfisherOptionsInfo
    typealias RZDownloadProgressBlock = ((_ receivedSize: Int64, _ totalSize: Int64) -> ())
    typealias RZCompletionHandler = ((_ image: Image?, _ error: NSError?, _ cacheType: CacheType, _ imageURL: URL?) -> ())
    typealias RZIndicatorType = IndicatorType
    
    // MARK: - public method
    func setImage(url: String,
                  placeholder: UIImage? = nil,
                  options: RZOptionsInfo? = nil,
                  progressBlock: RZDownloadProgressBlock? = nil,
                  completionHandler: RZCompletionHandler? = nil) {
        
        self.kf.setImage(with: URL(string: url), placeholder: placeholder, options: options, progressBlock: progressBlock, completionHandler: completionHandler)
    }
    
    /**
     *  download progress indicator
     */
    func setIndicatorType(_ indicatorType: RZIndicatorType) {
        self.kf.indicatorType = indicatorType
    }
}


