//
//  UIView+RZGradient.swift
//  YJZ
//
//  Created by Zrocky on 2017/9/19.
//  Copyright © 2017年 Zrocky. All rights reserved.
//

import UIKit

extension UIView {
    func drawGradient(colors: [UIColor], startPoint: CGPoint, endPoint: CGPoint) {
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.bounds
        self.layer.insertSublayer(gradientLayer, at: 0)
        gradientLayer.colors = colors.map { $0.cgColor }
        gradientLayer.startPoint = startPoint
        gradientLayer.endPoint = endPoint
        
    }
}

