//
//  BCMarketRankCoinMarketCollectionCell.swift
//  BCB
//
//  Created by Zrocky on 2018/4/9.
//  Copyright © 2018年 zrocky. All rights reserved.
//

import UIKit

class BCMarketRankCoinMarketCollectionCell: UICollectionViewCell {
    // MARK: - property
    var data: [String: Any]? {
        didSet {
            iconView.setImage(url: "https://img.bishijie.com/coinpic-bitcoin.jpg?imageMogr2/thumbnail/100x")
            titleLabel.text = "BTC"
            subtitleLabel.text = "比特币"
            priceLabel.text = "57277.18"
            dollarPriceLabel.text = "$9049.48"
        }
    }
    
    // MARK: - life cycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.white
        
        setupSubviews()
        setupLayouts()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - event response
    
    // MARK: - public methods
    
    // MARK: - private methods
    private func setupSubviews() {
        contentView.addSubview(iconView)
        contentView.addSubview(titleLabel)
        contentView.addSubview(subtitleLabel)
        contentView.addSubview(priceLabel)
        contentView.addSubview(dollarPriceLabel)
    }
    
    private func setupLayouts() {
        iconView.snp.makeConstraints { (make) in
            make.top.equalTo(8)
            make.width.height.equalTo(33)
            make.trailing.equalTo(contentView.snp.centerX).offset(-4)
        }
        
        titleLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(iconView.snp.trailing).offset(8)
            make.top.equalTo(8)
        }
        
        subtitleLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(titleLabel)
            make.top.equalTo(titleLabel.snp.bottom).offset(-2)
        }
        
        priceLabel.snp.makeConstraints { (make) in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(iconView.snp.bottom).offset(8)
        }
        
        dollarPriceLabel.snp.makeConstraints { (make) in
            make.top.equalTo(priceLabel.snp.bottom)
            make.leading.trailing.equalToSuperview()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        iconView.layer.cornerRadius = min(iconView.width, iconView.height) / 2
    }
    
    // MARK: - UI property
    fileprivate lazy var iconView: UIImageView = {
        let view = UIImageView()
        view.clipsToBounds = true
        return view
    }()
    
    fileprivate lazy var titleLabel: UILabel = {
        let view = UILabel()
        view.font = UIFont.bold(size: 16)
        view.textColor = UIColor.title
        return view
    }()
    
    fileprivate lazy var subtitleLabel: UILabel = {
        let view = UILabel()
        view.font = UIFont.normal(size: 10)
        view.textColor = UIColor.disable
        return view
    }()
    
    fileprivate lazy var priceLabel: UILabel = {
        let view = UILabel()
        view.font = UIFont.bold(size: 18)
        view.textColor = UIColor.lightGreen
        view.textAlignment = NSTextAlignment.center
        return view
    }()
    
    fileprivate lazy var dollarPriceLabel: UILabel = {
        let view = UILabel()
        view.font = UIFont.normal(size: 12)
        view.textColor = UIColor.subTitle
        view.textAlignment = NSTextAlignment.center
        return view
    }()
    
}
