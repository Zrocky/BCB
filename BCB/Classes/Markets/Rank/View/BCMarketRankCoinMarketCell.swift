//
//  BCMarketRankCoinMarketCell.swift
//  BCB
//
//  Created by Zrocky on 2018/4/10.
//  Copyright © 2018年 zrocky. All rights reserved.
//

import UIKit

class BCMarketRankCoinMarketCell: UITableViewCell {
    // MARK: - property
    var data: [String: Any]? {
        didSet {
            iconView.setImage(url: "https://img.bishijie.com/coinpic-bitcoin.jpg?imageMogr2/thumbnail/100x")
            titleLabel.text = "BTC"
            subtitleLabel.text = "比特币"
            priceLabel.text = "57277.18"
            dollarPriceLabel.text = "$9049.48"
            changeLabel.text = "-0.36%"
        }
    }
    
    // MARK: - life cycle
    static func generateCell(tableView: UITableView) -> BCMarketRankCoinMarketCell {
        let reuseIdentifier = String(describing: self)
        var cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier) as? BCMarketRankCoinMarketCell
        if cell == nil {
            cell = BCMarketRankCoinMarketCell(style: UITableViewCellStyle.default, reuseIdentifier: reuseIdentifier)
        }
        assert(cell != nil, "cell can't be nil")
        
        return cell!
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupSubviews()
        setupLayouts()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - event response
    
    // MARK: - public methods
    
    // MARK: - private methods
    private func setupSubviews() {
        contentView.addSubview(iconView)
        contentView.addSubview(titleLabel)
        contentView.addSubview(subtitleLabel)
        contentView.addSubview(priceLabel)
        contentView.addSubview(dollarPriceLabel)
        contentView.addSubview(changeLabel)
    }
    
    private func setupLayouts() {
        iconView.snp.makeConstraints { (make) in
            make.width.height.equalTo(33)
            make.centerY.equalToSuperview()
            make.leading.equalTo(16)
        }
        
        titleLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(iconView.snp.trailing).offset(8)
            make.top.equalTo(6)
        }
        
        subtitleLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(titleLabel)
            make.top.equalTo(titleLabel.snp.bottom)
        }
        
        priceLabel.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.top.equalTo(6)
        }
        
        dollarPriceLabel.snp.makeConstraints { (make) in
            make.top.equalTo(priceLabel.snp.bottom)
            make.leading.trailing.equalToSuperview()
        }
        
        changeLabel.snp.makeConstraints { (make) in
            make.trailing.equalTo(-16)
            make.top.equalTo(10)
            make.bottom.equalTo(-10)
            make.width.equalTo(77)
        }
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        iconView.layer.cornerRadius = min(iconView.width, iconView.height) / 2
    }
    
    // MARK: - UI property
    fileprivate lazy var iconView: UIImageView = {
        let view = UIImageView()
        view.clipsToBounds = true
        return view
    }()
    
    fileprivate lazy var titleLabel: UILabel = {
        let view = UILabel()
        view.font = UIFont.bold(size: 16)
        view.textColor = UIColor.title
        return view
    }()
    
    fileprivate lazy var subtitleLabel: UILabel = {
        let view = UILabel()
        view.font = UIFont.normal(size: 10)
        view.textColor = UIColor.disable
        return view
    }()
    
    fileprivate lazy var priceLabel: UILabel = {
        let view = UILabel()
        view.font = UIFont.bold(size: 16)
        view.textColor = UIColor.title
        view.textAlignment = NSTextAlignment.center
        return view
    }()
    
    fileprivate lazy var dollarPriceLabel: UILabel = {
        let view = UILabel()
        view.font = UIFont.normal(size: 12)
        view.textColor = UIColor.subTitle
        view.textAlignment = NSTextAlignment.center
        return view
    }()
    
    fileprivate lazy var changeLabel: RZEdgeLabel = {
        let view = RZEdgeLabel()
        view.font = UIFont.bold(size: 14)
        view.textColor = UIColor.white
        view.backgroundColor = UIColor.lightGreen
        view.textAlignment = NSTextAlignment.right
        view.layer.cornerRadius = 2
        view.clipsToBounds = true
        return view
    }()
}
