//
//  BCMarketRankSelectHeaderView.swift
//  BCB
//
//  Created by Zrocky on 2018/4/10.
//  Copyright © 2018年 zrocky. All rights reserved.
//

import UIKit

struct BCMarketRankSelectHeaderViewItem {
    var icon: UIImage
    var selectedIcon: UIImage
    var title: String = ""
    
    init(icon: UIImage,
         selectedIcon: UIImage,
         title: String = "") {
        self.icon = icon
        self.selectedIcon = selectedIcon
        self.title = title
    }
}

protocol BCMarketRankSelectHeaderViewDelegate: class {
    func marketRankSelectHeaderView(view: BCMarketRankSelectHeaderView, didClick index: Int)
}

class BCMarketRankSelectHeaderView: UIView {
    // MARK: - property
    var delegate: BCMarketRankSelectHeaderViewDelegate?
    
    fileprivate var btns: [BCMarketRankSelectHeaderViewButton] = []
    fileprivate var selectedIndex: Int = 0
    
    // MARK: - life cycle
    init(items: [BCMarketRankSelectHeaderViewItem]) {
        super.init(frame: CGRect.zero)
        
        guard items.count > 0 else { return }
        for item in items {
            setupBtn(item: item)
        }
        
        setupSubviews()
        setupLayouts()
        btnClick(btns.first!)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - event response
    func btnClick(_ btn: BCMarketRankSelectHeaderViewButton) {
        let preBtn = btns[selectedIndex]
        preBtn.isSelected = false
        btn.isSelected = true
        selectedIndex = btn.tag
        delegate?.marketRankSelectHeaderView(view: self, didClick: btn.tag)
    }
    
    // MARK: - public methods
    
    // MARK: - private methods
    fileprivate func setupBtn(item: BCMarketRankSelectHeaderViewItem) {
        let btn = BCMarketRankSelectHeaderViewButton(type: .custom)
        btn.backgroundColor = UIColor.white
        btn.setImage(item.icon, for: UIControlState.normal)
        btn.setImage(item.icon, for: UIControlState.highlighted)
        btn.setImage(item.selectedIcon, for: UIControlState.selected)
        btn.setImage(item.selectedIcon, for: [UIControlState.selected, UIControlState.highlighted])
        btn.setTitle(item.title, for: .normal)
        btn.setTitleColor(UIColor.title, for: UIControlState.normal)
        btn.setTitleColor(UIColor.darkBlue, for: UIControlState.selected)
        btn.setTitleColor(UIColor.darkBlue, for: [UIControlState.selected, UIControlState.highlighted])
        btn.titleLabel?.font = UIFont.normal(size: 14)
        btn.addTarget(self, action: #selector(btnClick(_:)), for: .touchUpInside)
        btn.imageEdgeInsets = UIEdgeInsetsMake(12, 0, 12, 0)
        btn.imageView?.contentMode = UIViewContentMode.scaleAspectFit
        btn.tag = btns.count
        btns.append(btn)
    }
    
    private func setupSubviews() {
        for btn in btns {
            addSubview(btn)
        }
    }
    
    private func setupLayouts() {
        for btn in btns {
            let hasPreBtn = btns.indices.contains(btn.tag - 1)
            btn.snp.makeConstraints({ (make) in
                make.top.bottom.equalToSuperview()
                make.width.equalToSuperview().offset(1-btns.count.float()).multipliedBy(1/btns.count.float())
                if hasPreBtn {
                    let preBtn = btns[btn.tag - 1]
                    make.leading.equalTo(preBtn.snp.trailing).offset(1)
                }else {
                    make.leading.equalToSuperview()
                }
            })
        }
    }
    
    // MARK: - UI property
}
