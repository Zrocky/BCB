//
//  BCMarketRankTurnoverCell.swift
//  BCB
//
//  Created by Zrocky on 2018/4/10.
//  Copyright © 2018年 zrocky. All rights reserved.
//

import UIKit

class BCMarketRankTurnoverCell: UITableViewCell {
    // MARK: - property
    var data: [String: Any]? {
        didSet {
            orderLabel.text = "\(data?["BCOrder"] as? Int ?? 0)"
            titleLabel.text = "BTC"
            priceLabel.text = "57277.18"
            dollarPriceLabel.text = "$9049.48"
            turnoverLabel.text = "471亿"
        }
    }
    
    // MARK: - life cycle
    static func generateCell(tableView: UITableView) -> BCMarketRankTurnoverCell {
        let reuseIdentifier = String(describing: self)
        var cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier) as? BCMarketRankTurnoverCell
        if cell == nil {
            cell = BCMarketRankTurnoverCell(style: UITableViewCellStyle.default, reuseIdentifier: reuseIdentifier)
        }
        assert(cell != nil, "cell can't be nil")
        
        return cell!
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupSubviews()
        setupLayouts()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - event response
    
    // MARK: - public methods
    
    // MARK: - private methods
    
    private func setupSubviews() {
        contentView.addSubview(orderLabel)
        contentView.addSubview(titleLabel)
        contentView.addSubview(priceLabel)
        contentView.addSubview(dollarPriceLabel)
        contentView.addSubview(turnoverLabel)
    }
    
    private func setupLayouts() {
        orderLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(16)
            make.width.height.equalTo(16)
            make.centerY.equalToSuperview()
        }
        
        titleLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(orderLabel.snp.trailing).offset(8)
            make.centerY.equalToSuperview()
        }
        
        priceLabel.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.top.equalTo(6)
        }
        
        dollarPriceLabel.snp.makeConstraints { (make) in
            make.centerX.equalTo(priceLabel)
            make.top.equalTo(priceLabel.snp.bottom)
        }
        
        turnoverLabel.snp.makeConstraints { (make) in
            make.trailing.equalTo(-16)
            make.top.equalTo(10)
            make.bottom.equalTo(-10)
            make.width.equalTo(77)
        }
    }
    
    // MARK: - UI property
    fileprivate lazy var orderLabel: UILabel = {
        let view = UILabel()
        view.font = UIFont.bold(size: 10)
        view.textColor = UIColor.white
        view.textAlignment = NSTextAlignment.center
        view.backgroundColor = UIColor.darkPurple
        view.layer.cornerRadius = 3
        view.clipsToBounds = true
        return view
    }()
    
    fileprivate lazy var titleLabel: UILabel = {
        let view = UILabel()
        view.font = UIFont.bold(size: 16)
        view.textColor = UIColor.title
        return view
    }()
    
    fileprivate lazy var priceLabel: UILabel = {
        let view = UILabel()
        view.font = UIFont.bold(size: 16)
        view.textColor = UIColor.title
        view.textAlignment = NSTextAlignment.center
        return view
    }()
    
    fileprivate lazy var dollarPriceLabel: UILabel = {
        let view = UILabel()
        view.font = UIFont.normal(size: 12)
        view.textColor = UIColor.subTitle
        view.textAlignment = NSTextAlignment.center
        return view
    }()
    
    fileprivate lazy var turnoverLabel: UILabel = {
        let view = UILabel()
        view.font = UIFont.bold(size: 16)
        view.textColor = UIColor.title
        view.textAlignment = NSTextAlignment.right
        return view
    }()
}
