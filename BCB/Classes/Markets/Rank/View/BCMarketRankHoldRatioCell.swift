//
//  BCMarketRankHoldRatioCell.swift
//  BCB
//
//  Created by Zrocky on 2018/4/10.
//  Copyright © 2018年 zrocky. All rights reserved.
//

import UIKit

class BCMarketRankHoldRatioCell: UITableViewCell {
    // MARK: - property
    var data: [String: Any]? {
        didSet {
            orderLabel.text = "\(data?["BCOrder"] as? Int ?? 0)"
            titleLabel.text = "BTC"
            holdRatioLabel.text = "56.65%"
            changeLabel.text = "15.35%"
        }
    }
    
    // MARK: - life cycle
    static func generateCell(tableView: UITableView) -> BCMarketRankHoldRatioCell {
        let reuseIdentifier = String(describing: self)
        var cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier) as? BCMarketRankHoldRatioCell
        if cell == nil {
            cell = BCMarketRankHoldRatioCell(style: UITableViewCellStyle.default, reuseIdentifier: reuseIdentifier)
        }
        assert(cell != nil, "cell can't be nil")
        
        return cell!
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupSubviews()
        setupLayouts()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - event response
    
    // MARK: - public methods
    
    // MARK: - private methods
    
    private func setupSubviews() {
        contentView.addSubview(orderLabel)
        contentView.addSubview(titleLabel)
        contentView.addSubview(holdRatioLabel)
        contentView.addSubview(changeLabel)
    }
    
    private func setupLayouts() {
        orderLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(16)
            make.width.height.equalTo(16)
            make.centerY.equalToSuperview()
        }
        
        titleLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(orderLabel.snp.trailing).offset(8)
            make.centerY.equalToSuperview()
        }
        
        holdRatioLabel.snp.makeConstraints { (make) in
            make.centerX.centerY.equalToSuperview()
        }
        
        changeLabel.snp.makeConstraints { (make) in
            make.trailing.equalTo(-16)
            make.top.equalTo(10)
            make.bottom.equalTo(-10)
            make.width.equalTo(77)
        }
    }
    
    // MARK: - UI property
    fileprivate lazy var orderLabel: UILabel = {
        let view = UILabel()
        view.font = UIFont.bold(size: 10)
        view.textColor = UIColor.white
        view.textAlignment = NSTextAlignment.center
        view.backgroundColor = UIColor.darkPurple
        view.layer.cornerRadius = 3
        view.clipsToBounds = true
        return view
    }()
    
    fileprivate lazy var titleLabel: UILabel = {
        let view = UILabel()
        view.font = UIFont.bold(size: 16)
        view.textColor = UIColor.title
        return view
    }()
    
    fileprivate lazy var holdRatioLabel: UILabel = {
        let view = UILabel()
        view.font = UIFont.bold(size: 16)
        view.textColor = UIColor.title
        view.textAlignment = NSTextAlignment.center
        return view
    }()
    
    fileprivate lazy var changeLabel: UILabel = {
        let view = UILabel()
        view.font = UIFont.bold(size: 16)
        view.textColor = UIColor.title
        view.textAlignment = NSTextAlignment.right
        return view
    }()
}
