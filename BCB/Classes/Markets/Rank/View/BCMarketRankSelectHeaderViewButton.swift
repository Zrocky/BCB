//
//  BCMarketRankSelectHeaderViewButton.swift
//  BCB
//
//  Created by Zrocky on 2018/4/10.
//  Copyright © 2018年 zrocky. All rights reserved.
//

import UIKit

class BCMarketRankSelectHeaderViewButton: UIButton {
    // MARK: - property
    
    // MARK: - life cycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupSubviews()
        setupLayouts()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var isSelected: Bool {
        didSet {
            triangleView.isHidden = !isSelected
        }
    }
    // MARK: - event response
    
    // MARK: - public methods
    
    // MARK: - private methods
    private func setupSubviews() {
        addSubview(triangleView)
    }
    
    private func setupLayouts() {
        triangleView.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.bottom.equalToSuperview()
//            make.width.height.equalTo(6)
        }
    }
    
    // MARK: - UI property
    fileprivate lazy var triangleView: UIImageView = {
        let view = UIImageView(image: #imageLiteral(resourceName: "Triangle"))
        view.contentMode = UIViewContentMode.scaleAspectFit
        view.isHidden = true
        return view
    }()
}
