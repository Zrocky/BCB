//
//  BCMarketRankHeaderView.swift
//  BCB
//
//  Created by Zrocky on 2018/4/9.
//  Copyright © 2018年 zrocky. All rights reserved.
//

import UIKit

protocol BCMarketRankHeaderViewDelegate: class {
    func marketRankHeaderView(didClick view: BCMarketRankHeaderView)
}

struct BCMarketRankHeaderViewItem {
    var icon: UIImage
    var title: String = ""
    var hasDetail: Bool = true
    
    init(icon: UIImage,
         title: String = "",
         hasDetail: Bool = true) {
        self.icon = icon
        self.title = title
        self.hasDetail = hasDetail
    }
}

class BCMarketRankHeaderView: UIView {
    // MARK: - property
    var delegate: BCMarketRankHeaderViewDelegate?
    var data: BCMarketRankHeaderViewItem? {
        didSet {
            iconView.image = data?.icon
            titleLabel.text = data?.title
            detailView.isHidden = !(data?.hasDetail ?? true)
            btn.isEnabled = data?.hasDetail ?? true
        }
    }
    
    // MARK: - life cycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupSubviews()
        setupLayouts()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - event response
    func btnClick() {
        delegate?.marketRankHeaderView(didClick: self)
    }
    
    // MARK: - public methods
    
    // MARK: - private methods
    private func setupSubviews() {
        addSubview(btn)
        btn.addSubview(iconView)
        btn.addSubview(titleLabel)
        btn.addSubview(detailView)
    }
    
    private func setupLayouts() {
        iconView.snp.makeConstraints { (make) in
            make.leading.equalTo(16)
            make.centerY.equalToSuperview()
        }
        
        titleLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(iconView.snp.trailing).offset(8)
            make.centerY.equalToSuperview()
        }
        
        detailView.snp.makeConstraints { (make) in
            make.width.equalTo(8)
            make.height.equalTo(13)
            make.centerY.equalToSuperview()
            make.trailing.equalTo(-16)
        }
        
        btn.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    }
    
    // MARK: - UI property
    fileprivate lazy var iconView: UIImageView = {
        let view = UIImageView()
        view.clipsToBounds = true
        return view
    }()
    
    fileprivate lazy var titleLabel: UILabel = {
        let view = UILabel()
        view.font = UIFont.normal(size: 14)
        view.textColor = UIColor.title
        return view
    }()
    
    fileprivate lazy var detailView: UIImageView = {
        let view = UIImageView(image: #imageLiteral(resourceName: "Detail"))
        view.contentMode = UIViewContentMode.scaleAspectFit
        return view
    }()
    
    fileprivate lazy var btn: UIButton = {
        let view = UIButton()
        view.backgroundColor = UIColor.white
        view.addTarget(self, action: #selector(btnClick), for: UIControlEvents.touchUpInside)
        return view
    }()
}
