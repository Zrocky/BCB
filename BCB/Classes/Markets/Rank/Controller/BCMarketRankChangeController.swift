//
//  BCMarketRankChangeController.swift
//  BCB
//
//  Created by Zrocky on 2018/4/10.
//  Copyright © 2018年 zrocky. All rights reserved.
//

import UIKit

enum BCMarketRankChangeControllerType {
    case increase
    case drop
}

class BCMarketRankChangeController: UIViewController {
    // MARK: - property
    var type: BCMarketRankChangeControllerType = .increase
    
    // MARK: - life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSubviews()
        setupLayouts()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setupNavi()
    }
    
    private func setupNavi() {
        if type == .increase {
            navigationItem.title = "涨幅榜"
        }else if type == .drop {
            navigationItem.title = "跌幅榜"
        }
    }
    
    // MARK: - event response
    
    // MARK: - public methods
    
    // MARK: - private methods
    private func setupSubviews() {
        view.addSubview(tableView)
    }
    
    private func setupLayouts() {
        tableView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    }
    
    // MARK: - UI property
    fileprivate lazy var tableView: UITableView = {
        let view = UITableView()
        view.backgroundColor = UIColor.background
        let tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: Layout.width, height: 8))
        view.tableHeaderView = tableHeaderView
        view.dataSource = self
        view.delegate = self
        return view
    }()
    
}

// MARK: - UITableViewDataSource, UITableViewDelegate
extension BCMarketRankChangeController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 100
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = BCMarketRankChangeCell.generateCell(tableView: tableView)
        cell.data = ["BCOrder": indexPath.row + 1]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 49
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 34
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = BCColumnNameView(items: [BCColumnNameViewItem(title: "币种",
                                                                 isSmaller: true),
                                            BCColumnNameViewItem(title: "最新价",
                                                                 isSmaller: true),
                                            BCColumnNameViewItem(title: "涨跌幅",
                                                                 isSmaller: true)])
        return view
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}


