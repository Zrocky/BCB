//
//  BCMarketExchangeCell.swift
//  BCB
//
//  Created by Zrocky on 2018/4/8.
//  Copyright © 2018年 zrocky. All rights reserved.
//

import UIKit

class BCMarketExchangeCell: UITableViewCell {
    // MARK: - property
    var data: [String: Any]? {
        didSet {
            iconView.setImage(url: "https://img.bishijie.com/exchange-binance.jpg?imageMogr2/thumbnail/100x")
            titleLabel.text = "Binance"
            priceLabel.text = "113亿"
            changeLabel.text = "-12.35%"
        }
    }
    
    // MARK: - life cycle
    static func generateCell(tableView: UITableView) -> BCMarketExchangeCell {
        let reuseIdentifier = String(describing: self)
        var cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier) as? BCMarketExchangeCell
        if cell == nil {
            cell = BCMarketExchangeCell(style: UITableViewCellStyle.default, reuseIdentifier: reuseIdentifier)
        }
        assert(cell != nil, "cell can't be nil")
        
        return cell!
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupSubviews()
        setupLayouts()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - event response
    
    // MARK: - public methods
    
    // MARK: - private methods
    
    private func setupSubviews() {
        contentView.addSubview(iconView)
        contentView.addSubview(titleLabel)
        contentView.addSubview(priceLabel)
        contentView.addSubview(changeLabel)
    }
    
    private func setupLayouts() {
        iconView.snp.makeConstraints { (make) in
            make.leading.equalTo(16)
            make.top.equalTo(8)
            make.bottom.equalTo(-8)
            make.width.equalTo(contentView.snp.height).offset(-16)
        }
        
        titleLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(iconView.snp.trailing).offset(8)
            make.centerY.equalToSuperview()
        }
        
        priceLabel.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
        }
        
        changeLabel.snp.makeConstraints { (make) in
            make.trailing.equalTo(-16)
            make.top.equalTo(10)
            make.bottom.equalTo(-10)
            make.width.equalTo(77)
        }
    }
    
    // MARK: - UI property
    fileprivate lazy var iconView: UIImageView = {
        let view = UIImageView()
        view.layer.cornerRadius = 3
        view.clipsToBounds = true
        return view
    }()
    
    fileprivate lazy var titleLabel: UILabel = {
        let view = UILabel()
        view.font = UIFont.bold(size: 16)
        view.textColor = UIColor.title
        return view
    }()
    
    fileprivate lazy var priceLabel: UILabel = {
        let view = UILabel()
        view.font = UIFont.normal(size: 16)
        view.textColor = UIColor.title
        return view
    }()
    
    fileprivate lazy var changeLabel: RZEdgeLabel = {
        let view = RZEdgeLabel()
        view.font = UIFont.bold(size: 14)
        view.textColor = UIColor.white
        view.backgroundColor = UIColor.lightRed
        view.textAlignment = NSTextAlignment.right
        view.layer.cornerRadius = 2
        view.clipsToBounds = true
        return view
    }()
}
