//
//  BCMarketCoinCell.swift
//  BCB
//
//  Created by Zrocky on 2018/4/8.
//  Copyright © 2018年 zrocky. All rights reserved.
//

import UIKit

class BCMarketCoinCell: UITableViewCell {
    // MARK: - property
    var data: [String: Any]? {
        didSet {
            iconView.setImage(url: "https://img.bishijie.com/coinpic-bitcoin.jpg?imageMogr2/thumbnail/100x")
            titleLabel.text = "BTC"
            subtitleLabel.text = "比特币"
            turnoverLabel.text = "479亿"
            valueLabel.text = "9,200亿"
        }
    }
    
    // MARK: - life cycle
    static func generateCell(tableView: UITableView) -> BCMarketCoinCell {
        let reuseIdentifier = String(describing: self)
        var cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier) as? BCMarketCoinCell
        if cell == nil {
            cell = BCMarketCoinCell(style: UITableViewCellStyle.default, reuseIdentifier: reuseIdentifier)
        }
        assert(cell != nil, "cell can't be nil")
        
        return cell!
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupSubviews()
        setupLayouts()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - event response
    
    // MARK: - public methods
    
    // MARK: - private methods
    
    private func setupSubviews() {
        contentView.addSubview(iconView)
        contentView.addSubview(titleLabel)
        contentView.addSubview(subtitleLabel)
        contentView.addSubview(turnoverLabel)
        contentView.addSubview(valueLabel)
    }
    
    private func setupLayouts() {
        iconView.snp.makeConstraints { (make) in
            make.leading.equalTo(16)
            make.top.equalTo(8)
            make.bottom.equalTo(-8)
            make.width.equalTo(contentView.snp.height).offset(-16)
        }
        
        titleLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(iconView.snp.trailing).offset(8)
            make.top.equalTo(6)
        }
        
        subtitleLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(titleLabel)
            make.top.equalTo(titleLabel.snp.bottom)
        }
        
        turnoverLabel.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
        }
        
        valueLabel.snp.makeConstraints { (make) in
            make.trailing.equalTo(-16)
            make.centerY.equalToSuperview()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        iconView.layer.cornerRadius = min(iconView.width, iconView.height) / 2
    }
    
    // MARK: - UI property
    fileprivate lazy var iconView: UIImageView = {
        let view = UIImageView()
        view.clipsToBounds = true
        return view
    }()
    
    fileprivate lazy var titleLabel: UILabel = {
        let view = UILabel()
        view.font = UIFont.bold(size: 16)
        view.textColor = UIColor.title
        return view
    }()
    
    fileprivate lazy var subtitleLabel: UILabel = {
        let view = UILabel()
        view.font = UIFont.normal(size: 10)
        view.textColor = UIColor.disable
        return view
    }()
    
    fileprivate lazy var turnoverLabel: UILabel = {
        let view = UILabel()
        view.font = UIFont.normal(size: 16)
        view.textColor = UIColor.title
        return view
    }()
    
    fileprivate lazy var valueLabel: UILabel = {
        let view = UILabel()
        view.font = UIFont.normal(size: 16)
        view.textColor = UIColor.title
        view.textAlignment = NSTextAlignment.right
        return view
    }()
}
