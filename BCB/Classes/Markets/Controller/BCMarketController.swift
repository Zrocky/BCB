//
//  BCMarketsController.swift
//  BCB
//
//  Created by Zrocky on 2018/3/27.
//  Copyright © 2018年 zrocky. All rights reserved.
//

import UIKit

class BCMarketController: UIViewController {
    // MARK: - property
    
    // MARK: - life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        
        setupSubviews()
        setupLayouts()
        
        if let firstVC = childContentControllers.first {
            addContentControler(firstVC, to: contentView)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setupNavi()
    }
    
    private func setupNavi() {
        navigationItem.titleView = segmentControl
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: portraitView)
    }
    
    // MARK: - event response
    
    // MARK: - public methods
    
    // MARK: - private methods
    private func setupSubviews() {
        view.addSubview(contentView)
    }
    
    private func setupLayouts() {
        segmentControl.snp.makeConstraints { (make) in
            make.width.equalTo(205)
            make.height.equalTo(44)
        }
        
        portraitView.snp.makeConstraints { (make) in
            make.width.height.equalTo(34)
        }
        
        contentView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    }
    
    // MARK: - UI property
    fileprivate lazy var segmentControl: RZTextSegmentControl = {
        let view = RZTextSegmentControl(items: ["榜单", "币种", "交易所"])
        view.delegate = self
        return view
    }()
    
    fileprivate lazy var childContentControllers = [BCMarketRankController(),
                                                    BCMarketCoinController(),
                                                    BCMarketExchangeController()]

    fileprivate lazy var portraitView = BCUserControl.sharedInstance
    
    fileprivate lazy var contentView: UIView = {
        let view = UIView()
        return view
    }()
}

extension BCMarketController: RZTextSegmentControlDelegate {
    func segmentControl(_ segment: RZTextSegmentControl, selectIndex: Int) {
        if let preVC = childViewControllers.first {
            removeContentController(preVC)
        }
        if childContentControllers.indices.contains(selectIndex) {
            addContentControler(childContentControllers[selectIndex], to: contentView)
        }
    }
}

