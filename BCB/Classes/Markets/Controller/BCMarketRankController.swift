//
//  BCMarketRankController.swift
//  BCB
//
//  Created by Zrocky on 2018/4/2.
//  Copyright © 2018年 zrocky. All rights reserved.
//

import UIKit

class BCMarketRankController: UIViewController {
    // MARK: - property
    fileprivate var selectedChangeType: BCMarketRankChangeControllerType = .increase
    
    // MARK: - life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.background
        
        setupSubviews()
        setupLayouts()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setupNavi()
    }
    
    private func setupNavi() {
        
    }
    
    // MARK: - event response
    func moreChangeBtnClick() {
        let vc = BCMarketRankChangeController()
        vc.type = selectedChangeType
        vc.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(vc, animated: true)
    }
    
    // MARK: - public methods
    
    // MARK: - private methods
    private func setupSubviews() {
        view.addSubview(scrollView)
        scrollView.addSubview(contentView)
        contentView.addSubview(coinMarketHeaderView)
        contentView.addSubview(coinMarketView)
        contentView.addSubview(changeHeaderView)
        contentView.addSubview(changeView)
        contentView.addSubview(moreChangeBtn)
        contentView.addSubview(turnoverHeaderView)
        contentView.addSubview(turnoverView)
        contentView.addSubview(inflowHeaderView)
        contentView.addSubview(inflowView)
        contentView.addSubview(holdRatioHeaderView)
        contentView.addSubview(holdRatioView)
    }
    
    private func setupLayouts() {
        scrollView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        contentView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
            make.width.equalToSuperview()
        }
        
        coinMarketHeaderView.snp.makeConstraints { (make) in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(8)
            make.height.equalTo(40)
        }
        
        coinMarketView.snp.makeConstraints { (make) in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(coinMarketHeaderView.snp.bottom).offset(1)
            make.height.equalTo(98)
        }
        
        changeHeaderView.snp.makeConstraints { (make) in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(coinMarketView.snp.bottom).offset(8)
            make.height.equalTo(40)
        }
        
        changeView.snp.makeConstraints { (make) in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(changeHeaderView.snp.bottom).offset(1)
            make.height.equalTo(280)
        }
        
        moreChangeBtn.snp.makeConstraints { (make) in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(changeView.snp.bottom)
            make.height.equalTo(40)
        }
        
        turnoverHeaderView.snp.makeConstraints { (make) in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(moreChangeBtn.snp.bottom).offset(8)
            make.height.equalTo(40)
        }
        
        turnoverView.snp.makeConstraints { (make) in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(turnoverHeaderView.snp.bottom).offset(1)
            make.height.equalTo(280)
        }
        
        inflowHeaderView.snp.makeConstraints { (make) in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(turnoverView.snp.bottom).offset(8)
            make.height.equalTo(40)
        }
        
        inflowView.snp.makeConstraints { (make) in
            make.top.equalTo(inflowHeaderView.snp.bottom).offset(1)
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(280)
        }
        
        holdRatioHeaderView.snp.makeConstraints { (make) in
            make.top.equalTo(inflowView.snp.bottom).offset(8)
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(40)
        }
        
        holdRatioView.snp.makeConstraints { (make) in
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(280)
            make.top.equalTo(holdRatioHeaderView.snp.bottom).offset(1)
            make.bottom.equalTo(-20)
        }
    }
    
    // MARK: - UI property
    fileprivate lazy var scrollView: UIScrollView = {
        let view = UIScrollView()
        return view
    }()
    
    fileprivate lazy var contentView: UIView = {
        let view = UIView()
        return view
    }()
    
    fileprivate lazy var coinMarketView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let view = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        let reuseIdentifier = String(describing: BCMarketRankCoinMarketCollectionCell.self)
        view.register(BCMarketRankCoinMarketCollectionCell.self, forCellWithReuseIdentifier: reuseIdentifier)
        view.backgroundColor = UIColor.background
        view.dataSource = self
        view.delegate = self
        return view
    }()
    
    fileprivate lazy var changeView: UITableView = {
        let view = UITableView()
        view.isScrollEnabled = false
        view.dataSource = self
        view.delegate = self
        return view
    }()
    
    fileprivate lazy var moreChangeBtn: BCColumnNameViewButton = {
        let view = BCColumnNameViewButton(type: .custom)
        view.backgroundColor = UIColor.white
        view.setImage(#imageLiteral(resourceName: "Detail"), for: UIControlState.normal)
        view.setTitle("查看更多", for: UIControlState.normal)
        view.setTitleColor(UIColor.disable, for: UIControlState.normal)
        view.setTitleColor(UIColor.disable.alpha(0.8), for: UIControlState.highlighted)
        view.titleLabel?.font = UIFont.normal(size: 14)
        view.addTarget(self, action: #selector(moreChangeBtnClick), for: UIControlEvents.touchUpInside)
        return view
    }()
    
    fileprivate lazy var turnoverView: UITableView = {
        let view = UITableView()
        view.isScrollEnabled = false
        view.dataSource = self
        view.delegate = self
        return view
    }()
    
    fileprivate lazy var inflowView: UITableView = {
        let view = UITableView()
        view.isScrollEnabled = false
        view.dataSource = self
        view.delegate = self
        return view
    }()
    
    fileprivate lazy var holdRatioView: UITableView = {
        let view = UITableView()
        view.isScrollEnabled = false
        view.dataSource = self
        view.delegate = self
        return view
    }()
    
    fileprivate lazy var coinMarketHeaderView: BCMarketRankHeaderView = {
        let view = BCMarketRankHeaderView()
        view.data = BCMarketRankHeaderViewItem(icon: #imageLiteral(resourceName: "CoinMarket"), title: "币种行情", hasDetail: true)
        view.delegate = self
        return view
    }()
    
    fileprivate lazy var changeHeaderView: BCMarketRankSelectHeaderView = {
        let view = BCMarketRankSelectHeaderView(items: [BCMarketRankSelectHeaderViewItem(icon: #imageLiteral(resourceName: "CoinMarket"), selectedIcon: #imageLiteral(resourceName: "CoinMarket"), title: "涨幅榜"),
                                                        BCMarketRankSelectHeaderViewItem(icon: #imageLiteral(resourceName: "CoinMarket"), selectedIcon: #imageLiteral(resourceName: "CoinMarket"), title: "跌幅榜")])
        view.delegate = self
        return view
    }()
    
    fileprivate lazy var turnoverHeaderView: BCMarketRankHeaderView = {
        let view = BCMarketRankHeaderView()
        view.data = BCMarketRankHeaderViewItem(icon: #imageLiteral(resourceName: "Turnover"), title: "24H成交量榜", hasDetail: true)
        view.delegate = self
        return view
    }()
    
    fileprivate lazy var inflowHeaderView: BCMarketRankHeaderView = {
        let view = BCMarketRankHeaderView()
        view.data = BCMarketRankHeaderViewItem(icon: #imageLiteral(resourceName: "Inflow"), title: "24H资金流入榜", hasDetail: true)
        view.delegate = self
        return view
    }()
    
    fileprivate lazy var holdRatioHeaderView: BCMarketRankHeaderView = {
        let view = BCMarketRankHeaderView()
        view.data = BCMarketRankHeaderViewItem(icon: #imageLiteral(resourceName: "HoldRatio"), title: "持有比例变动榜", hasDetail: true)
        view.delegate = self
        return view
    }()
}

// MARK: - BCMarketRankSelectHeaderViewDelegate
extension BCMarketRankController: BCMarketRankSelectHeaderViewDelegate {
    func marketRankSelectHeaderView(view: BCMarketRankSelectHeaderView, didClick index: Int) {
        if index == 0 {
            selectedChangeType = .increase
        }else if index == 1 {
            selectedChangeType = .drop
        }
    }
}

// MARK: - BCMarketRankHeaderViewDelegate
extension BCMarketRankController: BCMarketRankHeaderViewDelegate {
    func marketRankHeaderView(didClick view: BCMarketRankHeaderView) {
        switch view {
        case coinMarketHeaderView:
            let vc = BCMarketRankCoinMarketController()
            vc.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(vc, animated: true)
        case turnoverHeaderView:
            let vc = BCMarketRankTurnoverController()
            vc.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(vc, animated: true)
        case inflowHeaderView:
            let vc = BCMarketRankInflowController()
            vc.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(vc, animated: true)
        case holdRatioHeaderView:
            let vc = BCMarketRankHoldRatioController()
            vc.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(vc, animated: true)
        default:
            break
        }
    }
}

// MARK: - UICollectionViewDataSource & UICollectionViewDelegate
extension BCMarketRankController: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let reuseIdentifier = String(describing: BCMarketRankCoinMarketCollectionCell.self)
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! BCMarketRankCoinMarketCollectionCell
        cell.data = [:]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension BCMarketRankController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.width - 2) / 3, height: 98)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
}

// MARK: - UITableViewDataSource, UITableViewDelegate
extension BCMarketRankController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch tableView {
        case changeView:
            let cell = BCMarketRankChangeCell.generateCell(tableView: tableView)
            cell.data = ["BCOrder": indexPath.row + 1]
            return cell
        case turnoverView:
            let cell = BCMarketRankTurnoverCell.generateCell(tableView: tableView)
            cell.data = ["BCOrder": indexPath.row + 1]
            return cell
        case inflowView:
            let cell = BCMarketRankInflowCell.generateCell(tableView: tableView)
            cell.data = ["BCOrder": indexPath.row + 1]
            return cell
        case holdRatioView:
            let cell = BCMarketRankHoldRatioCell.generateCell(tableView: tableView)
            cell.data = ["BCOrder": indexPath.row + 1]
            return cell
        default:
            break
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 49
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 34
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if tableView == changeView {
            let view = BCColumnNameView(items: [BCColumnNameViewItem(title: "币种",
                                                                     isSmaller: true),
                                                BCColumnNameViewItem(title: "最新价",
                                                                     isSmaller: true),
                                                BCColumnNameViewItem(title: "涨跌幅",
                                                                     isSmaller: true)])
            return view
        }else if tableView == turnoverView {
            let view = BCColumnNameView(items: [BCColumnNameViewItem(title: "币种",
                                                                     isSmaller: true),
                                                BCColumnNameViewItem(title: "最新价",
                                                                     isSmaller: true),
                                                BCColumnNameViewItem(title: "交易量",
                                                                     isSmaller: true)])
            return view
        }else if tableView == inflowView {
            let view = BCColumnNameView(items: [BCColumnNameViewItem(title: "币种",
                                                                     isSmaller: true),
                                                BCColumnNameViewItem(title: "最新价",
                                                                     isSmaller: true),
                                                BCColumnNameViewItem(title: "净流入",
                                                                     isSmaller: true)])
            return view
        }else if tableView == holdRatioView {
            let view = BCColumnNameView(items: [BCColumnNameViewItem(title: "币种",
                                                                     isSmaller: true),
                                                BCColumnNameViewItem(title: "前100用户持有比例",
                                                                     isSmaller: true),
                                                BCColumnNameViewItem(title: "变动幅度",
                                                                     isSmaller: true)])
            return view
        }
        return view
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}
