//
//  BCExchangeDetailDataView.swift
//  BCB
//
//  Created by Zrocky on 2018/4/17.
//  Copyright © 2018年 zrocky. All rights reserved.
//

import UIKit

class BCExchangeDetailDataView: UIView {
    // MARK: - property
    var data: [String: Any]? {
        didSet {
            priceLabel.text = "￥100.32亿"
            changeLabel.text = "+379.84%"
            netInflowLabel.text = "净流入额 5.58亿"
            inflowLabel.text = "流入额 47.73亿"
            outflowLabel.text = "流出额 52.17亿"
        }
    }
    
    // MARK: - life cycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupSubviews()
        setupLayouts()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - event response
    
    // MARK: - public methods
    
    // MARK: - private methods
    private func setupSubviews() {
        addSubview(contentView)
        contentView.addSubview(priceTitleLabel)
        contentView.addSubview(priceLabel)
        contentView.addSubview(changeLabel)
        contentView.addSubview(netInflowLabel)
        contentView.addSubview(inflowLabel)
        contentView.addSubview(outflowLabel)
    }
    
    private func setupLayouts() {
        contentView.snp.makeConstraints { (make) in
            make.leading.top.trailing.equalToSuperview()
            make.bottom.equalTo(-8)
        }
        
        priceTitleLabel.snp.makeConstraints { (make) in
            make.top.equalTo(12)
            make.trailing.equalTo(changeLabel.snp.leading)
            make.leading.equalTo(16)
        }
        
        priceLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(priceTitleLabel)
            make.trailing.equalTo(changeLabel.snp.leading)
            make.top.equalTo(priceTitleLabel.snp.bottom)
        }
        
        changeLabel.snp.makeConstraints { (make) in
            make.top.equalTo(18)
            make.trailing.equalTo(inflowLabel.snp.leading)
            make.width.equalToSuperview().multipliedBy(0.3)
        }
        
        netInflowLabel.snp.makeConstraints { (make) in
            make.width.equalTo(changeLabel)
            make.trailing.equalTo(inflowLabel.snp.leading)
            make.bottom.equalTo(-18)
        }
        
        inflowLabel.snp.makeConstraints { (make) in
            make.trailing.equalTo(-16)
            make.width.equalToSuperview().multipliedBy(0.25)
            make.top.equalTo(18)
        }
        
        outflowLabel.snp.makeConstraints { (make) in
            make.trailing.width.equalTo(inflowLabel)
            make.bottom.equalTo(-18)
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        contentView.drawGradient(colors: [UIColor.darkPurple, UIColor.darkBlue], startPoint: CGPoint(x: 0, y: 0), endPoint: CGPoint(x: 0, y: 1))
    }
    
    // MARK: - UI property
    fileprivate lazy var contentView: UIView = {
        let view = UIView()
        return view
    }()
    
    fileprivate lazy var priceTitleLabel: UILabel = {
        let view = UILabel()
        view.textColor = UIColor.white.alpha(0.7)
        view.font = UIFont.normal(size: 12)
        view.text = "成交额"
        return view
    }()
    
    fileprivate lazy var priceLabel: UILabel = {
        let view = UILabel()
        view.textColor = UIColor.white
        view.font = UIFont.bold(size: 24)
        return view
    }()
    
    fileprivate lazy var changeLabel: UILabel = {
        let view = UILabel()
        view.textColor = UIColor.white
        view.font = UIFont.normal(size: 12)
        return view
    }()
    
    fileprivate lazy var netInflowLabel: UILabel = {
        let view = UILabel()
        view.textColor = UIColor.white
        view.font = UIFont.normal(size: 12)
        return view
    }()
    
    fileprivate lazy var inflowLabel: UILabel = {
        let view = UILabel()
        view.textColor = UIColor.white
        view.font = UIFont.normal(size: 12)
        return view
    }()
    
    fileprivate lazy var outflowLabel: UILabel = {
        let view = UILabel()
        view.textColor = UIColor.white
        view.font = UIFont.normal(size: 12)
        return view
    }()
}
