//
//  BCNewsRealTimeHeaderView.swift
//  BCB
//
//  Created by Zrocky on 2018/3/30.
//  Copyright © 2018年 zrocky. All rights reserved.
//

import UIKit

class BCNewsRealTimeHeaderView: UITableViewHeaderFooterView {
    // MARK: - property
    var data: [String: Any]? {
        didSet {
            titleLabel.text = "今天 03月20日 星期二"
        }
    }
    
    // MARK: - life cycle
    static func generateHeader() -> BCNewsRealTimeHeaderView {
        let reuseIdentifier = String(describing: self)
        return BCNewsRealTimeHeaderView(reuseIdentifier: reuseIdentifier)
    }
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        
        setupSubviews()
        setupLayouts()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - event response
    
    // MARK: - public methods
    
    // MARK: - private methods
    private func setupSubviews() {
        contentView.backgroundColor = UIColor.background
        contentView.addSubview(titleLabel)
    }
    
    private func setupLayouts() {
        titleLabel.snp.makeConstraints({ (make) in
            make.leading.equalTo(16)
            make.trailing.equalTo(-16)
            make.top.bottom.equalToSuperview()
        })
    }
    
    // MARK: - UI property
    fileprivate lazy var titleLabel: UILabel = {
        let view = UILabel()
        view.textColor = UIColor.title
        view.font = UIFont.normal(size: 14)
        return view
    }()
}
