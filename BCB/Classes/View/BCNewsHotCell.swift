//
//  BCNewsHotCell.swift
//  BCB
//
//  Created by Zrocky on 2018/3/29.
//  Copyright © 2018年 zrocky. All rights reserved.
//

import UIKit

class BCNewsHotCell: UITableViewCell {
    // MARK: - property
    var data: [String: Any]? {
        didSet {
            titleLabel.text = "IBM在其投资者年会强调区块链技术的重大作用"
            subtitleLabel.text = "湾区报道 · 25评论"
            illustrationView.setImage(url: "http://cdn.8btc.com/wp-content/uploads/2018/03/201803150734211689.jpg")
        }
    }
    
    // MARK: - life cycle
    static func generateCell(tableView: UITableView) -> BCNewsHotCell {
        let reuseIdentifier = String(describing: self)
        var cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier) as? BCNewsHotCell
        if cell == nil {
            cell = BCNewsHotCell(style: UITableViewCellStyle.default, reuseIdentifier: reuseIdentifier)
        }
        assert(cell != nil, "cell can't be nil")
        
        return cell!
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupSubviews()
        setupLayouts()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - event response
    
    // MARK: - public methods
    
    // MARK: - private methods
    
    private func setupSubviews() {
        contentView.addSubview(titleLabel)
        contentView.addSubview(subtitleLabel)
        contentView.addSubview(illustrationView)
    }
    
    private func setupLayouts() {
        illustrationView.snp.makeConstraints { (make) in
            make.trailing.equalTo(-16)
            make.top.equalTo(10)
            make.bottom.equalTo(-10)
            make.width.equalToSuperview().multipliedBy(0.3)
        }
        
        titleLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(16)
            make.top.equalTo(10)
            make.trailing.equalTo(illustrationView.snp.leading).offset(-8)
        }
        
        subtitleLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(16)
            make.bottom.equalTo(-10)
            make.trailing.equalTo(-16)
        }
    }
    
    // MARK: - UI property
    fileprivate lazy var titleLabel: UILabel = {
        let view = UILabel()
        view.font = UIFont.bold(size: 16)
        view.textColor = UIColor.title
        view.numberOfLines = 2
        return view
    }()
    
    fileprivate lazy var subtitleLabel: UILabel = {
        let view = UILabel()
        view.font = UIFont.normal(size: 14)
        view.textColor = UIColor.subTitle
        return view
    }()
    
    fileprivate lazy var illustrationView: UIImageView = {
        let view = UIImageView()
        view.contentMode = UIViewContentMode.scaleAspectFill
        view.clipsToBounds = true
        return view
    }()
}
