//
//  BCNewsRealTimeCell.swift
//  BCB
//
//  Created by Zrocky on 2018/3/30.
//  Copyright © 2018年 zrocky. All rights reserved.
//

import UIKit
import SnapKit

class BCNewsRealTimeCell: UITableViewCell {
    // MARK: - property
    var data: [String: Any]? {
        didSet {
            timeLabel.text = "15:30"
            titleLabel.text = "巴曙松：区块链等技术具备应用于金融领域的基础条件"
            contentLabel.text = "据证券时报消息，3月19日举行的“2018中英金融科技论坛”上，参会者一致认为金融行业的发展和革命性变化，已经进入到科技驱动引导的新时代，区块链将成未来金融核心底…"
        }
    }
    
    var isFirst: Bool = false {
        didSet {
            if isFirst {
                pointView.layer.borderColor = UIColor.darkRed.cgColor
                lineTopConstraint?.deactivate()
                lineView.snp.makeConstraints { (make) in
                    lineTopConstraint = make.top.equalTo(timeLabel.snp.centerY).constraint
                }
            }else {
                pointView.layer.borderColor = UIColor.lightCyan.cgColor
                lineTopConstraint?.deactivate()
                lineView.snp.makeConstraints { (make) in
                    lineTopConstraint = make.top.equalToSuperview().constraint
                }
            }
        }
    }
    
    fileprivate var lineTopConstraint: Constraint?
    
    // MARK: - life cycle
    static func generateCell(tableView: UITableView) -> BCNewsRealTimeCell {
        let reuseIdentifier = String(describing: self)
        var cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier) as? BCNewsRealTimeCell
        if cell == nil {
            cell = BCNewsRealTimeCell(style: UITableViewCellStyle.default, reuseIdentifier: reuseIdentifier)
        }
        assert(cell != nil, "cell can't be nil")
        
        return cell!
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupSubviews()
        setupLayouts()
        
        contentView.layoutIfNeeded()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - event response
    
    // MARK: - public methods
    
    // MARK: - private methods
    
    private func setupSubviews() {
        contentView.addSubview(timeLabel)
        contentView.addSubview(lineView)
        contentView.addSubview(pointView)
        contentView.addSubview(titleLabel)
        contentView.addSubview(contentLabel)
    }
    
    private func setupLayouts() {
        timeLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(16)
            make.top.equalTo(16)
        }
        
        lineView.snp.makeConstraints { (make) in
            make.width.equalTo(2)
            lineTopConstraint = make.top.equalToSuperview().constraint
            make.bottom.equalToSuperview()
            make.centerX.equalTo(60)
        }
        
        pointView.snp.makeConstraints { (make) in
            make.width.height.equalTo(8)
            make.centerY.equalTo(timeLabel)
            make.centerX.equalTo(lineView)
        }
        
        titleLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(pointView.snp.trailing).offset(12)
            make.top.equalTo(timeLabel)
            make.trailing.equalTo(-16)
        }
        
        contentLabel.snp.makeConstraints { (make) in
            make.leading.trailing.equalTo(titleLabel)
            make.top.equalTo(titleLabel.snp.bottom).offset(4)
            make.bottom.equalTo(-16)
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        pointView.layer.cornerRadius = min(pointView.width, pointView.height) / 2
    }
    
    // MARK: - UI property
    fileprivate lazy var timeLabel: UILabel = {
        let view = UILabel()
        view.textColor = UIColor.title
        view.font = UIFont.normal(size: 14)
        return view
    }()
    
    fileprivate lazy var lineView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.background
        return view
    }()
    
    fileprivate lazy var pointView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.layer.borderColor = UIColor.lightCyan.cgColor
        view.layer.borderWidth = 1
        return view
    }()
    
    fileprivate lazy var titleLabel: UILabel = {
        let view = UILabel()
        view.textColor = UIColor.title
        view.font = UIFont.bold(size: 15)
        view.numberOfLines = 0
        return view
    }()
    
    fileprivate lazy var contentLabel: UILabel = {
        let view = UILabel()
        view.textColor = UIColor.title
        view.font = UIFont.normal(size: 14)
        view.numberOfLines = 0
        return view
    }()
}
