//
//  BCUserInfoView.swift
//  BCB
//
//  Created by Zrocky on 2018/4/18.
//  Copyright © 2018年 zrocky. All rights reserved.
//

import UIKit

class BCUserInfoView: UIView {
    // MARK: - property
    var data: [String: Any]? {
        didSet {
            iconView.setImage(url: "https://i.pinimg.com/564x/04/09/d6/0409d6be0fb2e8ed188ea95938c8927f.jpg")
            nameLabel.text = "雕弓满月"
            fansBtn.data = BCUserInfoViewButtonItem(title: "粉丝", value: "256")
            focusBtn.data = BCUserInfoViewButtonItem(title: "关注", value: "300")
            tickerBtn.data = BCUserInfoViewButtonItem(title: "自选", value: "17")
            portfolioBtn.data = BCUserInfoViewButtonItem(title: "组合", value: "9")
        }
    }
    
    // MARK: - life cycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.background
        
        setupSubviews()
        setupLayouts()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - event response
    
    // MARK: - public methods
    
    // MARK: - private methods
    private func setupSubviews() {
        addSubview(bgView)
        addSubview(iconView)
        addSubview(nameLabel)
        addSubview(followBtn)
        addSubview(lineView)
        addSubview(fansBtn)
        addSubview(focusBtn)
        addSubview(tickerBtn)
        addSubview(portfolioBtn)
    }
    
    private func setupLayouts() {
        bgView.snp.makeConstraints { (make) in
            make.top.leading.trailing.equalToSuperview()
            make.bottom.equalTo(-8)
        }
        
        iconView.snp.makeConstraints { (make) in
            make.leading.equalTo(16)
            make.centerY.equalToSuperview()
            make.width.height.equalTo(82)
        }
        
        nameLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(iconView.snp.trailing).offset(16)
            make.top.equalTo(22)
        }
        
        followBtn.snp.makeConstraints { (make) in
            make.trailing.equalTo(-16)
            make.width.equalTo(61)
            make.height.equalTo(22)
            make.centerY.equalTo(nameLabel)
        }
        
        lineView.snp.makeConstraints { (make) in
            make.leading.equalTo(nameLabel)
            make.trailing.equalTo(followBtn)
            make.centerY.equalToSuperview()
            make.height.equalTo(1)
        }
        
        fansBtn.snp.makeConstraints { (make) in
            make.leading.equalTo(nameLabel)
            make.bottom.equalTo(-22)
//            make.height.equalTo(40)
            make.width.equalTo(35)
        }
        
        focusBtn.snp.makeConstraints { (make) in
            make.leading.equalTo(fansBtn.snp.trailing).offset(22)
            make.bottom.height.width.equalTo(fansBtn)
        }
        
        tickerBtn.snp.makeConstraints { (make) in
            make.leading.equalTo(focusBtn.snp.trailing).offset(22)
            make.bottom.height.width.equalTo(fansBtn)
        }
        
        portfolioBtn.snp.makeConstraints { (make) in
            make.leading.equalTo(tickerBtn.snp.trailing).offset(22)
            make.bottom.height.width.equalTo(fansBtn)
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        iconView.layer.cornerRadius = min(iconView.width, iconView.height) / 2
    }
    
    // MARK: - UI property
    fileprivate lazy var bgView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        return view
    }()
    
    fileprivate lazy var iconView: UIImageView = {
        let view = UIImageView()
        view.contentMode = UIViewContentMode.scaleAspectFill
        view.clipsToBounds = true
        return view
    }()
    
    fileprivate lazy var nameLabel: UILabel = {
        let view = UILabel()
        view.font = UIFont.bold(size: 18)
        view.textColor = UIColor.title
        return view
    }()
    
    fileprivate lazy var followBtn: UIButton = {
        let view = UIButton(type: UIButtonType.custom)
        view.setBackgroundColor(UIColor.darkBlue, for: UIControlState.normal)
        view.setTitle("关注", for: UIControlState.normal)
        view.titleLabel?.font = UIFont.normal(size: 12)
        view.setImage(#imageLiteral(resourceName: "Add_w"), for: UIControlState.normal)
        view.layer.cornerRadius = 3
        view.clipsToBounds = true
        return view
    }()
    
    fileprivate lazy var lineView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.background
        return view
    }()
    
    fileprivate lazy var fansBtn: BCUserInfoViewButton = {
        let view = BCUserInfoViewButton(type: UIButtonType.custom)
        return view
    }()
    
    fileprivate lazy var focusBtn: BCUserInfoViewButton = {
        let view = BCUserInfoViewButton(type: UIButtonType.custom)
        return view
    }()
    
    fileprivate lazy var tickerBtn: BCUserInfoViewButton = {
        let view = BCUserInfoViewButton(type: UIButtonType.custom)
        return view
    }()
    
    fileprivate lazy var portfolioBtn: BCUserInfoViewButton = {
        let view = BCUserInfoViewButton(type: UIButtonType.custom)
        return view
    }()
}


struct BCUserInfoViewButtonItem {
    var title: String
    var value: String
    init(title: String, value: String) {
        self.title = title
        self.value = value
    }
}

class BCUserInfoViewButton: UIButton {
    // MARK: - property
    var data: BCUserInfoViewButtonItem? {
        didSet {
            nameLabel.text = data?.title
            valueLabel.text = data?.value
        }
    }
    
    // MARK: - life cycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupSubviews()
        setupLayouts()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - event response
    
    // MARK: - public methods
    
    // MARK: - private methods
    private func setupSubviews() {
        addSubview(valueLabel)
        addSubview(nameLabel)
    }
    
    private func setupLayouts() {
        valueLabel.snp.makeConstraints { (make) in
            make.top.leading.trailing.equalToSuperview()
        }
        
        nameLabel.snp.makeConstraints { (make) in
            make.top.equalTo(valueLabel.snp.bottom)
            make.bottom.leading.trailing.equalToSuperview()
        }
    }
    
    // MARK: - UI property
    fileprivate lazy var valueLabel: UILabel = {
        let view = UILabel()
        view.textColor = UIColor.title
        view.font = UIFont.normal(size: 14)
        view.textAlignment = NSTextAlignment.center
        return view
    }()
    
    fileprivate lazy var nameLabel: UILabel = {
        let view = UILabel()
        view.textColor = UIColor.disable
        view.font = UIFont.normal(size: 10)
        view.textAlignment = NSTextAlignment.center
        return view
    }()
}
