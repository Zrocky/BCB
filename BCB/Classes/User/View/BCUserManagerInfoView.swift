//
//  BCUserManagerInfoView.swift
//  BCB
//
//  Created by Zrocky on 2018/4/18.
//  Copyright © 2018年 zrocky. All rights reserved.
//

import UIKit

protocol BCUserManagerInfoViewDelegate: class {
    func userManagerInfoView(didClick view: BCUserManagerInfoView)
}

class BCUserManagerInfoView: UIView {
    // MARK: - property
    var delegate: BCUserManagerInfoViewDelegate?
    var data: [String: Any]? {
        didSet {
            iconView.setImage(url: "https://i.pinimg.com/564x/04/09/d6/0409d6be0fb2e8ed188ea95938c8927f.jpg")
            nameLabel.text = "雕弓满月"
            postLabel.text = "帖子 512"
            followLabel.text = "粉丝 256"
            focusLabel.text = "关注 300"
        }
    }
    
    // MARK: - life cycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupSubviews()
        setupLayouts()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - event response
    func bgViewClick() {
        delegate?.userManagerInfoView(didClick: self)
    }
    
    // MARK: - public methods
    
    // MARK: - private methods
    private func setupSubviews() {
        addSubview(bgView)
        bgView.addSubview(iconView)
        bgView.addSubview(nameLabel)
        bgView.addSubview(postLabel)
        bgView.addSubview(followLabel)
        bgView.addSubview(focusLabel)
    }
    
    private func setupLayouts() {
        bgView.snp.makeConstraints { (make) in
            make.leading.top.equalTo(16)
            make.trailing.equalTo(-16)
            make.height.equalTo(110)
            make.bottom.equalTo(-16)
        }
        
        iconView.snp.makeConstraints { (make) in
            make.width.height.equalTo(78)
            make.leading.top.equalTo(16)
            make.bottom.equalTo(-16)
        }
        
        nameLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(iconView.snp.trailing).offset(16)
            make.top.equalTo(29)
        }
        
        postLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(iconView.snp.trailing).offset(16)
            make.bottom.equalTo(-29)
        }
        
        followLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(postLabel.snp.trailing).offset(16)
            make.bottom.equalTo(postLabel)
        }
        
        focusLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(followLabel.snp.trailing).offset(16)
            make.bottom.equalTo(postLabel)
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        iconView.layer.cornerRadius = min(iconView.width, iconView.height) / 2
    }
    
    // MARK: - UI property
    fileprivate lazy var bgView: UIButton = {
        let view = UIButton()
        view.addTarget(self, action: #selector(bgViewClick), for: UIControlEvents.touchUpInside)
        view.backgroundColor = UIColor.white
        view.layer.cornerRadius = 3
        view.layer.shadowColor = RGB(0xD1D1D1).cgColor
        view.layer.shadowOpacity = 1
        view.layer.shadowOffset = CGSize(width: 0, height: 0)
        return view
    }()
    
    fileprivate lazy var iconView: UIImageView = {
        let view = UIImageView()
        view.contentMode = UIViewContentMode.scaleAspectFill
        view.clipsToBounds = true
        return view
    }()
    
    fileprivate lazy var nameLabel: UILabel = {
        let view = UILabel()
        view.font = UIFont.bold(size: 18)
        view.textColor = UIColor.title
        return view
    }()
    
    fileprivate lazy var postLabel: UILabel = {
        let view = UILabel()
        view.textColor = UIColor.subTitle
        view.font = UIFont.normal(size: 13)
        return view
    }()
    
    fileprivate lazy var followLabel: UILabel = {
        let view = UILabel()
        view.textColor = UIColor.subTitle
        view.font = UIFont.normal(size: 13)
        return view
    }()
    
    fileprivate lazy var focusLabel: UILabel = {
        let view = UILabel()
        view.textColor = UIColor.subTitle
        view.font = UIFont.normal(size: 13)
        return view
    }()
}
