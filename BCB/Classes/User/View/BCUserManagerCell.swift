//
//  BCUserManagerCell.swift
//  BCB
//
//  Created by Zrocky on 2018/4/18.
//  Copyright © 2018年 zrocky. All rights reserved.
//

import UIKit

struct BCUserManagerCellItem {
    var icon: UIImage
    var title: String
    init(icon: UIImage, title: String) {
        self.icon = icon
        self.title = title
    }
}

class BCUserManagerCell: UITableViewCell {
    // MARK: - property
    var data: BCUserManagerCellItem? {
        didSet {
            iconView.image = data?.icon
            nameLabel.text = data?.title
        }
    }
    
    // MARK: - life cycle
    static func generateCell(tableView: UITableView) -> BCUserManagerCell {
        let reuseIdentifier = String(describing: self)
        var cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier) as? BCUserManagerCell
        if cell == nil {
            cell = BCUserManagerCell(style: UITableViewCellStyle.default, reuseIdentifier: reuseIdentifier)
        }
        assert(cell != nil, "cell can't be nil")
        
        return cell!
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupSubviews()
        setupLayouts()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - event response
    
    // MARK: - public methods
    
    // MARK: - private methods
    
    private func setupSubviews() {
        addSubview(iconView)
        addSubview(nameLabel)
    }
    
    private func setupLayouts() {
        iconView.snp.makeConstraints { (make) in
            make.leading.top.equalTo(16)
            make.bottom.equalTo(-16)
            make.width.height.equalTo(24)
        }
        
        nameLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(iconView.snp.trailing).offset(16)
            make.top.bottom.trailing.equalToSuperview()
        }

    }
    // MARK: - UI property
    fileprivate lazy var iconView: UIImageView = {
        let view = UIImageView()
        view.contentMode = UIViewContentMode.scaleAspectFit
        view.clipsToBounds = true
        return view
    }()
    
    fileprivate lazy var nameLabel: UILabel = {
        let view = UILabel()
        view.textColor = UIColor.subTitle
        view.font = UIFont.normal(size: 16)
        return view
    }()
}
