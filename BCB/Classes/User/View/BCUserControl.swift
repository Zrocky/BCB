//
//  BCUserControl.swift
//  BCB
//
//  Created by Zrocky on 2018/3/29.
//  Copyright © 2018年 zrocky. All rights reserved.
//

import UIKit

class BCUserControl: UIButton {
    // MARK: - property
    static let sharedInstance = BCUserControl()
    
    // MARK: - life cycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        layer.borderColor = UIColor.background.cgColor
        setBackgroundColor(UIColor.background, for: UIControlState.normal)
        imageView?.contentMode = UIViewContentMode.scaleAspectFill
        clipsToBounds = true
        addTarget(self, action: #selector(portraitViewDidClick), for: UIControlEvents.touchUpInside)
        setImage(url: "https://i.pinimg.com/564x/15/e4/c2/15e4c23f10695848874f9e437f1771f7.jpg",
                 for: UIControlState.normal)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - event response
    func portraitViewDidClick() {
        if let tabBarVC = window?.rootViewController as? UITabBarController {
            let focusVC = tabBarVC.childViewControllers[tabBarVC.selectedIndex] as? UINavigationController
            let umVC = BCUserManageController()
            umVC.hidesBottomBarWhenPushed = true
            focusVC?.pushViewController(umVC, animated: true)
        }
    }
    
    // MARK: - public methods
    
    // MARK: - private methods
    
    // MARK: - UI property

}
