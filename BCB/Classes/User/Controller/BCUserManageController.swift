//
//  BCUserManagerController.swift
//  BCB
//
//  Created by Zrocky on 2018/3/29.
//  Copyright © 2018年 zrocky. All rights reserved.
//

import UIKit

class BCUserManageController: UIViewController {
    // MARK: - property
    var items: [BCUserManagerCellItem] = [BCUserManagerCellItem(icon: #imageLiteral(resourceName: "CommentMe"), title: "评论我的"),
                                          BCUserManagerCellItem(icon: #imageLiteral(resourceName: "LikeMe"), title: "赞我的"),
                                          BCUserManagerCellItem(icon: #imageLiteral(resourceName: "NewFollow"), title: "新粉丝"),
                                          BCUserManagerCellItem(icon: #imageLiteral(resourceName: "SystemMsg"), title: "系统消息")]
    
    
    // MARK: - life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        
        setupSubviews()
        setupLayouts()
        
        tableView.tableHeaderView = infoView
        infoView.data = [:]        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setupNavi()
    }
    
    private func setupNavi() {
        navigationItem.title = "我"
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: settingBtn)
    }
    
    // MARK: - event response
    func settingBtnClick() {
        
    }
    
    // MARK: - public methods
    
    // MARK: - private methods
    private func setupSubviews() {
        view.addSubview(tableView)
    }
    
    private func setupLayouts() {
        tableView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    }
    
    // MARK: - UI property
    fileprivate lazy var settingBtn: UIButton = {
        let view = UIButton(type: UIButtonType.custom)
        view.addTarget(self, action: #selector(settingBtnClick), for: UIControlEvents.touchUpInside)
        view.setImage(#imageLiteral(resourceName: "Setting"), for: UIControlState.normal)
        view.imageView?.contentMode = UIViewContentMode.scaleAspectFit
        view.imageEdgeInsets = UIEdgeInsetsMake(4, 0, 4, 0)
        return view
    }()
    
    fileprivate lazy var tableView: UITableView = {
        let view = UITableView()
        view.backgroundColor = UIColor.background
        view.rowHeight = UITableViewAutomaticDimension
        view.separatorInset = UIEdgeInsets.zero
        view.separatorColor = UIColor.background
        view.dataSource = self
        view.delegate = self
        return view
    }()
    
    fileprivate lazy var infoView: BCUserManagerInfoView = {
        let view = BCUserManagerInfoView(frame: CGRect(x: 0, y: 0, width: Layout.width, height: 142))
        view.delegate = self
        return view
    }()
    
    fileprivate lazy var headerView: BCUserManageControllerHeaderView = {
        let view = BCUserManageControllerHeaderView()
        view.title = "我的消息"
        return view
    }()
}

// MARK: - BCUserManagerInfoViewDelegate
extension BCUserManageController: BCUserManagerInfoViewDelegate {
    func userManagerInfoView(didClick view: BCUserManagerInfoView) {
        let vc = BCUserController()
        navigationController?.pushViewController(vc, animated: true)
    }
}

// MARK: - UITableViewDataSource, UITableViewDelegate
extension BCUserManageController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = BCUserManagerCell.generateCell(tableView: tableView)
        cell.data = items[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 34
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

class BCUserManageControllerHeaderView: UIView {
    // MARK: - property
    var title: String? {
        didSet {
            titleLabel.text = title
        }
    }
    
    // MARK: - life cycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupSubviews()
        setupLayouts()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - event response
    
    // MARK: - public methods
    
    // MARK: - private methods
    private func setupSubviews() {
        addSubview(titleLabel)
    }
    
    private func setupLayouts() {
        titleLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(16)
            make.top.bottom.trailing.equalToSuperview()
        }
    }
    
    // MARK: - UI property
    fileprivate lazy var titleLabel: UILabel = {
        let view = UILabel()
        view.textColor = UIColor.content
        view.font = UIFont.normal(size: 14)
        return view
    }()
}
