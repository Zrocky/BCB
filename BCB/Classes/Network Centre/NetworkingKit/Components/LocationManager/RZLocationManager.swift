//
//  RZLocationManager.swift
//  YJZ
//
//  Created by Zrocky on 2017/7/13.
//  Copyright © 2017年 Zrocky. All rights reserved.
//

import UIKit
import CoreLocation

enum RZLocationManagerLocationServiceStatus {
    case `default`
    case ok
    case unknownError
    case unAvailable
    case noAuthorization
    case noNetwork
    case notDetermined
}

enum RZLocationManagerLocationResult {
    case `default`
    case locating
    case success
    case fail
    case paramsError
    case timeout
    case noNetwork
    case noContent
}

let RZLocationManagerDidUpdateLocationNotification = NSNotification.Name("RZLocationManagerDidUpdateLocationNotification")

class RZLocationManager: NSObject {
    // MARK: - property
    static let sharedInstance = RZLocationManager()
    fileprivate(set) var locationResult: RZLocationManagerLocationResult?
    fileprivate(set) var locationStatus: RZLocationManagerLocationServiceStatus?
    // WGS84
    fileprivate(set) var currentLocation: CLLocation?
    fileprivate(set) var currentCity: String?
    
    fileprivate lazy var locationManager: CLLocationManager = {
        let manager = CLLocationManager()
        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyBest
        return manager
    }()
    
    // MARK: - life cycle
    
    // MARK: - public methods
    func startLocation() {
        if checkLocationStatus() {
            locationResult = .locating
            locationManager.startUpdatingLocation()
        }else {
            failedLocation(resultType: .fail, statusType: locationStatus ?? .default)
        }
    }
    
    func stopLocation() {
        if checkLocationStatus() {
            locationManager.stopUpdatingLocation()
        }
    }
    
    func resetartLocation() {
        stopLocation()
        startLocation()
    }
    
    // MARK: - private methods
    fileprivate func failedLocation(resultType: RZLocationManagerLocationResult, statusType: RZLocationManagerLocationServiceStatus) {
        locationResult = resultType
        locationStatus = statusType
    }
    
    fileprivate func checkLocationStatus() -> Bool {
        var result = false
        let serviceEnable = locationServiceEnabled()
        let authorizationStatus = locationServiceStatus()
        if authorizationStatus == .ok && serviceEnable {
            result = true
        }else if authorizationStatus == .notDetermined {
            result = true
        }else {
            result = false
        }
        
        if serviceEnable && result {
            result = true
        }else {
            result = false
        }
        
        if result == false {
            failedLocation(resultType: .fail, statusType: locationStatus!)
        }
        
        return result
    }
    
    fileprivate func locationServiceEnabled() -> Bool {
        guard CLLocationManager.locationServicesEnabled() else {
            locationStatus = .unknownError
            return false
        }
        locationStatus = .ok
        return true
    }
    
    fileprivate func locationServiceStatus() -> RZLocationManagerLocationServiceStatus {
        locationStatus = .unknownError
        let serviceEnable = CLLocationManager.locationServicesEnabled()
        if serviceEnable {
            let authorizationStatus = CLLocationManager.authorizationStatus()
            switch authorizationStatus {
            case .notDetermined:
                locationStatus = .notDetermined
            case .authorizedAlways, .authorizedWhenInUse:
                locationStatus = .ok
            case .denied:
                locationStatus = .noAuthorization
            default:
                break
            }
        }else {
            locationStatus = .unAvailable
        }
        return locationStatus!
    }
    
    fileprivate func geoCity() {
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(currentLocation!) { (placemarks, error) in
            if error == nil {
                let placemark = placemarks?.first
                var city = placemark?.locality
                if city == nil {
                    city = placemark?.administrativeArea
                }
                self.currentCity = city
                NotificationCenter.default.post(name: RZLocationManagerDidUpdateLocationNotification, object: self, userInfo: nil)
            }
        }
    }
}

extension RZLocationManager: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        currentLocation = manager.location
        geoCity()
        stopLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        // if user not allow GPS location, don't return fail
        if locationStatus == .notDetermined {
            return
        }
        // if locating, don't notification out
        if locationResult == .locating {
            return
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == CLAuthorizationStatus.authorizedAlways || status == CLAuthorizationStatus.authorizedWhenInUse {
            locationStatus = RZLocationManagerLocationServiceStatus.ok
            resetartLocation()
        }else {
            if locationStatus != .notDetermined {
                failedLocation(resultType: .default, statusType: .noAuthorization)
            }else {
                locationManager.requestWhenInUseAuthorization()
                locationManager.startUpdatingLocation()
            }
        }
        
    }
}
