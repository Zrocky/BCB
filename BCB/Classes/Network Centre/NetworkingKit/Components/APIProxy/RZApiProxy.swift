//
//  RZApiProxy.swift
//  NetworkingKit
//
//  Created by Zrocky on 2017/6/28.
//  Copyright © 2017年 zrocky. All rights reserved.
//

import Alamofire

typealias RZCallback = (RZURLResponse) -> Void

class RZApiProxy {
    // MARK: - property
    static let sharedInstance = RZApiProxy()
    private lazy var dispatchTable = [Int: Any]()
    
    // MARK: - life cycle
    private init() {}
    
    
    // MARK: - public methods
    func callGET(params: Any, serviceIdentifier: String, methodName: String, success: @escaping RZCallback, fail: @escaping RZCallback) -> Int {
        let request = RZRequestGenerator.sharedInstance.generateGETRequest(serviceIdentifier: serviceIdentifier, requestParams: params, methodName: methodName)
        let requestID = callApi(request: request, success: success, fail: fail)
        return requestID
    }
    
    func callPOST(params: Any, serviceIdentifier: String, methodName: String, success: @escaping RZCallback, fail: @escaping RZCallback) -> Int {
        let request = RZRequestGenerator.sharedInstance.generatePOSTRequest(serviceIdentifier: serviceIdentifier, requestParams: params, methodName: methodName)
        let requestID = callApi(request: request, success: success, fail: fail)
        return requestID
    }
    
    func callPUT(params: Any, serviceIdentifier: String, methodName: String, success: @escaping RZCallback, fail: @escaping RZCallback) -> Int {
        let request = RZRequestGenerator.sharedInstance.generatePUTRequest(serviceIdentifier: serviceIdentifier, requestParams: params, methodName: methodName)
        let requestID = callApi(request: request, success: success, fail: fail)
        return requestID
    }
    
    func callDELETE(params: Any, serviceIdentifier: String, methodName: String, success: @escaping RZCallback, fail: @escaping RZCallback) -> Int {
        let request = RZRequestGenerator.sharedInstance.generateDELETERequest(serviceIdentifier: serviceIdentifier, requestParams: params, methodName: methodName)
        let requestID = callApi(request: request, success: success, fail: fail)
        return requestID
    }
    
    func cancelReuqest(requestID: Int) {
        let dataRequest = dispatchTable[requestID] as! DataRequest
        dataRequest.cancel()
        dispatchTable.removeValue(forKey: requestID)
    }
    
    func cancelRequest(requestIDList: [Int]) {
        for requestId in requestIDList {
            cancelReuqest(requestID: requestId)
        }
    }
    
    // MARK: - private methods
    fileprivate func callApi(request: URLRequest, success: @escaping RZCallback, fail: @escaping RZCallback) -> Int {
        var dataRequest: DataRequest?
        dataRequest = Alamofire.request(request).responseJSON { (response) in
            let requestIdentifier = dataRequest?.task?.taskIdentifier
            self.dispatchTable.removeValue(forKey: requestIdentifier!)
            let responseString = String(data: response.data!, encoding: String.Encoding.utf8) ?? ""

            switch response.result {
            case .success(_):
                RZLogger.logDebugInfo(response: response.response, responseString: responseString, responseData: response.data, request: request, error: nil)
                let urlResponse = RZURLResponse(responseString: responseString, requestID: requestIdentifier!, request: request, responseData: response.data, status: RZURLResponseErrorStatus.success)
                success(urlResponse)
                
            case .failure(let error):
                RZLogger.logDebugInfo(response: response.response, responseString: responseString, responseData: response.data, request: request, error: error)
                let urlResponse = RZURLResponse(responseString: responseString, requestID: requestIdentifier!, request: request, responseData: response.data, error: error)
                fail(urlResponse)
            }
        }
        let requestID = dataRequest?.task?.taskIdentifier
        dispatchTable[requestID!] = dataRequest
        
        return requestID!;
    }
}


