//
//  RZNetworkReachabilityManager.swift
//  YJZ
//
//  Created by Zrocky on 2017/7/18.
//  Copyright © 2017年 Zrocky. All rights reserved.
//

import Foundation
import Alamofire

let RZNetworkReachabilityNotification = NSNotification.Name(rawValue: "RZNetworkReachabilityNotification")

class RZNetworkReachabilityManager {
    typealias RZNetworkReachabilityStatus = NetworkReachabilityManager.NetworkReachabilityStatus
    
    // MARK: - property
    static let sharedInstance = RZNetworkReachabilityManager()
    private lazy var manager: NetworkReachabilityManager? = {
        let manager = NetworkReachabilityManager(host: "www.baidu.com")
        return manager
    }()
    var status: RZNetworkReachabilityStatus = .unknown
    var isReachable: Bool {
        return isReachableOnWWAN || isReachableOnEthernetOrWiFi
    }
    var isReachableOnWWAN: Bool {
        return status == .reachable(.wwan)
    }
    var isReachableOnEthernetOrWiFi: Bool {
        return status == .reachable(.ethernetOrWiFi)
    }
    
    // MARK: - life cycle
    private init() {}
    
    // MARK: - public methods
    func startListening() {
        manager?.listener = { status in
            self.status = status
            NotificationCenter.default.post(name: RZNetworkReachabilityNotification, object: nil, userInfo: nil)
        }
        manager?.startListening()
    }
    
    func stopListening() {
        manager?.stopListening()
    }
}
