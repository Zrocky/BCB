//
//  RZLogger.swift
//  NetworkingKit
//
//  Created by Zrocky on 2017/7/11.
//  Copyright © 2017年 zrocky. All rights reserved.
//

import Foundation


class RZLogger {
    // MARK: - property
    static let sharedInstance = RZLogger()
    private(set) var configParams: RZLoggerConfiguration?
    
    // MARK: - life cycle
    private init() {
        self.configParams = RZLoggerConfiguration()
    }
    
    // MARK: - publicMethod
    static func logDebugInfo(request: URLRequest, apiName: String, service: RZService, requestParams: Any, httpMethod: String) {
        #if DEBUG
            var logString = "\n\n**************************************************************\n*                       Request Start                        *\n**************************************************************\n\n"
            logString += "API Name:\t\t\(apiName)\n"
            logString += "Method:\t\t\t\(httpMethod)\n"
            logString += "Version:\t\t\(service.apiVersion)\n"
            logString += "Service:\t\t\(service.self)\n"
//            logString += "Public Key:\t\t\(service.publicKey)\n"
//            logString += "Private Key:\t\(service.privateKey)\n"
            logString += "Params:\n\(requestParams)"
            logString.appendURLRequest(request)
            logString += "\n\n**************************************************************\n*                         Request End                        *\n**************************************************************\n\n\n\n"
            print(logString)
        #endif
    }
    
    static func logDebugInfo(response: HTTPURLResponse?, responseString: String, responseData: Data?, request: URLRequest, error: Error?) {
        #if DEBUG
            var logString = "\n\n==============================================================\n=                        API Response                        =\n==============================================================\n\n"
            logString += "Status:\t\(error?._code ?? 200)\t(\(HTTPURLResponse.localizedString(forStatusCode: error?._code ?? 200)))\n\n"
            if responseData != nil {
                do {
                    let data = try JSONSerialization.jsonObject(with: responseData!, options: JSONSerialization.ReadingOptions.mutableContainers)
                    logString += "Content:\n\t\n\(data)\n"
                } catch {
                    // TODO:
                }
            }else {            
                logString += "Content:\n\t\n\(responseString)\n"
            }
            if let _ = error {
                logString += "Error Domain:\t\t\t\t\t\t\t\(error!._domain)\n"
                logString += "Error Domain Code:\t\t\t\t\t\t\(error!._code)\n"
                logString += "Error Localized Description:\t\t\t\(error!.localizedDescription)\n"
            }
            logString += "\n---------------  Related Request Content  --------------\n"
            logString.appendURLRequest(request)
            logString += "\n\n==============================================================\n=                        Response End                        =\n==============================================================\n\n\n\n"
            print(logString)
        #endif
    }
    
    static func logDebugInfo(cacheResponse response: RZURLResponse, methodName: String, service: RZService) {
        #if DEBUG
            var logString = "\n\n==============================================================\n=                      Cached Response                       =\n==============================================================\n\n"
            logString += "API Name:\t\t\(methodName)\n"
            logString += "Version:\t\t\(service.apiVersion)\n"
            logString += "Service:\t\t\(service.self)\n"
            logString += "Public Key:\t\t\(service.publicKey)\n"
            logString += "Private Key:\t\(service.privateKey)\n"
            logString += "Method Name:\t\(methodName)\n"
            logString += "Params:\n\(response.requestParams!)\n\n"
            logString += "Content:\n\t\(response.contentString!)\n\n"
            logString += "\n\n==============================================================\n=                        Response End                        =\n==============================================================\n\n\n\n"
            print(logString)
        #endif
    }
    
    func log(actionCode: String, params: [String: Any]) {
        var actionDict:[String: Any] = ["act": actionCode]
        actionDict += params
        actionDict += RZCommonParamGenerator.commonParamsDictionaryForLog()
        let logJsonDict: [String: Any] = [(configParams?.sendActionKey)!: ([actionDict].RZ_jsonString())]
        _ = RZApiProxy.sharedInstance.callPOST(params: logJsonDict, serviceIdentifier: (configParams?.serviceType)!, methodName: (configParams?.sendActionMethod)!, success:{ (response) in }, fail:{ (response) in })
    }
}
