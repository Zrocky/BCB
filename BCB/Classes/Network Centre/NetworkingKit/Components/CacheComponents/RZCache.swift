//
//  RZCache.swift
//  NetworkingKit
//
//  Created by Zrocky on 2017/7/12.
//  Copyright © 2017年 zrocky. All rights reserved.
//

import Foundation

class RZCache {
    // MARK: - proerty
    static let sharedInstance = RZCache()
    private lazy var cache: NSCache<AnyObject, AnyObject>? = {
        let cache = NSCache<AnyObject, AnyObject>()
        cache.countLimit = RZNetworkingConfigurationManager.sharedInstance.cacheCountLimit
        return cache
    }()
    
    // MARK: - public method
    func fetchCachedData(serviceIdentifier: String, methodName: String, requestParams: Any) -> Data? {
        let key = generateKey(serviceIdentifier: serviceIdentifier, methodName: methodName, requestParams: requestParams)
        return fetchCachedData(withKey: key)
    }
    
    func saveCache(cacheData: Data, serviceIdentifier: String, methodName: String, requestParams: Any) {
        let key = generateKey(serviceIdentifier: serviceIdentifier, methodName: methodName, requestParams: requestParams)
        saveCache(cacheData: cacheData, key: key)
    }
    
    func deleteCache(serviceIdentifier: String, methodName: String, requestParams: Any) {
        let key = generateKey(serviceIdentifier: serviceIdentifier, methodName: methodName, requestParams: requestParams)
        deleteCache(key: key)
    }
    
    func fetchCachedData(withKey: String) -> Data? {
        let cachedObject = cache?.object(forKey: withKey as AnyObject) as? RZCacheObject
        if cachedObject?.isOutdated ?? true || cachedObject?.isEmpty ?? true {
            return nil
        }else {
            return cachedObject?.content
        }
    }
    
    func saveCache(cacheData: Data, key: String) {
        var cacheObject = cache?.object(forKey: key as AnyObject) as? RZCacheObject
        if cacheObject == nil {
            cacheObject = RZCacheObject(content: cacheData)
        }
        cacheObject?.updateContent(content: cacheData)
        cache?.setObject(cacheObject!, forKey: key as AnyObject)
    }
    
    func deleteCache(key: String) {
        cache?.removeObject(forKey: key as AnyObject)
    }
    
    func clean() {
        cache?.removeAllObjects()
    }
    
    func generateKey(serviceIdentifier: String, methodName: String, requestParams: Any) -> String {
        let params = (requestParams as? [String: Any])?.RZ_urlParamsStringSignature(false)
        let key: String = "\(serviceIdentifier)\(methodName)\(params!)"
        return key
    }
}
