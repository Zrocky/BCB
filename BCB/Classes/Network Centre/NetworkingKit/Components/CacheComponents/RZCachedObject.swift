//
//  RZCachedObject.swift
//  NetworkingKit
//
//  Created by Zrocky on 2017/7/12.
//  Copyright © 2017年 zrocky. All rights reserved.
//

import Foundation

class RZCacheObject {
    // MARK: - porperty
    var lastUpdateTime: Date?
    var isEmpty: Bool {
        return content == nil
    }
    var isOutdated: Bool {
        get {            
            let timeInterval = Date().timeIntervalSince(lastUpdateTime!)
            return timeInterval > RZNetworkingConfigurationManager.sharedInstance.cacheOutdateTimeSeconds
        }
        
        set {
            self.isOutdated = newValue
        }
    }
    var content: Data? {
        didSet {
            lastUpdateTime = Date(timeIntervalSinceNow: 0)
        }
    }
    
    // MARK: - life cycle
    init(content: Data) {
        self.content = content;
    }
    
    // MARK: - public method
    func updateContent(content: Data) {
        self.content = content
    }
}
