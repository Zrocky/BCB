//
//  AppInfoFunction.swift
//  YJZ
//
//  Created by Zrocky on 2017/7/18.
//  Copyright © 2017年 Zrocky. All rights reserved.
//

import UIKit
import KeychainAccess

struct AppInfo {
    /**
     *  device name, eg: iPhone, iPod touch
     */
    static let device: String = UIDevice.current.model
    /**
     *  os version, eg: 10.3.2
     */
    static let systemVersion: String = UIDevice.current.systemVersion
    /**
     *  App Name
     */
    static let appName: String = Bundle.main.infoDictionary?["CFBundleDisplayName"] as? String ?? ""
    /**
     *  Project Name
     */
    static let projectName: String = Bundle.main.infoDictionary?["CFBundleExecutable"] as? String ?? ""
    /**
     *  App Bundle Identifier
     */
    static let bundleId: String = Bundle.main.bundleIdentifier ?? ""
    /**
     *  App version, eg: 3.0.0
     */
    static let version: String = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? ""
    /**
     *  App build version, eg: 18
     */
    static let build: String = Bundle.main.infoDictionary?["CFBundleVersion"] as? String ?? ""
    /**
     *  current time's timestamp, use ms
     */
    static var timestamp: String {
        return "\(Date().timeIntervalSince1970 * 1000)"
    }
    /**
     *  device uuid, save in keychain
     */
    static var uuid: String {
        let keychain = Keychain(service: Bundle.main.bundleIdentifier ?? "")
        let key = "\(Bundle.main.infoDictionary?["CFBundleExecutable"] as? String ?? "")UUID"
        if let olduuid = keychain[string: key] {
            return olduuid
        }else {
            let newuuid = UUID().uuidString
            keychain[string: key] = newuuid
            return newuuid
        }
    }
    
    
    /**
     *  check is iPhoneX
     */
    static var isiPhoneX: Bool {
        if UIDevice().userInterfaceIdiom == .phone && UIScreen.main.nativeBounds.height == 2436 {
            return true
        }
        return false
    }
}
