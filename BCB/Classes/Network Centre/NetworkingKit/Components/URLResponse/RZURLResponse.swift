//
//  RZURLResponse.swift
//  NetworkingKit
//
//  Created by Zrocky on 2017/6/28.
//  Copyright © 2017年 zrocky. All rights reserved.
//

import Foundation

enum RZURLResponseErrorStatus {
    case success
    case timeout
    case noNetwork
    case lostServerConnect
    case other(code: Int)
}

class RZURLResponse {
    // MARK: - property
    private(set) var status: RZURLResponseErrorStatus?
    private(set) var contentString: String?
    private(set) var content: Any?
    private(set) var requestID: Int = 0
    private(set) var request: URLRequest?
    private(set) var responseData: Data?
    var requestParams: Any?
    private(set) var error: Error?
    private(set) var isCache: Bool = false
    
    // MARK: - public method
    init(responseString: String, requestID: Int, request: URLRequest, responseData: Data?, status: RZURLResponseErrorStatus) {
        self.contentString = responseString
        do {
            self.content = try JSONSerialization.jsonObject(with: responseData!, options: .mutableContainers)
        } catch {
             // TODO:
        }
        self.status = status
        self.requestID = requestID
        self.request = request
        self.responseData = responseData
        self.requestParams = request.requestParams
        self.isCache = false
        self.error = nil
    }
    
    init(responseString: String, requestID: Int, request: URLRequest, responseData: Data?, error: Error) {
        self.contentString = responseString
        self.status = resposneStatus(error: error)
        self.requestID = requestID
        self.request = request
        self.responseData = responseData
        self.requestParams = request.requestParams
        self.isCache = false
        self.error = error
        if responseData != nil {
            do {
                self.content = try JSONSerialization.jsonObject(with: responseData!, options: .mutableContainers)
            } catch  {
                // TODO:
            }
        }else {
            self.content = nil
        }
    }
    
    init(data: Data) {
        self.contentString = String(data: data, encoding: String.Encoding.utf8)
        self.status = resposneStatus(error: nil)
        self.requestID = 0
        self.request = nil
        self.responseData = data
        do {
            self.content = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers)
        }catch {
            // TODO:
        }
        self.isCache = true
    }
    
    // MARK: - private method
    func resposneStatus(error: Error?) -> RZURLResponseErrorStatus {
        guard error != nil else { return RZURLResponseErrorStatus.success }
        guard error?._code != NSURLErrorTimedOut else { return RZURLResponseErrorStatus.timeout }
        guard error?._code != NSURLErrorCannotConnectToHost else { return RZURLResponseErrorStatus.lostServerConnect }
        guard error?._code != NSURLErrorNotConnectedToInternet else { return RZURLResponseErrorStatus.noNetwork }
        guard error?._code != NSURLErrorNetworkConnectionLost else { return RZURLResponseErrorStatus.noNetwork }
        return RZURLResponseErrorStatus.other(code: error?._code ?? 0)
    }
}
