//
//  RZAPIBaseManager.swift
//  NetworkingKit
//
//  Created by Zrocky on 2017/7/11.
//  Copyright © 2017年 zrocky. All rights reserved.
//

import Foundation

fileprivate let kRZAPIBaseManagerRequestID: String = "kRZAPIBaseManagerRequestID"

/**
 *  error Type is just for bussiness
 *  have more error not support at this stage
 *  eg: responseSerialized error
 */
enum RZAPIManagerErrorType: Equatable {
    case `default`
    case success
    case noContent
    case paramsError
    case timeout
    case noNetwork
    case lostServerConnect
    case businessError
    case other(code: Int)
    
    static func ==(lhs: RZAPIManagerErrorType, rhs: RZAPIManagerErrorType) -> Bool {
        switch (lhs, rhs) {
        case (.default, .default),
             (.success, .success),
             (.noContent, .noContent),
             (.paramsError, .paramsError),
             (.timeout, .timeout),
             (.noNetwork, .noNetwork),
             (.lostServerConnect, .lostServerConnect),
             (.businessError, .businessError):
            return true
        case let (.other(code: a), .other(code: b)):
            return a == b
        default:
            return false
        }
    }
}

enum RZAPIManagerRequestType: String {
    case GET
    case POST
    case PUT
    case DELETE
}

/**
 *  callBack for Api
 */
protocol RZAPIManagerCallBackDelegate: class {
    func managerCallAPIDidSuccess(_ manager: RZAPIBaseManager)
    func managerCallAPIDidFailed(_ manager: RZAPIBaseManager)
}

/**
 *  combination API Data
 */
protocol RZAPIManagerDataReformer {
    func manager(_ manager: RZAPIBaseManager, reformData: Any?) -> Any?
}

/**
 *  validator for callback data and request parameters
 */
protocol RZAPIManagerValidator: class {
    func manager(_ manager: RZAPIBaseManager, isCorrectCallBack data: Any) -> Bool
    func manager(_ manager: RZAPIBaseManager, isCorrectParams data: Any) -> Bool
}

/**
 *  return parameters for manager
 */
protocol RZAPIManagerParamSource: class {
    func paramsForAPI(_ manager: RZAPIBaseManager) -> Any
}

/**
 *  RZAPIBaseManager's subclass mast conform this protocol
 */
protocol RZAPIManager: class {
    func methodName() -> String
    func serviceType() -> String
    func requestType() -> RZAPIManagerRequestType
    func shouldCache() -> Bool
    func cleanData()
    func reformParams(_ params: Any) -> Any
    func loadData(_ params: Any) -> Int
    func shouldLoadFromNative() -> Bool
}

/**
 *  interceptor for API request and response process
 */
protocol RZAPIManagerInterceptor: class {
    func manager(_ manager: RZAPIBaseManager, beforePerformSuccess response: RZURLResponse) -> Bool
    func manager(_ manager: RZAPIBaseManager, afterPerformSuccess response: RZURLResponse)
    func manager(_ manager: RZAPIBaseManager, beforePerformFail response: RZURLResponse) -> Bool
    func manager(_ manager: RZAPIBaseManager, afterPerformFail response: RZURLResponse)
    func manager(_ manager: RZAPIBaseManager, shouldCallAPI params: Any) -> Bool
    func manager(_ manager: RZAPIBaseManager, afterCallingAPI params: Any)
}


open class RZAPIBaseManager: NSObject {
    // MARK: - porperty
    weak var delegate: RZAPIManagerCallBackDelegate?
    weak var paramSource: RZAPIManagerParamSource?
    weak var validator: RZAPIManagerValidator?
    weak var child: RZAPIManager?
    weak var interceptor: RZAPIManagerInterceptor?
    
    private(set) var errorMessage: String?
    private(set) var errorType: RZAPIManagerErrorType?
    private var fetchedRawData: Any?
    private var response: RZURLResponse?
    private lazy var requestIDList: [Int] = []
    private var cache: RZCache = RZCache.sharedInstance
    private var isNativeDataEmpty: Bool = false
    private var isReachable: Bool {
        let isReachability = RZNetworkingConfigurationManager.sharedInstance.isReachable()
        if !isReachability {
            errorType = RZAPIManagerErrorType.noNetwork
        }
        return isReachability
    }
    
    private var _isLoading: Bool?
    private var isLoading: Bool? {
        get {
            if requestIDList.count == 0 {
                return false
            }
            return _isLoading
        }
        set {
            _isLoading = newValue
        }
    }
    
    
    // MARK: - life cycle
    override init() {
        super.init()
        
        self.delegate = nil
        self.paramSource = nil
        self.validator = nil
        self.fetchedRawData = nil
        self.errorMessage = nil
        self.errorType = RZAPIManagerErrorType.default
        if let _ = self as? RZAPIManager {
            self.child = self as? RZAPIManager
        }
    }
    
    // MARK: - public method
    func cancelAllRequests() {
        RZApiProxy.sharedInstance.cancelRequest(requestIDList: requestIDList)
        requestIDList.removeAll()
    }
    
    func cancelRequest(requestID: Int) {
        removeRequestID(requestID)
        RZApiProxy.sharedInstance.cancelReuqest(requestID: requestID)
    }
    
    func fetchData(_ reformer: Any? = nil) -> Any? {
        // TODO: limit reformer conform RZAPIManagerDataReformer protocol
        // update: Swift 4.0 may be can fix
        guard reformer != nil && (reformer as? RZAPIManagerDataReformer) != nil  else {
            return fetchedRawData
        }
        return (reformer as! RZAPIManagerDataReformer).manager(self, reformData: fetchedRawData)
    }
    
    
    // MARK: - calling api
    func loadData() -> Int {
        let params = paramSource?.paramsForAPI(self)
        let requestID = loadData(params!)
        return requestID
    }
    
    func loadData(_ params: Any) -> Int {
        var requestID = 0
        var apiParams = reformParams(params) as! [String : Any]
        if shouldCallAPI(params: params) {
            guard validator?.manager(self, isCorrectParams: apiParams) ?? true else {
                failedOnCallingAPI(response: RZURLResponse(data: Data()), with: RZAPIManagerErrorType.paramsError)
                return requestID
            }
            if child!.shouldLoadFromNative() {
                loadDataFromNative()
            }
            
            if shouldCache() && hasCache(params: apiParams) {
                return 0
            }
            
            guard isReachable else {
                failedOnCallingAPI(response: RZURLResponse(data: Data()), with: RZAPIManagerErrorType.noNetwork)
                return requestID
            }
            
            isLoading = true
            callAPI(requestMethod: child!.requestType().rawValue, params: apiParams, requestID: &requestID)
            apiParams[kRZAPIBaseManagerRequestID] = requestID
            afterCallingAPI(params: params)
            return requestID
        }
        return requestID
    }
    
    // MARK: - api callbacks
    private func successOnCallingAPI(response: RZURLResponse) {
        self.isLoading = false
        self.response = response
        if child!.shouldLoadFromNative() && response.isCache == false {
            UserDefaults.standard.set(response.responseData, forKey: child!.methodName())
        }
        let serviceIdentifier = child?.serviceType()
        let service = RZServiceFactory.sharedInstance.service(serviceIdentifier!)
        guard service.child?.shouldCallBackBySucessedOnCallingAPI(response: response) ?? true else {
            failedOnCallingAPI(response: response, with: RZAPIManagerErrorType.businessError)
            return
        }
        
        if let _ = response.content {
            let service = RZServiceFactory.sharedInstance.service(child!.serviceType())
            if let handledResponse = (service as? RZServiceProtocol)?.handleResponse(response: response.content as Any) {
                fetchedRawData = handledResponse
            }else {
                fetchedRawData = response.content
            }
        }else {
            fetchedRawData = response.responseData
        }
        removeRequestID(response.requestID)
        if validator?.manager(self, isCorrectCallBack: response.content!) ?? true {
            if shouldCache() && !response.isCache {
                cache.saveCache(cacheData: response.responseData!, serviceIdentifier: child!.serviceType(), methodName: child!.methodName(), requestParams: response.requestParams!)
            }
            
            if beforePerformSuccess(response: response) {
                if child!.shouldLoadFromNative() {
                    if response.isCache {
                        delegate?.managerCallAPIDidSuccess(self)
                    }
                    if isNativeDataEmpty {
                        delegate?.managerCallAPIDidSuccess(self)
                    }
                }else {
                    delegate?.managerCallAPIDidSuccess(self)
                }
            }
            afterPerformSuccess(response: response)
        }else {
            failedOnCallingAPI(response: response, with: RZAPIManagerErrorType.noContent)
        }
    }
    
    private func failedOnCallingAPI(response: RZURLResponse, with errorType: RZAPIManagerErrorType) {
        let serviceIdentifier = child?.serviceType()
        let service = RZServiceFactory.sharedInstance.service(serviceIdentifier!)
        self.isLoading = false
        self.response = response
        
        guard service.child?.shouldCallBackByFailedOnCallingAPI(response: response) ?? true else {
            return
        }
        self.errorType = errorType
        self.errorMessage = RZNetworkingConfigurationManager.sharedInstance.errorMessage(type: errorType, response: response)
        removeRequestID(response.requestID)
        if response.content != nil {
            self.fetchedRawData = response.content
        }else {
            self.fetchedRawData = response.responseData
        }
        if beforePerformFail(response: response) {
            delegate?.managerCallAPIDidFailed(self)
        }
        afterPerformFail(response: response)
    }
    
    // MARK: - method for interceptor
    func beforePerformSuccess(response: RZURLResponse) -> Bool {
        errorType = RZAPIManagerErrorType.success
        guard self != (interceptor as? RZAPIBaseManager) else {
            return true
        }
        return interceptor?.manager(self, beforePerformSuccess: response) ?? true
    }
    
    func afterPerformSuccess(response: RZURLResponse) {
        if self != (interceptor as? RZAPIBaseManager) {
            interceptor?.manager(self, afterPerformSuccess: response)
        }
    }
    
    func beforePerformFail(response: RZURLResponse) -> Bool {
        guard self != (interceptor as? RZAPIBaseManager) else {
            return true
        }
        return interceptor?.manager(self, beforePerformFail: response) ?? true
    }
    
    func afterPerformFail(response: RZURLResponse) {
        if self != (interceptor as? RZAPIBaseManager) {
            interceptor?.manager(self, afterPerformFail: response)
        }
    }
    
    func shouldCallAPI(params: Any) -> Bool {
        guard self != (interceptor as? RZAPIBaseManager) else {
            return true
        }
        return interceptor?.manager(self, shouldCallAPI: params) ?? true
    }
    
    func afterCallingAPI(params: Any) {
        if self != (interceptor as? RZAPIBaseManager) {
            interceptor?.manager(self, afterCallingAPI: params)
        }
    }
    
    // MARK: - method for child
    func reformParams(_ params: Any) -> Any {
        let childIMP = (child as? NSObject)?.method(for: #selector(reformParams(_:)))
        let selfIMP = self.method(for: #selector(reformParams(_:)))
        if childIMP == selfIMP {
            return params
        }else {
            if let result = child?.reformParams(params) {
                return result
            }else {
                return params
            }
        }
    }
    
    func shouldCache() -> Bool {
        return RZNetworkingConfigurationManager.sharedInstance.shouldCache
    }
    
    func shouldLoadFromNative() -> Bool {
        return false
    }
    
    func handleResponseData(_ responseData: Any) -> Any {
        return responseData
    }
    
    func cleanData() {
        cache.clean()
        fetchedRawData = nil
        errorMessage = nil
        errorType = .default
    }
    
    // MARK: - private method
    func removeRequestID(_ requestID: Int) {
        for storedRequestID in requestIDList {
            if storedRequestID == requestID {
                requestIDList.remove(at: requestIDList.index(of: storedRequestID)!)
            }
        }
    }
    
    func hasCache(params: Any) -> Bool {
        let serviceIdentifier = child?.serviceType()
        let methodName = child?.methodName()
        let result = cache.fetchCachedData(serviceIdentifier: serviceIdentifier!, methodName: methodName!, requestParams: params)
        guard result != nil else {
            return false
        }
        DispatchQueue.main.async {
            let response = RZURLResponse(data: result!)
            response.requestParams = params
            let service = RZServiceFactory.sharedInstance.service(serviceIdentifier!)
            RZLogger.logDebugInfo(cacheResponse: response, methodName: methodName!, service: service)
            self.successOnCallingAPI(response: response)
        }
        return true
    }
    
    func loadDataFromNative() {
        
        let data = UserDefaults.standard.data(forKey: child!.methodName())
        var result: Any?
        do {
            result = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)
        } catch {
            // TODO:
        }
        
        if result != nil {
            isNativeDataEmpty = false
            DispatchQueue.main.async {
                var data: Data?
                do {
                    data = try JSONSerialization.data(withJSONObject: result!, options: JSONSerialization.WritingOptions.prettyPrinted)
                }catch {
                    // TODO:
                }
                let response = RZURLResponse(data: data!)
                self.successOnCallingAPI(response: response)
            }
        }else {
            isNativeDataEmpty = true
        }
    }
    
    private func handleResponseError(response: RZURLResponse) -> RZAPIManagerErrorType {
        let status = response.status
        switch status {
        case .success?:
            return RZAPIManagerErrorType.success
        case .noNetwork?:
            return RZAPIManagerErrorType.noNetwork
        case .timeout?:
            return RZAPIManagerErrorType.timeout
        case .lostServerConnect?:
            return RZAPIManagerErrorType.lostServerConnect
        case .other(code: let value)?:
            return RZAPIManagerErrorType.other(code: value)
        default:
            break
        }
        return RZAPIManagerErrorType.success
    }
    
    private func callAPI(requestMethod: String, params: Any, requestID: inout Int) {
        var requestID:Int = 0
        switch requestMethod {
        case RZAPIManagerRequestType.GET.rawValue:
            requestID = RZApiProxy.sharedInstance.callGET(params: params, serviceIdentifier: self.child!.serviceType(), methodName: self.child!.methodName(), success: { (response) in
                self.successOnCallingAPI(response: response)
            }, fail: { (response) in
                self.failedOnCallingAPI(response: response, with: self.handleResponseError(response: response))
            })
        case RZAPIManagerRequestType.POST.rawValue:
            requestID = RZApiProxy.sharedInstance.callPOST(params: params, serviceIdentifier: self.child!.serviceType(), methodName: self.child!.methodName(), success: { (response) in
                self.successOnCallingAPI(response: response)
            }, fail: { (response) in
                self.failedOnCallingAPI(response: response, with: self.handleResponseError(response: response))
            })
        case RZAPIManagerRequestType.PUT.rawValue:
            requestID = RZApiProxy.sharedInstance.callPUT(params: params, serviceIdentifier: self.child!.serviceType(), methodName: self.child!.methodName(), success: { (response) in
                self.successOnCallingAPI(response: response)
            }, fail: { (response) in
                self.failedOnCallingAPI(response: response, with: self.handleResponseError(response: response))
            })
        case RZAPIManagerRequestType.POST.rawValue:
            requestID = RZApiProxy.sharedInstance.callDELETE(params: params, serviceIdentifier: self.child!.serviceType(), methodName: self.child!.methodName(), success: { (response) in
                self.successOnCallingAPI(response: response)
            }, fail: { (response) in
                self.failedOnCallingAPI(response: response, with: self.handleResponseError(response: response))
            })
        default:
            break
        }
        requestIDList.append(requestID)
    }
}
