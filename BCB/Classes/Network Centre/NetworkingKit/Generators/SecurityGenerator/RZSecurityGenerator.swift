//
//  RZSecurityGenerator.swift
//  YJZ
//
//  Created by Zrocky on 2017/7/21.
//  Copyright © 2017年 Zrocky. All rights reserved.
//

import UIKit

struct RZSecurityGenerator {
    static func encrypt(_ string: String, key: String) -> String {
        let md5Str = string.MD5()
        let middleStr = md5Str + string
        let cipherData = middleStr.DESEncrypt(key: key)
        let base64Str = cipherData.base64EncodedString(options: Data.Base64EncodingOptions(rawValue: 0))
        return base64Str
    }
    
    static func decrypt(_ string: String, key: String) -> String {
        let base64Data = Data(base64Encoded: string, options: Data.Base64DecodingOptions(rawValue: 0))
        let cypherData = base64Data!.DESDecrypt(key: key)
        guard let cypherStr = String(data: cypherData, encoding: .utf8) else {
            return ""
        }
        let clearStr = cypherStr.substring(from: cypherStr.index(cypherStr.startIndex, offsetBy: 32))
        return clearStr
    }
}
