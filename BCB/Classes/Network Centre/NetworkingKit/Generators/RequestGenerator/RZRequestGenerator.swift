//
//  RZRequestGenerator.swift
//  NetworkingKit
//
//  Created by Zrocky on 2017/7/10.
//  Copyright © 2017年 zrocky. All rights reserved.
//

import UIKit
import Alamofire

class RZRequestGenerator {
    // MARK: - property
    static let sharedInstance = RZRequestGenerator()
    
    // MARK: - public method
    func generateGETRequest(serviceIdentifier: String, requestParams: Any, methodName: String) -> URLRequest {
        return generateRequest(serviceIdentifier: serviceIdentifier, requestParams: requestParams as! [String : Any], methodName: methodName, requestMethod: "GET")
    }
    
    func generatePOSTRequest(serviceIdentifier: String, requestParams: Any, methodName: String) -> URLRequest {
        return generateRequest(serviceIdentifier: serviceIdentifier, requestParams: requestParams as! [String : Any], methodName: methodName, requestMethod: "POST")
    }
    
    func generatePUTRequest(serviceIdentifier: String, requestParams: Any, methodName: String) -> URLRequest {
        return generateRequest(serviceIdentifier: serviceIdentifier, requestParams: requestParams as! [String : Any], methodName: methodName, requestMethod: "PUT")
    }
    
    func generateDELETERequest(serviceIdentifier: String, requestParams:  Any, methodName: String) -> URLRequest {
        return generateRequest(serviceIdentifier: serviceIdentifier, requestParams: requestParams as! [String : Any], methodName: methodName, requestMethod: "DELETE")
    }
    
    private func generateRequest(serviceIdentifier: String, requestParams: [String: Any], methodName: String, requestMethod: String) -> URLRequest {
        let service = RZServiceFactory.sharedInstance.service(serviceIdentifier)
        let urlString = (service as! RZServiceProtocol).urlGenerateingRule(methodName: methodName)
        let totalParams = totalRequestParams(service: service, requestParams: requestParams)
        var request: URLRequest?
        do {
            request = try URLRequest(url: URL(string: urlString)!, method: HTTPMethod(rawValue: requestMethod)!)
            request?.timeoutInterval = RZNetworkingConfigurationManager.sharedInstance.apiNetworkingTimeoutSeconds
            
            if let dict = service.child?.extraHttpHeadParams(methodName: methodName) {
                for (key, value) in dict {
                    request?.setValue(value as? String, forHTTPHeaderField: key)
                }
            }
            request?.setValue("application/json", forHTTPHeaderField: "Content-Type")
            
            var requestData: Data?
            if requestMethod != "GET" && RZNetworkingConfigurationManager.sharedInstance.shouldSetParamsInHTTPBodyButGET {
                requestData = try JSONSerialization.data(withJSONObject: requestParams, options: JSONSerialization.WritingOptions(rawValue: 0))
            }else {
                requestData = (service as! RZServiceProtocol).encryptParams(params: totalParams)
            }
            request?.httpBody = requestData
        } catch {
            // TODO:
            printLog(error)

        }
        request?.requestParams = totalParams
        RZLogger.logDebugInfo(request: request!, apiName:methodName, service: service, requestParams: totalParams, httpMethod: requestMethod)
        return request!
    }
    
    // MARK: - private method
    func totalRequestParams(service: RZService, requestParams: [String: Any]) -> [String: Any] {
        var totalRequestParams = service.child?.handleBussinessParams(params: requestParams) ?? requestParams
        if let extraParams = service.child?.extraParams() {
            for (key, value) in extraParams {
                totalRequestParams[key] = value
            }
        }
        return totalRequestParams
    }
}
