//
//  RZSignatureGenerator.swift
//  YJZ
//
//  Created by Zrocky on 2017/7/13.
//  Copyright © 2017年 Zrocky. All rights reserved.
//

import UIKit

class RZSignatureGenerator {
    // MARK: - public methods
    func signGet(sigParams: [String: Any], methodName: String, apiVersion: String, privateKey: String, publicKey: String) -> String {
        let sigString = sigParams.RZ_urlParamsStringSignature(true)
        let originalResult = "\(sigString)\(privateKey)"
        return originalResult.MD5()
    }
    
    func signRestfulGet(allParams: [String: Any], methodName: String, apiVersion: String, privateKey: String) -> String {
        let part1 = "\(apiVersion)\(methodName)"
        let part2 = allParams.RZ_urlParamsStringSignature(true)
        let part3 = privateKey
        let beforeSign = "\(part1)\(part2)\(part3)"
        return beforeSign.MD5()
    }
    
    func signPost(apiParams: [String: Any], privateKey: String, publicKey: String) -> String {
        var sigParams = apiParams
        sigParams["api_key"] = publicKey
        let sigString = sigParams.RZ_urlParamsStringSignature(true)
        let originalResult = "\(sigString)\(privateKey)"
        return originalResult.MD5()
    }
    
    func signRestfulPOST(apiParams: Any, commonParams: [String: Any], methodName: String, apiVersion: String, privateKey: String) -> String {
        let part1 = "\(apiVersion)\(methodName)"
        let part2 = commonParams.RZ_urlParamsStringSignature(true)
        var part3:Any?
        if apiParams is [String: Any] {
            part3 = (apiParams as! [String: Any]).RZ_jsonString()
        }else if apiParams is [Any] {
            part3 = (apiParams as! [Any]).RZ_jsonString()
        }else {
            return ""
        }
        let part4 = privateKey
        let beforeSign = "\(part1)\(part2)\(part3!)\(part4)"
        return beforeSign.MD5()
    }
}
