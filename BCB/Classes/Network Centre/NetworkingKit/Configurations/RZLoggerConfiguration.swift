//
//  RZLoggerConfiguration.swift
//  NetworkingKit
//
//  Created by Zrocky on 2017/7/11.
//  Copyright © 2017年 zrocky. All rights reserved.
//

import Foundation

class RZLoggerConfiguration {
    var channelID: String?
    var appKey: String?
    var logAppName: String?
    var serviceType: String?
    var sendLogMethod: String?
    var sendActionMethod: String?
    var sendLogkey: String?
    var sendActionKey: String?
}
