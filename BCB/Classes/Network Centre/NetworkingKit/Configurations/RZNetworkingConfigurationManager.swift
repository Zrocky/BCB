//
//  RZNetworkingConfigurationManager.swift
//  NetworkingKit
//
//  Created by Zrocky on 2017/7/10.
//  Copyright © 2017年 zrocky. All rights reserved.
//

import Foundation

class RZNetworkingConfigurationManager {
    // MARK: - property
    static let sharedInstance = RZNetworkingConfigurationManager()
    let shouldCache = false
    let apiNetworkingTimeoutSeconds = 20.0
    let cacheOutdateTimeSeconds = 300.0
    let cacheCountLimit = 1000
    let shouldSetParamsInHTTPBodyButGET = false
    let serviceIsOnline = true

    
    // MARK: - life cycle
    private init() {}
    
    // MARK: - public method
    func isReachable() -> Bool {
        if RZNetworkReachabilityManager.sharedInstance.status == .unknown {
            return true
        }else {
            return RZNetworkReachabilityManager.sharedInstance.isReachable
        }
    }
    
    func errorMessage(type: RZAPIManagerErrorType, response: RZURLResponse?) -> String {
        switch type {
        case .default:
            return "请求掉入了黑洞"
        case .success:
            return "请求成功"
        case .noContent:
            return "返回数据为空"
        case .paramsError:
            return "参数错误"
        case .timeout:
            return "请求超时"
        case .noNetwork:
            return "网络好像不太好"
        case .lostServerConnect:
            return "失去服务器连接"
        case .other(let code):
            return "出现异常 \(code)"
        case .businessError:
            guard let content = response?.content as? [String: Any] else {
                return "服务器异常返回"
            }
            if let errorMessage = content["errorInfo"] as? String {
                return errorMessage
            }
            return "服务器异常"
        }
    }
    
}
