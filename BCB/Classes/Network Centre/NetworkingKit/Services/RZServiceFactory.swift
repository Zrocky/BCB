//
//  RZServiceFactory.swift
//  NetworkingKit
//
//  Created by Zrocky on 2017/7/10.
//  Copyright © 2017年 zrocky. All rights reserved.
//

import Foundation

protocol RZServiceFactoryDataSource: class {
    func servicesKindsOfServiceFactory() -> [String: String]
}

class RZServiceFactory {
    // MARK: - property
    static let sharedInstance = RZServiceFactory()
    weak var dataSource: RZServiceFactoryDataSource?
    private var serviceStorage = [String: Any]()
    
    // MARK: - life cycle
    private init() {}
    
    // MARK: - public methods
    func service(_ identifier: String) -> RZService {
        if serviceStorage[identifier] == nil {
            serviceStorage[identifier] = newService(identifier)
        }
        return serviceStorage[identifier] as! RZService
    }
    
    // MARK: - private methods
    private func newService(_ identifier: String) -> RZService? {
        if let classStr = dataSource?.servicesKindsOfServiceFactory()[identifier] {
            let projectName = Bundle.main.infoDictionary!["CFBundleExecutable"] as! String
            let serviceClass = NSClassFromString("\(projectName)." + classStr) as! RZService.Type
            let service = serviceClass.init()
            return service
        }else {
            return nil;
        }
        
    }
    
    
}
