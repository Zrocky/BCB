//
//  RZService.swift
//  NetworkingKit
//
//  Created by Zrocky on 2017/7/10.
//  Copyright © 2017年 zrocky. All rights reserved.
//

import UIKit

protocol RZServiceProtocol: class {
    var isOnline: Bool { get }
    
    var offlineApiBaseUrl: String { get }
    var onlineAPIBaseUrl: String { get }
    
    var offlineApiVersion: String { get }
    var onlineApiVersion: String { get }
    
    var offlinePublicKey: String { get }
    var onlinePublicKey: String { get }
    
    var onlinePrivateKey: String { get }
    var offlinePrivateKey: String { get }
    
    // Service handle bussiness params to totalParams
    func handleBussinessParams(params: [String: Any]) -> [String: Any]
    // Service need insert extra Params to totalParams
    func extraParams() -> [String: Any]
    // Service need insert extra HTTPToken, eg: accessToken
    func extraHttpHeadParams(methodName: String) -> [String: Any]
    // rule of Generateing URL
    func urlGenerateingRule(methodName: String) -> String
    // Interceptor handler Service error, eg: token invaild need post notification
    func shouldCallBackByFailedOnCallingAPI(response: RZURLResponse) -> Bool
    // Interceptor handler Business error, eg: has business need post notification
    func shouldCallBackBySucessedOnCallingAPI(response: RZURLResponse) -> Bool
    // Service handle response
    func handleResponse(response: Any) -> Any
    // Encrypt params
    func encryptParams(params: [String: Any]) -> Data
    // Decrypt response
    func decryptResponse(response: String) -> Any
}

class RZService {
    // MARK: - property
    weak var child: RZServiceProtocol?
    var privateKey: String {
        return ((child?.isOnline)! ? child?.onlinePrivateKey : child?.offlinePrivateKey)!
    }
    var publicKey: String {
        return ((child?.isOnline)! ? child?.onlinePublicKey : child?.offlinePublicKey)!
    }
    var apiBaseUrl: String {
        return ((child?.isOnline)! ? child?.onlineAPIBaseUrl : child?.offlineApiBaseUrl)!
    }
    var apiVersion: String {
        return ((child?.isOnline)! ? child?.onlineApiVersion : child?.offlineApiVersion)!
    }
    
    // MARK: - lify cycle
    required init() {
        if let _ = self as? RZServiceProtocol {
            self.child = self as? RZServiceProtocol
        }
    }
}
