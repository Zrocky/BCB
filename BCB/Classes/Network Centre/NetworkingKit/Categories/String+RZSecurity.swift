//
//  String+RZSecurity.swift
//  YJZ
//
//  Created by Zrocky on 2017/7/20.
//  Copyright © 2017年 Zrocky. All rights reserved.
//

import Foundation

extension String {
    func MD5() -> String {
        let messageData = self.data(using: .utf8)!
        var digesData = Data(count: Int(CC_MD5_DIGEST_LENGTH))
        _ = digesData.withUnsafeMutableBytes{ (digestBytes)  in
            messageData.withUnsafeBytes{ (messageBytes)  in
                CC_MD5(messageBytes, CC_LONG(messageData.count), digestBytes)
            }
        }
        let md5Hex = digesData.map { String(format: "%02hhx", $0) }.joined()
        return md5Hex
    }
    
    func DESEncrypt(key: String) -> Data {
        let clearStr = self
        let options = kCCOptionPKCS7Padding | kCCOptionECBMode
        let cypher = SymmetricCryptor(algorithm: .des, options: options)
        do {
            let cypherData = try cypher.crypt(string: clearStr, key: key)
            return cypherData
        }catch {
            assert(false, "Unable to perform cyphering on given text. \(error). Try enabling PKCS7 padding.")
        }
        return Data()
    }
}

extension Data {
    func DESDecrypt(key: String) -> Data {
        let cypherData =  self
        let options = kCCOptionPKCS7Padding | kCCOptionECBMode
        let cypher = SymmetricCryptor(algorithm: .des, options: options)
        do {
            let clearData = try cypher.decrypt(cypherData, key: key)
            return clearData
        } catch {
            assert(false, "Unable to perform decyphering on given text. \(error)")
        }
        return Data()
    }
}
