//
//  String+RZNetworkingMethods.swift
//  NetworkingKit
//
//  Created by Zrocky on 2017/7/11.
//  Copyright © 2017年 zrocky. All rights reserved.
//

import Foundation

extension String {
    mutating func appendURLRequest(_ request: URLRequest) {
        self += "\n\nHTTP URL:\n\t\(request.url!)"
        let header: Any = request.allHTTPHeaderFields ?? "\t\t\t\t\tN/A"
        self += "\n\nHTTP Header:\n\(header)"
        var body:String?
        if let httpBody = request.httpBody {
            body = String(data: httpBody, encoding: String.Encoding.utf8) ?? "\t\t\t\tN/A"
        }else {
            body = "\t\t\t\tN/A"
        }
        self += "\n\nHTTP Body:\n\t\(body!)"
    }
    
}
