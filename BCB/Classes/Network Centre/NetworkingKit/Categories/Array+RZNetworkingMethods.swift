//
//  Array+RZNetworkingMethods.swift
//  NetworkingKit
//
//  Created by Zrocky on 2017/7/11.
//  Copyright © 2017年 zrocky. All rights reserved.
//

import Foundation

extension Array {
    func RZ_paramsString() -> String {
        var paramString = ""
        
        for obj in self {
            if paramString.characters.count == 0 {
                paramString += "\(obj)"
            }else {
                paramString += "&\(obj)"
            }
        }
        return paramString
    }
    
    func RZ_jsonString() -> String {
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: self, options: JSONSerialization.WritingOptions.prettyPrinted)
            return String(data: jsonData, encoding: String.Encoding.utf8)!
        } catch {
            // TODO:
            return ""
        }
        
    }
}
