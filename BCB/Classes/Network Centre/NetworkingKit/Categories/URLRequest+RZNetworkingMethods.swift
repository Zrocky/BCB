//
//  URLRequest+RZNetworkingMethods.swift
//  NetworkingKit
//
//  Created by Zrocky on 2017/7/10.
//  Copyright © 2017年 zrocky. All rights reserved.
//

import Foundation

private var RZNetworkingRequestParams: [String: Any]?

extension URLRequest {
    var requestParams: [String: Any]? {
        get {
            return objc_getAssociatedObject(self, &RZNetworkingRequestParams) as? [String: Any]
        }
        
        set {
            objc_setAssociatedObject(self, &RZNetworkingRequestParams, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
        
    }
}
