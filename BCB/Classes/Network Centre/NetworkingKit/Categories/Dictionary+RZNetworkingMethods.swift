//
//  Dictionary+RZNetworkingMethods.swift
//  NetworkingKit
//
//  Created by Zrocky on 2017/7/11.
//  Copyright © 2017年 zrocky. All rights reserved.
//

import Foundation

/**
 *  syntactic sugar for join two dictionary
 */
func += <K, V> ( left: inout [K:V], right: [K:V]) {
    for (k, v) in right {
        left[k] = v
    }
}

extension Dictionary {
    func RZ_urlParamsStringSignature(_ isForSignature: Bool) -> String {
        let sortedArray = RZ_transformerUrlParamsArraySignature(isForSignature: isForSignature)
        return sortedArray.RZ_paramsString()
    }
    
    func RZ_jsonString() -> String {
        var jsonData: Data?
        do {
            jsonData = try JSONSerialization.data(withJSONObject: self, options: JSONSerialization.WritingOptions.prettyPrinted)
        } catch {
            // TODO:
        }
        return String(data: jsonData!, encoding: String.Encoding.utf8)!
    }
    
    func RZ_transformerUrlParamsArraySignature(isForSignature: Bool) -> [String] {
        var result: [String] = []
        for (k, v) in self {
            var obj: Any = v
            if !(obj is String) {
                obj = "\(obj)"
            }
            if !isForSignature {
                var allowed = CharacterSet.alphanumerics
                allowed.insert(charactersIn: "-._~")
                obj = (obj as! String).addingPercentEncoding(withAllowedCharacters: allowed)!
            }
            if (obj as! String).characters.count > 0 {
                result.append("\(k)=\(obj)")
            }
        }
        return result
    }
}
