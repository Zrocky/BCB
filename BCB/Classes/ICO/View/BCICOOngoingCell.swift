//
//  BCICOOngoingCell.swift
//  BCB
//
//  Created by Zrocky on 2018/4/8.
//  Copyright © 2018年 zrocky. All rights reserved.
//

import UIKit

class BCICOOngoingCell: UITableViewCell {
    // MARK: - property
    var data: [String: Any]? {
        didSet {
            iconView.setImage(url: "https://img.bishijie.com/coinpic-bitcoin.jpg?imageMogr2/thumbnail/100x")
            titleLabel.text = "PolySwarm"
            subtitleLabel.text = "安全"
            let progress: Float = 46.35
            progressLabel.text = "\(progress)%"
            progressView.progress = progress / 100
        }
    }
    
    // MARK: - life cycle
    static func generateCell(tableView: UITableView) -> BCICOOngoingCell {
        let reuseIdentifier = String(describing: self)
        var cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier) as? BCICOOngoingCell
        if cell == nil {
            cell = BCICOOngoingCell(style: UITableViewCellStyle.default, reuseIdentifier: reuseIdentifier)
        }
        assert(cell != nil, "cell can't be nil")
        
        return cell!
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupSubviews()
        setupLayouts()
        
        contentView.layoutIfNeeded()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - event response
    
    // MARK: - public methods
    
    // MARK: - private methods
    
    private func setupSubviews() {
        contentView.addSubview(iconView)
        contentView.addSubview(titleLabel)
        contentView.addSubview(subtitleLabel)
        contentView.addSubview(progressLabel)
        contentView.addSubview(progressView)
    }
    
    private func setupLayouts() {
        iconView.snp.makeConstraints { (make) in
            make.leading.equalTo(16)
            make.top.equalTo(8)
            make.bottom.equalTo(-8)
            make.width.equalTo(contentView.snp.height).offset(-16)
        }
        
        titleLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(iconView.snp.trailing).offset(8)
            make.top.equalTo(6)
        }
        
        subtitleLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(titleLabel)
            make.top.equalTo(titleLabel.snp.bottom)
        }
        
        progressView.snp.makeConstraints { (make) in
            make.trailing.equalTo(-16)
            make.height.equalTo(6)
            make.width.equalTo(52)
            make.bottom.equalTo(-18)
        }
        
        progressLabel.snp.makeConstraints { (make) in
            make.trailing.equalTo(-16)
            make.bottom.equalTo(progressView.snp.top)
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        iconView.layer.cornerRadius = min(iconView.width, iconView.height) / 2
        progressView.layer.cornerRadius = min(progressView.width, progressView.height) / 2
    }
    
    // MARK: - UI property
    fileprivate lazy var iconView: UIImageView = {
        let view = UIImageView()
        view.clipsToBounds = true
        return view
    }()
    
    fileprivate lazy var titleLabel: UILabel = {
        let view = UILabel()
        view.font = UIFont.bold(size: 16)
        view.textColor = UIColor.title
        return view
    }()
    
    fileprivate lazy var subtitleLabel: UILabel = {
        let view = UILabel()
        view.font = UIFont.normal(size: 12)
        view.textColor = UIColor.disable
        return view
    }()
    
    fileprivate lazy var progressLabel: UILabel = {
        let view = UILabel()
        view.font = UIFont.normal(size: 14)
        view.textColor = UIColor.subTitle
        view.textAlignment = NSTextAlignment.right
        return view
    }()
    
    fileprivate lazy var progressView: UIProgressView = {
        let view = UIProgressView()
        view.progressTintColor = UIColor.darkBlue
        view.trackTintColor = UIColor.white
        view.layer.borderColor = UIColor.darkBlue.cgColor
        view.layer.borderWidth = 1
        view.clipsToBounds = true
        return view
    }()
}
