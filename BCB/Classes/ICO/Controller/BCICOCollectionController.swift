//
//  BCICOCollectionController.swift
//  BCB
//
//  Created by Zrocky on 2018/4/9.
//  Copyright © 2018年 zrocky. All rights reserved.
//

import UIKit

class BCICOCollectionController: UIViewController {
    // MARK: - property
    
    // MARK: - life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSubviews()
        setupLayouts()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setupNavi()
    }
    
    private func setupNavi() {
        navigationItem.title = "收藏"
    }
    
    // MARK: - event response
    
    // MARK: - public methods
    
    // MARK: - private methods
    private func setupSubviews() {
        view.addSubview(tableView)
    }
    
    private func setupLayouts() {
        tableView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    }
    
    // MARK: - UI property
    fileprivate lazy var tableView: UITableView = {
        let view = UITableView()
        view.backgroundColor = UIColor.background
        view.dataSource = self
        view.delegate = self
        let tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: Layout.width, height: 8))
        tableHeaderView.backgroundColor = UIColor.background
        view.tableHeaderView = tableHeaderView
        return view
    }()
}

// MARK: - UITableViewDataSource, UITableViewDelegate
extension BCICOCollectionController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 20
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = BCICOCollectionCell.generateCell(tableView: tableView)
        cell.data = [:]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 49
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // TODO
    }
}
