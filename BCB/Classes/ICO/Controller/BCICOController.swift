//
//  BCICOController.swift
//  BCB
//
//  Created by Zrocky on 2018/3/27.
//  Copyright © 2018年 zrocky. All rights reserved.
//

import UIKit

class BCICOController: UIViewController {
    // MARK: - property
    
    // MARK: - life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        
        setupSubviews()
        setupLayouts()
        
        if let firstVC = childContentControllers.first {
            addContentControler(firstVC, to: contentView)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setupNavi()
    }
    
    private func setupNavi() {
        navigationItem.title = "新币"
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: portraitView)
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: collectionBtn)
    }
    
    // MARK: - event response
    func collectionBtnClick() {
        let vc = BCICOCollectionController()
        vc.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(vc, animated: true)
    }
    
    // MARK: - public methods
    
    // MARK: - private methods
    private func setupSubviews() {
        view.addSubview(categoryMenu)
        view.addSubview(contentView)
    }
    
    private func setupLayouts() {
        portraitView.snp.makeConstraints { (make) in
            make.width.height.equalTo(34)
        }
        
        collectionBtn.snp.makeConstraints { (make) in
            make.width.height.equalTo(34)
        }
        
        categoryMenu.snp.makeConstraints { (make) in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(Layout.top)
            make.height.equalTo(40)
        }
        
        contentView.snp.makeConstraints { (make) in
            make.top.equalTo(categoryMenu.snp.bottom)
            make.leading.trailing.bottom.equalToSuperview()
        }
    }
    
    // MARK: - UI property
    fileprivate lazy var portraitView = BCUserControl.sharedInstance
    fileprivate lazy var collectionBtn: UIButton = {
        let view = UIButton(type: UIButtonType.custom)
        view.imageView?.contentMode = UIViewContentMode.scaleAspectFit
        view.setImage(#imageLiteral(resourceName: "Collection"), for: UIControlState.normal)
        view.addTarget(self, action: #selector(collectionBtnClick), for: UIControlEvents.touchUpInside)
        return view
    }()
    
    fileprivate lazy var categoryMenu: RZCategoryMenu = {
        let view = RZCategoryMenu(items: ["进行中", "即将发行", "已完结"])
        view.backgroundColor = UIColor.barBackground
        view.delegate = self
        return view
    }()
    
    fileprivate lazy var childContentControllers = [BCICOOngoingController(),
                                                    BCICOComingController(),
                                                    BCICOEndController()]
    
    fileprivate lazy var contentView: UIView = {
        let view = UIView()
        return view
    }()
    
}

extension BCICOController: RZCategoryMenuDelegate {
    func categoryMenu(_ menu: RZCategoryMenu, selectIndex: Int) {
        if let preVC = childViewControllers.first {
            removeContentController(preVC)
        }
        if childContentControllers.indices.contains(selectIndex) {
            addContentControler(childContentControllers[selectIndex], to: contentView)
        }
    }
}
