//
//  BCICODetailController.swift
//  BCB
//
//  Created by Zrocky on 2018/4/18.
//  Copyright © 2018年 zrocky. All rights reserved.
//

import UIKit

class BCICODetailController: UIViewController {
    // MARK: - property
    fileprivate var selectedCategoryMenuIndex: Int = 0
    fileprivate var tableviewContentOffSetYs: [CGFloat] = []
    fileprivate var tableviewCellHightDicts: [[Int: CGFloat]] = [[:], [:], [:], [:], [:]]
    
    // MARK: - life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSubviews()
        setupLayouts()
        
        tableView.tableHeaderView = headerView
        tableView.layoutIfNeeded()
        headerView.data = [:]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setupNavi()
    }
    
    private func setupNavi() {
        navigationItem.title = "SyncFab"
    }
    
    // MARK: - event response
    
    // MARK: - public methods
    
    // MARK: - private methods
    private func setupSubviews() {
        view.addSubview(tableView)
        view.addSubview(toolbar)
        view.addSubview(websiteBtn)
    }
    
    private func setupLayouts() {
        websiteBtn.snp.makeConstraints { (make) in
            make.leading.bottom.equalToSuperview()
            make.height.equalTo(40)
            make.width.equalTo(96)
        }
        toolbar.snp.makeConstraints { (make) in
            make.leading.equalTo(websiteBtn.snp.trailing)
            make.trailing.equalToSuperview()
            make.height.bottom.equalTo(websiteBtn)
        }
        
        tableView.snp.makeConstraints { (make) in
            make.top.leading.trailing.equalToSuperview()
            make.bottom.equalTo(toolbar.snp.top)
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        guard tableviewContentOffSetYs.count == categoryMenu.items.count else {
            for _ in 0..<categoryMenu.items.count {
                tableviewContentOffSetYs.append(tableView.contentOffset.y)
            }
            return
        }
    }
    
    // MARK: - UI property
    fileprivate lazy var tableView: UITableView = {
        let view = UITableView()
        view.separatorStyle = UITableViewCellSeparatorStyle.none
        view.backgroundColor = UIColor.background
        view.allowsSelection = false
        view.dataSource = self
        view.delegate = self
        return view
    }()
    
    fileprivate lazy var headerView: BCICODetailDataView = {
        let view = BCICODetailDataView(frame: CGRect(x: 0, y: 0, width: 0, height: 82))
        return view
    }()
    
    fileprivate lazy var categoryMenu: RZCategoryMenu = {
        let view = RZCategoryMenu(items: ["简况", "讨论", "新闻", "公告", "研报"])
        view.backgroundColor = UIColor.white
        view.delegate = self
        return view
    }()
    
    fileprivate lazy var toolbar: BCTickerCoinDetailToolbar = {
        let view = BCTickerCoinDetailToolbar(items: [BCTickerCoinDetailToolbarItem(icon: #imageLiteral(resourceName: "comment"), title: "发帖"),
                                                     BCTickerCoinDetailToolbarItem(icon: #imageLiteral(resourceName: "comment"), title: "收藏"),
                                                     BCTickerCoinDetailToolbarItem(icon: #imageLiteral(resourceName: "comment"), title: "分享")])
        view.layer.shadowColor = UIColor.shadow.cgColor
        view.layer.shadowOpacity = 1
        view.layer.shadowOffset = CGSize(width: 0, height: -2)
        return view
    }()
    
    fileprivate lazy var websiteBtn: UIButton = {
        let view = UIButton(type: UIButtonType.custom)
        view.setTitle("项目官网", for: UIControlState.normal)
        view.titleLabel?.font = UIFont.bold(size: 14)
        view.setBackgroundColor(UIColor.darkBlue, for: UIControlState.normal)
        return view
    }()
}

extension BCICODetailController: RZCategoryMenuDelegate {
    func categoryMenu(_ menu: RZCategoryMenu, selectIndex: Int) {
        let topOffSetY = (tableView.tableHeaderView?.height ?? 0) - Layout.top
        tableviewContentOffSetYs[selectedCategoryMenuIndex] = tableView.contentOffset.y
        selectedCategoryMenuIndex = selectIndex
        printLog("previous offsets \(tableviewContentOffSetYs)")
        printLog("tableview contentOffSet \(tableView.contentOffset.y)")
        if tableView.contentOffset.y >= topOffSetY && tableviewContentOffSetYs[selectedCategoryMenuIndex] < topOffSetY {
            // 当前category的列表偏移量已经到了顶部，将其它category列表中未到顶部的，全部置顶
            tableviewContentOffSetYs[selectedCategoryMenuIndex] = topOffSetY
        }else if tableView.contentOffset.y < topOffSetY {
            // 当前category的列表未到顶部，将所有category列表，均按当前列表偏移量做偏移
            let contentOffSetY = tableView.contentOffset.y > -Layout.top ? tableView.contentOffset.y : -Layout.top
            tableviewContentOffSetYs[selectedCategoryMenuIndex] = contentOffSetY
        }
        printLog("after offsets \(tableviewContentOffSetYs)")
        tableView.reloadData()
        tableView.layoutIfNeeded()
        tableView.setContentOffset(CGPoint(x: 0, y: tableviewContentOffSetYs[selectedCategoryMenuIndex]), animated: false)
    }
}
extension BCICODetailController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == tableView {
            printLog("scroll view contentoffsetY \(scrollView.contentOffset.y)")
        }
    }
}

// MARK: - UITableViewDataSource, UITableViewDelegate
extension BCICODetailController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch selectedCategoryMenuIndex {
        case 0:
            return 1
        case 1:
            return 50
        case 2:
            return 20
        case 3:
            return 40
        case 4:
            return 80
        default:
            break
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch selectedCategoryMenuIndex {
        case 0:
            let cell = BCICODetailBriefView.generateCell(tableView: tableView)
            cell.data = [:]
            return cell
        case 1:
            let cell = BCMomentsDiscussCell.generateCell(tableView: tableView)
            cell.data = ["BCOrder": indexPath.row]
            return cell
        case 2:
            let cell = BCMomentsNewsCell.generateCell(tableView: tableView)
            cell.data = ["BCOrder": indexPath.row]
            return cell
        case 3:
            let cell = BCMomentsNewsCell.generateCell(tableView: tableView)
            cell.data = ["BCOrder": indexPath.row]
            return cell
        case 4:
            let cell = BCMomentsNewsCell.generateCell(tableView: tableView)
            cell.data = ["BCOrder": indexPath.row]
            return cell
        default:
            break
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        tableviewCellHightDicts[selectedCategoryMenuIndex][indexPath.row] = cell.height
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if let height = tableviewCellHightDicts[selectedCategoryMenuIndex][indexPath.row] {
            return height
        }
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return categoryMenu
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //        tableView.deselectRow(at: indexPath, animated: true)
        //
        //        let vc = BCMomentsDetailController()
        //        vc.type = BCMomentsDetailControllerType.news
        //        vc.hidesBottomBarWhenPushed = true
        //        navigationController?.pushViewController(vc, animated: true)
    }
}
