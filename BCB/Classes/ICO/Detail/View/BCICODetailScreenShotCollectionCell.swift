//
//  BCICODetailScreenShotCollectionCell.swift
//  BCB
//
//  Created by Zrocky on 2018/4/18.
//  Copyright © 2018年 zrocky. All rights reserved.
//

import UIKit
import SnapKit

class BCICODetailScreenShotCollectionCell: UICollectionViewCell {
    // MARK: - property
    var data: [String: Any]? {
        didSet {
            imageView.setImage(url: "https://icodrops.com/wp-content/uploads/2018/03/Terms.png") { (image, error, cacheType, url) in
                let ratio = (image?.size.height ?? 1) / (image?.size.width ?? 1)
//                self.layoutIfNeeded()
                self.imageView.snp.makeConstraints({ (make) in
                    let imageWidth = Layout.width - 32
                    self.heightConstraint?.deactivate()
                    self.heightConstraint = make.height.equalTo(imageWidth * ratio).constraint
                })
            }
        }
    }
    fileprivate var heightConstraint: Constraint?
    
    // MARK: - life cycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupSubviews()
        setupLayouts()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - event response
    
    // MARK: - public methods
    
    // MARK: - private methods
    private func setupSubviews() {
        contentView.addSubview(bgView)
        contentView.addSubview(imageView)
    }
    
    private func setupLayouts() {
        bgView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
            make.width.equalTo(Layout.width-32)
        }
        imageView.snp.makeConstraints { (make) in
            make.leading.equalTo(16)
            make.trailing.equalTo(-16)
            make.top.bottom.equalToSuperview()
            heightConstraint = make.height.equalTo(0).constraint
        }
    }
    
    // MARK: - UI property
    fileprivate lazy var bgView: UIView = {
        let view = UIView()
        return view
    }()
    
    fileprivate lazy var imageView: UIImageView = {
        let view = UIImageView()
        view.contentMode = UIViewContentMode.scaleAspectFill
        return view
    }()
}
