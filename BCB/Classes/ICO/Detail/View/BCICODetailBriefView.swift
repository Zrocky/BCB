//
//  BCICODetailBriefView.swift
//  BCB
//
//  Created by Zrocky on 2018/4/18.
//  Copyright © 2018年 zrocky. All rights reserved.
//

import UIKit
import SnapKit

class BCICODetailBriefView: UITableViewCell {
    // MARK: - property
    var data: [String: Any]? {
        didSet {
            iconView.setImage(url: "https://icodrops.com/wp-content/uploads/2018/03/BitRewards-logo-150x150.jpg")
            nameLabel.text = "BitRewards"
            descTableView.reloadData()
            screenShotCollectionView.reloadData()
            descTableView.layoutIfNeeded()
            screenShotCollectionView.layoutIfNeeded()
            descTableViewHeightConstrait?.deactivate()
            screenShotViewHeightConstrait?.deactivate()
            descTableView.snp.makeConstraints { (make) in
                descTableViewHeightConstrait = make.height.equalTo(descTableView.contentSize.height).constraint
            }
            screenShotCollectionView.snp.makeConstraints { (make) in
                screenShotViewHeightConstrait = make.height.equalTo(screenShotCollectionView.contentSize.height).constraint
            }
        }
    }
    
    fileprivate var descTableViewHeightConstrait: Constraint?
    fileprivate var screenShotViewHeightConstrait: Constraint?
    
    // MARK: - life cycle
    static func generateCell(tableView: UITableView) -> BCICODetailBriefView {
        let reuseIdentifier = String(describing: self)
        var cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier) as? BCICODetailBriefView
        if cell == nil {
            cell = BCICODetailBriefView(style: UITableViewCellStyle.default, reuseIdentifier: reuseIdentifier)
        }
        assert(cell != nil, "cell can't be nil")
        
        return cell!
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = UIColor.background
        
        setupSubviews()
        setupLayouts()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - event response
    
    // MARK: - public methods
    
    // MARK: - private methods
    private func setupSubviews() {
        addSubview(projectView)
        projectView.addSubview(iconView)
        projectView.addSubview(nameLabel)
        addSubview(websiteBtn)
        addSubview(whitebookBtn)
        addSubview(socialLabel)
        addSubview(socialCollectionView)
        addSubview(descLabel)
        addSubview(descTableView)
        addSubview(screenShotLabel)
        addSubview(screenShotCollectionView)
    }
    
    private func setupLayouts() {
        projectView.snp.makeConstraints { (make) in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(8)
        }
        
        iconView.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.top.equalTo(16)
            make.width.height.equalTo(88)
        }
        
        nameLabel.snp.makeConstraints { (make) in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(iconView.snp.bottom)
            make.bottom.equalTo(-16)
        }
        
        websiteBtn.snp.makeConstraints { (make) in
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(36)
            make.top.equalTo(projectView.snp.bottom).offset(1)
                        make.width.equalToSuperview().multipliedBy(0.5).offset(-1)
        }
        
        whitebookBtn.snp.makeConstraints { (make) in
            make.leading.equalTo(websiteBtn.snp.trailing).offset(1)
            make.top.height.width.equalTo(websiteBtn)
        }
        
        socialLabel.snp.makeConstraints { (make) in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(websiteBtn.snp.bottom).offset(8)
            make.height.equalTo(40)
        }
        
        socialCollectionView.snp.makeConstraints { (make) in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(socialLabel.snp.bottom).offset(1)
            make.height.equalTo(64)
        }
        
        descLabel.snp.makeConstraints { (make) in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(socialCollectionView.snp.bottom).offset(8)
            make.height.equalTo(40)
        }
        
        descTableView.snp.makeConstraints { (make) in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(descLabel.snp.bottom).offset(1)
            descTableViewHeightConstrait = make.height.equalTo(200).constraint
        }
        
        screenShotLabel.snp.makeConstraints { (make) in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(descTableView.snp.bottom).offset(8)
            make.height.equalTo(40)
        }
        
        screenShotCollectionView.snp.makeConstraints { (make) in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(screenShotLabel.snp.bottom)
            screenShotViewHeightConstrait = make.height.equalTo(200).constraint
            make.bottom.equalToSuperview()
        }
        
    }
    
    // MARK: - UI property
    fileprivate lazy var projectView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        return view
    }()
    
    fileprivate lazy var iconView: UIImageView = {
        let view = UIImageView()
        view.clipsToBounds = true
        view.contentMode = UIViewContentMode.scaleAspectFit
        return view
    }()
    
    fileprivate lazy var nameLabel: UILabel = {
        let view = UILabel()
        view.textColor = UIColor.title
        view.font = UIFont.bold(size: 20)
        view.textAlignment = NSTextAlignment.center
        return view
    }()
    
    fileprivate lazy var websiteBtn: UIButton = {
        let view = UIButton(type: UIButtonType.custom)
        view.setBackgroundColor(UIColor.white, for: UIControlState.normal)
        view.setTitleColor(UIColor.darkBlue, for: UIControlState.normal)
        view.titleLabel?.font = UIFont.bold(size: 14)
        view.setTitle("白名单(开放)", for: UIControlState.normal)
        return view
    }()
    
    fileprivate lazy var whitebookBtn: UIButton = {
        let view = UIButton(type: UIButtonType.custom)
        view.setBackgroundColor(UIColor.white, for: UIControlState.normal)
        view.setTitleColor(UIColor.darkBlue, for: UIControlState.normal)
        view.titleLabel?.font = UIFont.bold(size: 14)
        view.setTitle("白皮书", for: UIControlState.normal)
        return view
    }()
    
    fileprivate lazy var socialLabel: BCTickerCoinDetailBriefViewHeaderView = {
        let view = BCTickerCoinDetailBriefViewHeaderView()
        view.backgroundColor = UIColor.white
        view.title = "社交媒体"
        return view
    }()
    
    fileprivate lazy var socialCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.estimatedItemSize = CGSize(width: 64, height: 64)
        let view = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        view.backgroundColor = UIColor.background
        let reuseIdentifier = String(describing: BCTickerCoinDetailBriefSocialCell.self)
        view.register(BCTickerCoinDetailBriefSocialCell.self, forCellWithReuseIdentifier: reuseIdentifier)
        view.dataSource = self
        view.delegate = self
        return view
    }()
    
    fileprivate lazy var descLabel: BCTickerCoinDetailBriefViewHeaderView = {
        let view = BCTickerCoinDetailBriefViewHeaderView()
        view.backgroundColor = UIColor.white
        view.title = "项目介绍"
        return view
    }()
    
    fileprivate lazy var descTableView: UITableView = {
        let view = UITableView()
        view.backgroundColor = UIColor.background
        view.separatorInset = UIEdgeInsets.zero
        view.separatorColor = UIColor.background
        view.delegate = self
        view.dataSource = self
        return view
    }()
    
    fileprivate lazy var screenShotLabel: BCTickerCoinDetailBriefViewHeaderView = {
        let view = BCTickerCoinDetailBriefViewHeaderView()
        view.backgroundColor = UIColor.white
        view.title = "屏幕截图"
        return view
    }()
    
    fileprivate lazy var screenShotCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.estimatedItemSize = CGSize(width: 44, height: 44)
        let view = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        view.backgroundColor = UIColor.white
        let reuseIdentifier = String(describing: BCICODetailScreenShotCollectionCell.self)
        view.register(BCICODetailScreenShotCollectionCell.self, forCellWithReuseIdentifier: reuseIdentifier)
        view.dataSource = self
        view.delegate = self
        return view
    }()
}

// MARK: - UICollectionViewDataSource & UICollectionViewDelegate
extension BCICODetailBriefView: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == socialCollectionView {
            return 4
        }else if collectionView == screenShotCollectionView {
            return 7
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == socialCollectionView {
            let reuseIdentifier = String(describing: BCTickerCoinDetailBriefSocialCell.self)
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! BCTickerCoinDetailBriefSocialCell
            cell.data = [:]
            return cell
        }else if collectionView == screenShotCollectionView {
            let reuseIdentifier = String(describing: BCICODetailScreenShotCollectionCell.self)
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! BCICODetailScreenShotCollectionCell
            cell.data = [:]
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == socialCollectionView {
            
        }else if collectionView == screenShotCollectionView {
            
        }
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension BCICODetailBriefView: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if collectionView == socialCollectionView {
            
        }else if collectionView == screenShotCollectionView {
            return UIEdgeInsetsMake(0, 0, 20, 0)
        }
        return UIEdgeInsets.zero
    }
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        if collectionView == socialCollectionView {
//            return CGSize(width: (collectionView.width - 3) / 4, height: 64)
//        }else if collectionView == screenShotCollectionView {
//            return CGSize(width: Layout.width-32, height: CGFloat.greatestFiniteMagnitude)
//        }
//        return CGSize.zero
//    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}


// MARK: - UITableViewDataSource, UITableViewDelegate
extension BCICODetailBriefView: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 15
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = BCTickerCoinDetailBriefDescCell.generateCell(tableView: tableView)
        cell.data = [:]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}
