//
//  BCICODetailDataView.swift
//  BCB
//
//  Created by Zrocky on 2018/4/18.
//  Copyright © 2018年 zrocky. All rights reserved.
//

import UIKit

class BCICODetailDataView: UIView {
    // MARK: - property
    var data: [String: Any]? {
        didSet {
            priceLabel.text = "$11,131,471"
            totalLabel.text = "目标金额 $24,100,000"
            progressLabel.text = "完成度 46%"
            statusLabel.text = "状态 进行中"
            endDateLabel.text = "结束日期 3天后"
            typeLabel.text = "类型 区块链服务"
        }
    }
    
    // MARK: - life cycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupSubviews()
        setupLayouts()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - event response
    
    // MARK: - public methods
    
    // MARK: - private methods
    private func setupSubviews() {
        addSubview(contentView)
        contentView.addSubview(totalLabel)
        contentView.addSubview(priceLabel)
        contentView.addSubview(progressLabel)
        contentView.addSubview(statusLabel)
        contentView.addSubview(endDateLabel)
        contentView.addSubview(typeLabel)
    }
    
    private func setupLayouts() {
        contentView.snp.makeConstraints { (make) in
            make.leading.top.trailing.equalToSuperview()
            make.bottom.equalTo(-8)
        }
        
        priceLabel.snp.makeConstraints { (make) in
            make.top.equalTo(10)
            make.leading.equalTo(16)
            make.trailing.equalTo(progressLabel.snp.leading)
        }
        
        totalLabel.snp.makeConstraints { (make) in
            make.top.equalTo(priceLabel.snp.bottom)
            make.trailing.equalTo(progressLabel.snp.leading)
            make.leading.equalTo(16)
        }
        
        progressLabel.snp.makeConstraints { (make) in
            make.top.equalTo(18)
            make.trailing.equalTo(endDateLabel.snp.leading)
            make.width.equalToSuperview().multipliedBy(0.25)
        }
        
        statusLabel.snp.makeConstraints { (make) in
            make.width.equalTo(progressLabel)
            make.trailing.equalTo(endDateLabel.snp.leading)
            make.bottom.equalTo(-18)
        }
        
        endDateLabel.snp.makeConstraints { (make) in
            make.trailing.equalTo(-16)
            make.width.equalToSuperview().multipliedBy(0.3)
            make.top.equalTo(18)
        }
        
        typeLabel.snp.makeConstraints { (make) in
            make.trailing.width.equalTo(endDateLabel)
            make.bottom.equalTo(-18)
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        contentView.drawGradient(colors: [UIColor.darkPurple, UIColor.darkBlue], startPoint: CGPoint(x: 0, y: 0), endPoint: CGPoint(x: 0, y: 1))
    }
    
    // MARK: - UI property
    fileprivate lazy var contentView: UIView = {
        let view = UIView()
        return view
    }()
    
    
    fileprivate lazy var priceLabel: UILabel = {
        let view = UILabel()
        view.textColor = UIColor.white
        view.font = UIFont.bold(size: 24)
        return view
    }()
    
    fileprivate lazy var totalLabel: UILabel = {
        let view = UILabel()
        view.textColor = UIColor.white
        view.font = UIFont.normal(size: 12)
        return view
    }()
    
    fileprivate lazy var progressLabel: UILabel = {
        let view = UILabel()
        view.textColor = UIColor.white
        view.font = UIFont.normal(size: 12)
        return view
    }()
    
    fileprivate lazy var statusLabel: UILabel = {
        let view = UILabel()
        view.textColor = UIColor.white
        view.font = UIFont.normal(size: 12)
        return view
    }()
    
    fileprivate lazy var endDateLabel: UILabel = {
        let view = UILabel()
        view.textColor = UIColor.white
        view.font = UIFont.normal(size: 12)
        return view
    }()
    
    fileprivate lazy var typeLabel: UILabel = {
        let view = UILabel()
        view.textColor = UIColor.white
        view.font = UIFont.normal(size: 12)
        return view
    }()
}
