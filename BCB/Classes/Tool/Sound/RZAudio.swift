//
//  RZAudio.swift
//  YJZ
//
//  Created by Zrocky on 2017/11/27.
//  Copyright © 2017年 Zrocky. All rights reserved.
//

import AVFoundation

class RZAudio {
    // MARK: - property
    static let sharedInstance = RZAudio()
    private var player: AVAudioPlayer?
    
    // MARK: - public methods
    func playSound(name: String) {
        guard let url = Bundle.main.url(forResource: name, withExtension: nil) else { return }
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            try AVAudioSession.sharedInstance().setActive(true)
            player = try AVAudioPlayer(contentsOf: url)
            player?.numberOfLoops = -1;
            guard let player = player else { return }
            player.play()
        } catch let error {
            print(error.localizedDescription)
        }
    }
    
    func stop() {
        player?.stop()
    }
}

