//
//  RZWechat.swift
//  YJZ
//
//  Created by Zrocky on 2017/11/23.
//  Copyright © 2017年 Zrocky. All rights reserved.
//

import UIKit
import Alamofire

struct RZWechat {
    // MARK: - public method
    static func userinfo(appid:String, secret: String, code: String, success: @escaping (_ userinfo: [String: Any]?)->Void, failure: @escaping (_ error: Error?)->Void) {
        getWechatAuth(appid: appid, secret: secret, code: code, success: success, failure: failure)
    }
    
    // MARK: - private method
    fileprivate static func getWechatAuth(appid:String, secret: String, code: String, success: @escaping (_ userinfo: [String: Any]?)->Void, failure: @escaping (_ error: Error?)->Void) {
        let url = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=\(appid)&secret=\(secret)&code=\(code)&grant_type=authorization_code"
        Alamofire.request(url, method: HTTPMethod.get).responseJSON { (response) in
            switch response.result {
            case .success(_):
                if let data = response.data {
                    do {
                        let dict = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? [String : Any]
                        let token = dict?["access_token"] as? String ?? ""
                        let openid = dict?["openid"] as? String ?? ""
                        getWechatUserinfo(token: token, openid: openid, success: success, failure: failure)
                    }catch let error {
                        failure(error)
                    }
                }else {
                    failure(nil)
                }
            case .failure(let error):
                failure(error)
            }
        }
    }
    
    fileprivate static func getWechatUserinfo(token: String, openid: String, success: @escaping (_ userinfo: [String: Any]?)->Void, failure: @escaping (_ error: Error?)->Void) {
        let url = "https://api.weixin.qq.com/sns/userinfo?access_token=\(token)&openid=\(openid)"
        Alamofire.request(url, method: HTTPMethod.get).responseJSON { (response) in
            switch response.result {
            case .success(_):
                if let data = response.data {
                    do {
                        let dict = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? [String : Any]
                        success(dict)
                    }catch let error {
                        failure(error)
                    }
                }else {
                    failure(nil)
                }
            case .failure(let error):
                failure(error)
            }
        }
    }
    
  
}
