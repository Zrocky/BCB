//
//  RZAlertBackgroundController.swift
//  YJZ
//
//  Created by Zrocky on 2017/6/28.
//  Copyright © 2017年 Zrocky. All rights reserved.
//

import UIKit

class RZAlertBackgroundController: UIViewController {
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return UIApplication.shared.statusBarStyle
    }
}
