//
//  BCTickerCoinCell.swift
//  BCB
//
//  Created by Zrocky on 2018/4/4.
//  Copyright © 2018年 zrocky. All rights reserved.
//

import UIKit

class BCTickerCoinCell: UITableViewCell {
    // MARK: - property
    var data: [String: Any]? {
        didSet {
            iconView.setImage(url: "https://img.bishijie.com/coinpic-bitcoin.jpg?imageMogr2/thumbnail/100x")
            titleLabel.text = "BTC"
            subtitleLabel.text = "比特币"
            priceLabel.text = "79389.12"
            changeLabel.text = "+12.35%"
        }
    }
    
    // MARK: - life cycle
    static func generateCell(tableView: UITableView) -> BCTickerCoinCell {
        let reuseIdentifier = String(describing: self)
        var cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier) as? BCTickerCoinCell
        if cell == nil {
            cell = BCTickerCoinCell(style: UITableViewCellStyle.default, reuseIdentifier: reuseIdentifier)
        }
        assert(cell != nil, "cell can't be nil")
        
        return cell!
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupSubviews()
        setupLayouts()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - event response
    
    // MARK: - public methods
    
    // MARK: - private methods
    
    private func setupSubviews() {
        contentView.addSubview(iconView)
        contentView.addSubview(titleLabel)
        contentView.addSubview(subtitleLabel)
        contentView.addSubview(priceLabel)
        contentView.addSubview(changeLabel)
    }
    
    private func setupLayouts() {
        iconView.snp.makeConstraints { (make) in
            make.leading.equalTo(16)
            make.width.height.equalTo(33)
            make.top.equalTo(8)
            make.bottom.equalTo(-8)
        }
        
        titleLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(iconView.snp.trailing).offset(8)
            make.top.equalTo(6)
        }
        
        subtitleLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(titleLabel)
            make.top.equalTo(titleLabel.snp.bottom)
        }
        
        priceLabel.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
        }
        
        changeLabel.snp.makeConstraints { (make) in
            make.trailing.equalTo(-16)
            make.top.equalTo(10)
            make.bottom.equalTo(-10)
            make.width.equalTo(77)
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        iconView.layer.cornerRadius = min(iconView.width, iconView.height) / 2
    }
    
    // MARK: - UI property
    fileprivate lazy var iconView: UIImageView = {
        let view = UIImageView()
        view.clipsToBounds = true
        return view
    }()
    
    fileprivate lazy var titleLabel: UILabel = {
        let view = UILabel()
        view.font = UIFont.bold(size: 16)
        view.textColor = UIColor.title
        return view
    }()
    
    fileprivate lazy var subtitleLabel: UILabel = {
        let view = UILabel()
        view.font = UIFont.normal(size: 10)
        view.textColor = UIColor.disable
        return view
    }()
    
    fileprivate lazy var priceLabel: UILabel = {
        let view = UILabel()
        view.font = UIFont.bold(size: 16)
        view.textColor = UIColor.lightGreen
        return view
    }()
    
    fileprivate lazy var changeLabel: RZEdgeLabel = {
        let view = RZEdgeLabel()
        view.font = UIFont.bold(size: 14)
        view.textColor = UIColor.white
        view.backgroundColor = UIColor.lightGreen
        view.textAlignment = NSTextAlignment.right
        view.layer.cornerRadius = 2
        view.clipsToBounds = true
        return view
    }()
}
