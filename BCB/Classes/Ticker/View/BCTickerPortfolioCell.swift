//
//  BCTickerPortfolioCell.swift
//  BCB
//
//  Created by Zrocky on 2018/4/9.
//  Copyright © 2018年 zrocky. All rights reserved.
//

import UIKit

class BCTickerPortfolioCell: UITableViewCell {
    // MARK: - property
    var data: [String: Any]? {
        didSet {
            titleLabel.text = "小步快跑组合"
            subtitleLabel.text = "ZH349729"
            changeLabel.text = "+22.35%"
        }
    }
    
    // MARK: - life cycle
    static func generateCell(tableView: UITableView) -> BCTickerPortfolioCell {
        let reuseIdentifier = String(describing: self)
        var cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier) as? BCTickerPortfolioCell
        if cell == nil {
            cell = BCTickerPortfolioCell(style: UITableViewCellStyle.default, reuseIdentifier: reuseIdentifier)
        }
        assert(cell != nil, "cell can't be nil")
        
        return cell!
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupSubviews()
        setupLayouts()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - event response
    
    // MARK: - public methods
    
    // MARK: - private methods
    
    private func setupSubviews() {
        contentView.addSubview(titleLabel)
        contentView.addSubview(subtitleLabel)
        contentView.addSubview(changeLabel)
    }
    
    private func setupLayouts() {
        titleLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(16)
            make.top.equalTo(6)
        }
        
        subtitleLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(titleLabel)
            make.top.equalTo(titleLabel.snp.bottom)
        }
        
        changeLabel.snp.makeConstraints { (make) in
            make.trailing.equalTo(-16)
            make.top.equalTo(10)
            make.bottom.equalTo(-10)
            make.width.equalTo(77)
        }
    }

    // MARK: - UI property
    fileprivate lazy var titleLabel: UILabel = {
        let view = UILabel()
        view.font = UIFont.bold(size: 16)
        view.textColor = UIColor.title
        return view
    }()
    
    fileprivate lazy var subtitleLabel: UILabel = {
        let view = UILabel()
        view.font = UIFont.normal(size: 10)
        view.textColor = UIColor.disable
        return view
    }()
    
    fileprivate lazy var changeLabel: RZEdgeLabel = {
        let view = RZEdgeLabel()
        view.font = UIFont.bold(size: 14)
        view.textColor = UIColor.white
        view.backgroundColor = UIColor.lightGreen
        view.textAlignment = NSTextAlignment.right
        view.layer.cornerRadius = 2
        view.clipsToBounds = true
        return view
    }()
}

