//
//  BCTickerPortfolioDetailController.swift
//  BCB
//
//  Created by Zrocky on 2018/4/18.
//  Copyright © 2018年 zrocky. All rights reserved.
//

import UIKit

class BCTickerPortfolioDetailController: UIViewController {
    // MARK: - property
    fileprivate var selectedCategoryMenuIndex: Int = 0
    fileprivate var tableviewContentOffSetYs: [CGFloat] = []
    fileprivate var tableviewCellHightDicts: [[Int: CGFloat]] = [[:], [:], [:], [:], [:]]
    
    // MARK: - life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSubviews()
        setupLayouts()
        
        tableView.tableHeaderView = headerView
        tableView.layoutIfNeeded()
        headerView.data = [:]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setupNavi()
    }
    
    private func setupNavi() {
        navigationItem.title = "平衡投资组合"
    }
    
    // MARK: - event response
    
    // MARK: - public methods
    
    // MARK: - private methods
    private func setupSubviews() {
        view.addSubview(tableView)
    }
    
    private func setupLayouts() {
        tableView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        guard tableviewContentOffSetYs.count == categoryMenu.items.count else {
            for _ in 0..<categoryMenu.items.count {
                tableviewContentOffSetYs.append(tableView.contentOffset.y)
            }
            return
        }
    }
    
    // MARK: - UI property
    fileprivate lazy var tableView: UITableView = {
        let view = UITableView()
        view.separatorStyle = UITableViewCellSeparatorStyle.none
        view.backgroundColor = UIColor.background
        view.allowsSelection = false
        view.dataSource = self
        view.delegate = self
        return view
    }()
    
    fileprivate lazy var headerView: BCTickerPortfolioDetailHeaderView = {
        let view = BCTickerPortfolioDetailHeaderView(frame: CGRect(x: 0, y: 0, width: 0, height: 180))
        return view
    }()
    
    fileprivate lazy var categoryMenu: RZCategoryMenu = {
        let view = RZCategoryMenu(items: ["作者思路", "所有讨论"])
        view.backgroundColor = UIColor.white
        view.delegate = self
        return view
    }()
}

extension BCTickerPortfolioDetailController: RZCategoryMenuDelegate {
    func categoryMenu(_ menu: RZCategoryMenu, selectIndex: Int) {
        let topOffSetY = (tableView.tableHeaderView?.height ?? 0) - Layout.top
        tableviewContentOffSetYs[selectedCategoryMenuIndex] = tableView.contentOffset.y
        selectedCategoryMenuIndex = selectIndex
        printLog("previous offsets \(tableviewContentOffSetYs)")
        printLog("tableview contentOffSet \(tableView.contentOffset.y)")
        if tableView.contentOffset.y >= topOffSetY && tableviewContentOffSetYs[selectedCategoryMenuIndex] < topOffSetY {
            // 当前category的列表偏移量已经到了顶部，将其它category列表中未到顶部的，全部置顶
            tableviewContentOffSetYs[selectedCategoryMenuIndex] = topOffSetY
        }else if tableView.contentOffset.y < topOffSetY {
            // 当前category的列表未到顶部，将所有category列表，均按当前列表偏移量做偏移
            let contentOffSetY = tableView.contentOffset.y > -Layout.top ? tableView.contentOffset.y : -Layout.top
            tableviewContentOffSetYs[selectedCategoryMenuIndex] = contentOffSetY
        }
        printLog("after offsets \(tableviewContentOffSetYs)")
        tableView.reloadData()
        tableView.layoutIfNeeded()
        tableView.setContentOffset(CGPoint(x: 0, y: tableviewContentOffSetYs[selectedCategoryMenuIndex]), animated: false)
    }
}
extension BCTickerPortfolioDetailController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == tableView {
            printLog("scroll view contentoffsetY \(scrollView.contentOffset.y)")
        }
    }
}

// MARK: - UITableViewDataSource, UITableViewDelegate
extension BCTickerPortfolioDetailController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch selectedCategoryMenuIndex {
        case 0:
            return 50
        case 1:
            return 20
        default:
            break
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch selectedCategoryMenuIndex {
        case 0:
            let cell = BCMomentsDiscussCell.generateCell(tableView: tableView)
            cell.data = ["BCOrder": indexPath.row]
            return cell
        case 1:
            let cell = BCMomentsDiscussCell.generateCell(tableView: tableView)
            cell.data = ["BCOrder": indexPath.row]
            return cell
        default:
            break
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        tableviewCellHightDicts[selectedCategoryMenuIndex][indexPath.row] = cell.height
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if let height = tableviewCellHightDicts[selectedCategoryMenuIndex][indexPath.row] {
            return height
        }
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return categoryMenu
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //        tableView.deselectRow(at: indexPath, animated: true)
        //
        //        let vc = BCMomentsDetailController()
        //        vc.type = BCMomentsDetailControllerType.news
        //        vc.hidesBottomBarWhenPushed = true
        //        navigationController?.pushViewController(vc, animated: true)
    }
}
