//
//  BCTickerPortfolioDetailHeaderView.swift
//  BCB
//
//  Created by Zrocky on 2018/4/18.
//  Copyright © 2018年 zrocky. All rights reserved.
//

import UIKit

class BCTickerPortfolioDetailHeaderView: UIView {
    // MARK: - property
    var data: [String: Any]? {
        didSet {
            totalRevenueLabel.text = "+21.27%"
            followLabel.text = "5人关注"
        }
    }
    
    // MARK: - life cycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupSubviews()
        setupLayouts()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - event response
    
    // MARK: - public methods
    
    // MARK: - private methods
    private func setupSubviews() {
        addSubview(bgView)
        addSubview(totalRevenueTitleLabel)
        addSubview(totalRevenueLabel)
        addSubview(followLabel)
        addSubview(lineView)
    }
    
    private func setupLayouts() {
        bgView.snp.makeConstraints { (make) in
            make.top.bottom.leading.trailing.equalToSuperview()
        }
        
        totalRevenueTitleLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(16)
            make.top.equalTo(20)
        }
        
        totalRevenueLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(16)
            make.top.equalTo(totalRevenueTitleLabel.snp.bottom)
        }
        
        followLabel.snp.makeConstraints { (make) in
            make.trailing.equalTo(-16)
            make.top.equalTo(totalRevenueTitleLabel)
        }
        
        lineView.snp.makeConstraints { (make) in
            make.top.equalTo(totalRevenueLabel.snp.bottom).offset(6)
            make.height.equalTo(1)
            make.leading.equalTo(16)
            make.trailing.equalTo(-16)
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        bgView.drawGradient(colors: [UIColor.darkPurple, UIColor.darkBlue], startPoint: CGPoint(x: 0, y: 0), endPoint: CGPoint(x: 0, y: 1))
    }
    
    // MARK: - UI property
    fileprivate lazy var bgView: UIView = {
        let view = UIView()
        return view
    }()
    
    fileprivate lazy var totalRevenueTitleLabel: UILabel = {
        let view = UILabel()
        view.textColor = UIColor.white
        view.font = UIFont.normal(size: 14)
        view.text = "总收益"
        return view
    }()
    
    fileprivate lazy var totalRevenueLabel: UILabel = {
        let view = UILabel()
        view.textColor = UIColor.white
        view.font = UIFont.bold(size: 40)
        return view
    }()
    
    fileprivate lazy var followLabel: UILabel = {
        let view = UILabel()
        view.textColor = UIColor.white
        view.font = UIFont.normal(size: 14)
        view.textAlignment = NSTextAlignment.center
        return view
    }()
    
    fileprivate lazy var lineView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white.alpha(0.2)
        return view
    }()
}
