//
//  BCTickerCoinDetailToolbarButton.swift
//  BCB
//
//  Created by Zrocky on 2018/4/17.
//  Copyright © 2018年 zrocky. All rights reserved.
//

import UIKit

struct BCTickerCoinDetailToolbarItem {
    var icon: UIImage?
    var title: String = ""
    init(icon: UIImage, title: String) {
        self.icon = icon
        self.title = title
    }
}

class BCTickerCoinDetailToolbarButton: UIButton {
    // MARK: - property
    var item: BCTickerCoinDetailToolbarItem? {
        didSet {
            iconView.image = item?.icon
            nameLabel.text = item?.title
        }
    }
    
    // MARK: - life cycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupSubviews()
        setupLayouts()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - event response
    
    // MARK: - public methods
    
    // MARK: - private methods
    private func setupSubviews() {
        addSubview(iconView)
        addSubview(nameLabel)
    }
    
    private func setupLayouts() {
        iconView.snp.makeConstraints { (make) in
            make.top.equalTo(2)
            make.centerX.equalToSuperview()
            make.width.height.equalTo(21)
        }
        
        nameLabel.snp.makeConstraints { (make) in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(iconView.snp.bottom)
        }
    }
    
    // MARK: - UI property
    fileprivate lazy var iconView: UIImageView = {
        let view = UIImageView()
        view.contentMode = UIViewContentMode.scaleAspectFit
        return view
    }()
    
    fileprivate lazy var nameLabel: UILabel = {
        let view = UILabel()
        view.textColor = UIColor.subTitle
        view.font = UIFont.normal(size: 10)
        view.textAlignment = NSTextAlignment.center
        return view
    }()
}
