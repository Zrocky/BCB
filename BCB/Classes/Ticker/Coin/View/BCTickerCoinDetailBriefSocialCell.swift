//
//  BCTickerCoinDetailBriefSocialCell.swift
//  BCB
//
//  Created by Zrocky on 2018/4/17.
//  Copyright © 2018年 zrocky. All rights reserved.
//

import UIKit

class BCTickerCoinDetailBriefSocialCell: UICollectionViewCell {
    // MARK: - property
    var data: [String: Any]? {
        didSet {
            iconView.setImage(url: "http://logos-download.com/wp-content/uploads/2016/02/Twitter_logo_bird_transparent_png-700x568.png")
            valueLabel.text = "114,406"
        }
    }
    
    // MARK: - life cycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.white
        setupSubviews()
        setupLayouts()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - event response
    
    // MARK: - public methods
    
    // MARK: - private methods
    private func setupSubviews() {
        contentView.addSubview(bgView)
        contentView.addSubview(iconView)
        contentView.addSubview(valueLabel)
    }
    
    private func setupLayouts() {
        bgView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
            make.width.equalTo((Layout.width-3)/4.0)
            make.height.equalTo(64)
        }
        
        iconView.snp.makeConstraints { (make) in
            make.top.equalTo(16)
            make.width.height.equalTo(20)
            make.centerX.equalToSuperview()
        }
        
        valueLabel.snp.makeConstraints { (make) in
            make.top.equalTo(iconView.snp.bottom).offset(2)
            make.leading.trailing.equalToSuperview()
        }
    }
    
    // MARK: - UI property
    fileprivate lazy var bgView: UIView = {
        let view = UIView()
        return view
    }()
    
    fileprivate lazy var iconView: UIImageView = {
        let view = UIImageView()
        view.contentMode = UIViewContentMode.scaleAspectFit
        return view
    }()
    
    fileprivate lazy var valueLabel: UILabel = {
        let view = UILabel()
        view.textAlignment = NSTextAlignment.center
        view.textColor = UIColor.subTitle
        view.font = UIFont.normal(size: 12)
        return view
    }()
}
