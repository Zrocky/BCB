//
//  BCTickerCoinDetailToolbar.swift
//  BCB
//
//  Created by Zrocky on 2018/4/17.
//  Copyright © 2018年 zrocky. All rights reserved.
//

import UIKit

class BCTickerCoinDetailToolbar: UIView {
    // MARK: - property
    private(set) var items: [BCTickerCoinDetailToolbarItem] = []
    fileprivate var btns: [BCTickerCoinDetailToolbarButton] = []
    fileprivate var lines: [UIView] = []
    
    // MARK: - life cycle
    
    /// initialize
    ///
    /// - Parameter items: [BCTickerCoinDetailToolbarItem]
    required init(items: [BCTickerCoinDetailToolbarItem]) {
        super.init(frame: CGRect.zero)
        
        self.items = items
        guard items.count > 0 else { return }
        for item in items {
            setupBtn(item: item)
        }
        for _ in 0..<items.count {
            let view = UIView()
            view.backgroundColor = UIColor.background
            view.tag = lines.count
            lines.append(view)
        }
        
        setupSubviews()
        setupLayouts()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - event response
    func btnClick(_ btn: BCTickerCoinDetailToolbarButton) {
//        delegate?.columnNameView(view: self, didClick: items[btn.tag])
    }
    
    // MARK: - public methods
    
    // MARK: - private methods
    fileprivate func setupBtn(item: BCTickerCoinDetailToolbarItem) {
        let btn = BCTickerCoinDetailToolbarButton(type: .custom)
        btn.setBackgroundColor(UIColor.white, for: UIControlState.normal)
        btn.item = item
        btn.addTarget(self, action: #selector(btnClick(_:)), for: .touchUpInside)
        btn.tag = btns.count
        btns.append(btn)
    }
    
    private func setupSubviews() {
        for btn in btns {
            addSubview(btn)
        }
        for line in lines {
            addSubview(line)
        }
    }
    
    private func setupLayouts() {
        for btn in btns {
            let hasPreBtn = btns.indices.contains(btn.tag - 1)
            btn.snp.makeConstraints({ (make) in
                make.top.bottom.equalToSuperview()
                make.width.equalToSuperview().multipliedBy(1/btns.count.float())
                if hasPreBtn {
                    let preBtn = btns[btn.tag - 1]
                    make.leading.equalTo(preBtn.snp.trailing)
                }else {
                    make.leading.equalToSuperview()
                }
            })
        }
        
        for line in lines {
            line.snp.makeConstraints { (make) in
                make.height.equalToSuperview().multipliedBy(0.5)
                make.width.equalTo(1)
                make.centerY.equalToSuperview()
                make.centerX.equalTo(btns[line.tag].snp.trailing)
            }
        }
    }
    // MARK: - UI property
}
