//
//  BCTickerCoinDetailDataView.swift
//  BCB
//
//  Created by Zrocky on 2018/4/16.
//  Copyright © 2018年 zrocky. All rights reserved.
//

import UIKit

class BCTickerCoinDetailDataView: UIView {
    // MARK: - property
    var data: [String: Any]? {
        didSet {
            infoView.data = [:]
        }
    }
    
    // MARK: - life cycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupSubviews()
        setupLayouts()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - event response
    
    // MARK: - public methods
    
    // MARK: - private methods
    private func setupSubviews() {
        addSubview(infoView)
        addSubview(headerView)
    }
    
    private func setupLayouts() {
        infoView.snp.makeConstraints { (make) in
            make.leading.trailing.top.equalToSuperview()
            make.height.equalTo(40)
        }
        
        headerView.snp.makeConstraints { (make) in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(infoView.snp.bottom).offset(8)
            make.height.equalTo(34)
        }
    }
    
    // MARK: - UI property
    fileprivate lazy var infoView: BCTickerCoinDetailInfoView = {
        let view = BCTickerCoinDetailInfoView()
        view.backgroundColor = UIColor.white
        return view
    }()
    
    fileprivate lazy var headerView: RZCategoryMenu = {
        let view = RZCategoryMenu(items: ["K线", "资金流入", "交易量", "持有比例"])
        view.backgroundColor = UIColor.white
        view.delegate = self
        return view
    }()
}

extension BCTickerCoinDetailDataView: RZCategoryMenuDelegate {
    func categoryMenu(_ menu: RZCategoryMenu, selectIndex: Int) {
        
    }
}
