//
//  BCTickerCoinDetailBriefDescCell.swift
//  BCB
//
//  Created by Zrocky on 2018/4/17.
//  Copyright © 2018年 zrocky. All rights reserved.
//

import UIKit

class BCTickerCoinDetailBriefDescCell: UITableViewCell {
    // MARK: - property
    var data: [String: Any]? {
        didSet {
            descLabel.text = "主要特色：虚拟币始创者，受众最广，被信任最高"
        }
    }
    
    // MARK: - life cycle
    static func generateCell(tableView: UITableView) -> BCTickerCoinDetailBriefDescCell {
        let reuseIdentifier = String(describing: self)
        var cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier) as? BCTickerCoinDetailBriefDescCell
        if cell == nil {
            cell = BCTickerCoinDetailBriefDescCell(style: UITableViewCellStyle.default, reuseIdentifier: reuseIdentifier)
        }
        assert(cell != nil, "cell can't be nil")
        
        return cell!
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupSubviews()
        setupLayouts()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - event response
    
    // MARK: - public methods
    
    // MARK: - private methods
    private func setupSubviews() {
        contentView.addSubview(descLabel)
    }
    
    private func setupLayouts() {
        descLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(16)
            make.trailing.equalTo(-16)
            make.top.equalTo(8)
            make.bottom.equalTo(-8).priority(900)
        }
    }
    // MARK: - UI property
    fileprivate lazy var descLabel: UILabel = {
        let view = UILabel()
        view.textColor = UIColor.title
        view.font = UIFont.normal(size: 14)
        view.numberOfLines = 0
        return view
    }()

}
