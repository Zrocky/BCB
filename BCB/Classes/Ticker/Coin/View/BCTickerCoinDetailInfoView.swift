//
//  BCTickerCoinDetailInfoView.swift
//  BCB
//
//  Created by Zrocky on 2018/4/16.
//  Copyright © 2018年 zrocky. All rights reserved.
//

import UIKit

class BCTickerCoinDetailInfoView: UIView {
    // MARK: - property
    var data: [String: Any]? {
        didSet {
            priceLabel.text = "￥57370.25"
            changeLabel.text = "+1.38%(今日)"
        }
    }
    
    // MARK: - life cycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupSubviews()
        setupLayouts()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - event response
    
    // MARK: - public methods
    
    // MARK: - private methods
    private func setupSubviews() {
        addSubview(priceLabel)
        addSubview(changeLabel)
        addSubview(exchangeBtn)
    }
    
    private func setupLayouts() {
        priceLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(16)
            make.centerY.equalToSuperview()
        }
        
        changeLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(priceLabel.snp.trailing).offset(8)
            make.centerY.equalToSuperview()
        }
        
        exchangeBtn.snp.makeConstraints { (make) in
            make.top.bottom.trailing.equalToSuperview()
            make.width.equalTo(98)
        }
    }
    
    // MARK: - UI property
    fileprivate lazy var priceLabel: UILabel = {
        let view = UILabel()
        view.textColor = UIColor.lightGreen
        view.font = UIFont.bold(size: 24)
        return view
    }()
    
    fileprivate lazy var changeLabel: UILabel = {
        let view = UILabel()
        view.textColor = UIColor.lightGreen
        view.font = UIFont.normal(size: 12)
        return view
    }()
    
    fileprivate lazy var exchangeBtn: UIButton = {
        let view = UIButton()
        view.setBackgroundColor(UIColor.darkPurple, for: UIControlState.normal)
        view.setBackgroundColor(UIColor.darkPurple.alpha(0.8), for: UIControlState.highlighted)
        view.setImage(#imageLiteral(resourceName: "Exchange"), for: UIControlState.normal)
        view.imageView?.contentMode = UIViewContentMode.scaleAspectFit
        view.imageEdgeInsets = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 3)
        view.titleLabel?.font = UIFont.normal(size: 12)
        view.setTitle("平台价格", for: UIControlState.normal)
        view.layer.shadowColor = UIColor.darkPurple.cgColor
        view.layer.shadowOffset = CGSize(width: 0, height: 0)
        view.layer.shadowOpacity = 1
        return view
    }()
}
