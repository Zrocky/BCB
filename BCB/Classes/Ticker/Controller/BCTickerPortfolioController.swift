//
//  BCTickerPortfolioController.swift
//  BCB
//
//  Created by Zrocky on 2018/4/2.
//  Copyright © 2018年 zrocky. All rights reserved.
//

import UIKit

class BCTickerPortfolioController: UIViewController {
    // MARK: - property
    
    // MARK: - life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.background
        
        setupSubviews()
        setupLayouts()
        
        if let firstVC = childContentControllers.first {
            addContentControler(firstVC, to: contentView)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setupNavi()
    }
    
    private func setupNavi() {
        
    }
    
    // MARK: - event response
    
    // MARK: - public methods
    
    // MARK: - private methods
    private func setupSubviews() {
        view.addSubview(categoryMenu)
        view.addSubview(contentView)
    }
    
    private func setupLayouts() {
        categoryMenu.snp.makeConstraints { (make) in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(Layout.top)
            make.height.equalTo(40)
        }
        
        contentView.snp.makeConstraints { (make) in
            make.top.equalTo(categoryMenu.snp.bottom)
            make.leading.trailing.bottom.equalToSuperview()
        }
    }
    
    // MARK: - UI property
    fileprivate lazy var categoryMenu: RZCategoryMenu = {
        let view = RZCategoryMenu(items: ["全部", "我的", "收益榜"])
        view.backgroundColor = UIColor.white
        view.delegate = self
        return view
    }()
    
    fileprivate lazy var childContentControllers = [BCTickerPortfolioAllController(),
                                                    BCTickerPortfolioMineController(),
                                                    BCTickerPortfolioRankController()]
    
    fileprivate lazy var contentView: UIView = {
        let view = UIView()
        return view
    }()
    
}

extension BCTickerPortfolioController: RZCategoryMenuDelegate {
    func categoryMenu(_ menu: RZCategoryMenu, selectIndex: Int) {
        if let preVC = childViewControllers.first {
            removeContentController(preVC)
        }
        if childContentControllers.indices.contains(selectIndex) {
            addContentControler(childContentControllers[selectIndex], to: contentView)
        }
    }
}
