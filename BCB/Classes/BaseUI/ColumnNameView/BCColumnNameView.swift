//
//  BCColumnNameView.swift
//  BCB
//
//  Created by Zrocky on 2018/4/2.
//  Copyright © 2018年 zrocky. All rights reserved.
//

import UIKit

protocol BCColumnNameViewDelegate: class {
    func columnNameView(view: BCColumnNameView, didClick item: BCColumnNameViewItem)
}

enum BCColumnNameViewStyle {
    case `default`
    case submenu
    case filter
}

struct BCColumnNameViewItem {
    var title: String = ""
    var subtitle: String = ""
    var style: BCColumnNameViewStyle = BCColumnNameViewStyle.default
    var isSmaller: Bool = false
    
    init(title: String,
         subtitle: String = "",
         style: BCColumnNameViewStyle = BCColumnNameViewStyle.default,
         isSmaller: Bool = false) {
        self.title = title
        self.subtitle = subtitle
        self.style = style
        self.isSmaller = isSmaller
    }
}

class BCColumnNameView: UITableViewHeaderFooterView {
    // MARK: - property
    var delegate: BCColumnNameViewDelegate?
    
    fileprivate var items: [BCColumnNameViewItem] = []
    fileprivate var btns: [BCColumnNameViewButton] = []
    
    // MARK: - life cycle
    
    /// initialize
    ///
    /// - Parameter items: [BCColumnNameViewItem]
    required init(items: [BCColumnNameViewItem]) {
        let reuseIdentifier = String(describing: BCColumnNameView.self)
        super.init(reuseIdentifier: reuseIdentifier)
        
        self.items = items
        guard items.count > 0 else { return }
        for item in items {
            setupBtn(item: item)
        }
        let firstBtn = btns.first
        let lastBtn = btns.last
        firstBtn?.contentHorizontalAlignment = UIControlContentHorizontalAlignment.left
        firstBtn?.contentEdgeInsets = UIEdgeInsetsMake(0, 16, 0, 0)
        lastBtn?.contentHorizontalAlignment = UIControlContentHorizontalAlignment.right
        lastBtn?.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 16)
        
        setupSubviews()
        setupLayouts()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - event response
    func btnClick(_ btn: BCColumnNameViewButton) {
        delegate?.columnNameView(view: self, didClick: items[btn.tag])
    }
    
    // MARK: - public methods
    
    // MARK: - private methods
    fileprivate func setupBtn(item: BCColumnNameViewItem) {
        let mainFont = UIFont.normal(size: item.isSmaller ? 12 : 14)
        let subFont = UIFont.normal(size: item.isSmaller ? 8 : 10)
        let btn = BCColumnNameViewButton(type: .custom)
        let title = "\(item.title)\(item.subtitle)"
        let titleAttrString = title.convertAttributeString(substring: item.subtitle,
                                                           mainFont: mainFont,
                                                           mainColor: UIColor.subTitle,
                                                           subFont: subFont,
                                                           subColor: UIColor.disable)
        btn.setAttributedTitle(titleAttrString, for: UIControlState.normal)
        switch item.style {
        case BCColumnNameViewStyle.submenu:
            btn.setImage(#imageLiteral(resourceName: "FilterItem"), for: UIControlState.normal)
        case BCColumnNameViewStyle.filter:
            btn.setImage(#imageLiteral(resourceName: "GroupBY"), for: UIControlState.normal)
        default:
            break
        }
        btn.addTarget(self, action: #selector(btnClick(_:)), for: .touchUpInside)
        btn.tag = btns.count
        btns.append(btn)
    }
    
    private func setupSubviews() {
        contentView.backgroundColor = UIColor.white
        for btn in btns {
            contentView.addSubview(btn)
        }
        
        contentView.addSubview(lineView)
    }
    
    private func setupLayouts() {
        for btn in btns {
            let hasPreBtn = btns.indices.contains(btn.tag - 1)
            btn.snp.makeConstraints({ (make) in
                make.top.bottom.equalToSuperview()
                make.width.equalToSuperview().multipliedBy(1/btns.count.float())
                if hasPreBtn {
                    let preBtn = btns[btn.tag - 1]
                    make.leading.equalTo(preBtn.snp.trailing)
                }else {
                    make.leading.equalToSuperview()
                }
            })
        }
        
        lineView.snp.makeConstraints { (make) in
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(1/UIScreen.main.scale)
            make.bottom.equalToSuperview()
        }
    }
    // MARK: - UI property
    
    fileprivate lazy var lineView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.background
        return view
    }()
}
