//
//  BCNavigationController.swift
//  BCB
//
//  Created by Zrocky on 2018/3/28.
//  Copyright © 2018年 zrocky. All rights reserved.
//

import UIKit

class BCNavigationController: UINavigationController {
    override init(rootViewController: UIViewController) {
        super.init(rootViewController: rootViewController)
        
        initializeNavigationBar()
//        navigationBar.layer.shadowColor = UIColor.shadow.cgColor
//        navigationBar.layer.shadowOpacity = 1
//        navigationBar.layer.shadowOffset = CGSize(width: 0, height: 2)
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - private method
    
    /// initialize navigationbar appearance
    func initializeNavigationBar() {
        let appearance = UINavigationBar.appearance(whenContainedInInstancesOf: [BCNavigationController.self])
        appearance.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.title,
                                          NSFontAttributeName: UIFont.bold(size: 18)]
        appearance.setBackgroundImage(UIImage(color: UIColor.barBackground), for: UIBarMetrics.default)
        appearance.shadowImage = UIImage(color: UIColor.white)
        appearance.tintColor = UIColor.subTitle
    }
    
}
