//
//  RZTextSegmentControl.swift
//  BCB
//
//  Created by Zrocky on 2018/3/28.
//  Copyright © 2018年 zrocky. All rights reserved.
//

import UIKit

protocol RZTextSegmentControlDelegate: class {
    func segmentControl(_ segment: RZTextSegmentControl, selectIndex: Int)
}

class RZTextSegmentControl: UIView {
    // MARK: - property
    var delegate: RZTextSegmentControlDelegate?
    var selectedSegmentIndex: Int = -1
    override var intrinsicContentSize: CGSize {
        return frame.size
    }
    
    fileprivate var btns: [UIButton] = []
    
    // MARK: - life cycle
    required init(items: [String]) {
        super.init(frame: CGRect.zero)
        
        guard items.count > 0 else { return }
        for item in items {
            setupBtn(item: item)
        }
        setupSubviews()
        setupLayouts()
        btnClick(btns.first!)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - delegate
    
    // MARK: - event response
    func btnClick(_ btn: UIButton) {
        // when touch selected button
        guard btn.tag != selectedSegmentIndex else {
            UIView.animate(withDuration: 0.15, animations: {
                btn.transform = CGAffineTransform(scaleX: 0.95, y: 0.95)
            }, completion: { (complete) in
                UIView.animate(withDuration: 0.15, animations: {
                    btn.transform = CGAffineTransform(scaleX: 1.05, y: 1.05)
                }, completion: { (complete) in
                    btn.transform = CGAffineTransform.identity
                })
            })
            return
        }
        
        // preview button animation
        if btns.indices.contains(selectedSegmentIndex) {
            let preBtn = btns[selectedSegmentIndex]
            UIView.animate(withDuration: 0.25, animations: {
                preBtn.titleLabel?.font = UIFont.bold(size: 14)
            }) { (complete) in
                preBtn.isSelected = false
            }
        }
        
        // current touch button animation
        selectedSegmentIndex = btn.tag
        btn.isSelected = true
        UIView.animate(withDuration: 0.25, animations: {
            btn.titleLabel?.font = UIFont.bold(size: 18)
        })
            
        delegate?.segmentControl(self, selectIndex: btn.tag)
    }
    
    // MARK: - public methods
    func setSelectSegmentControl(index: Int) {
        guard btns.indices.contains(index) else { return }
        let btn = btns[index]
        btnClick(btn)
    }
    
    // MARK: - private methods
    fileprivate func setupBtn(item: String) {
        let btn = UIButton(type: .custom)
        btn.setTitle(item, for: .normal)
        btn.setTitleColor(UIColor.disable, for: UIControlState.normal)
        btn.setTitleColor(UIColor.title, for: UIControlState.selected)
        btn.setTitleColor(UIColor.title, for: [UIControlState.selected, UIControlState.highlighted])
        btn.titleLabel?.font = UIFont.bold(size: 14)
        btn.contentVerticalAlignment = .bottom
        btn.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 3, 0)
        btn.addTarget(self, action: #selector(btnClick(_:)), for: .touchUpInside)
        btn.tag = btns.count
        btns.append(btn)
    }
    
    private func setupSubviews() {
        for btn in btns {
            addSubview(btn)
        }
    }
    
    private func setupLayouts() {
        for btn in btns {
            let hasPreBtn = btns.indices.contains(btn.tag - 1)
            btn.snp.makeConstraints({ (make) in
                make.top.bottom.equalToSuperview()
                make.width.equalToSuperview().multipliedBy(1/btns.count.float())
                if hasPreBtn {
                    let preBtn = btns[btn.tag - 1]
                    make.leading.equalTo(preBtn.snp.trailing)
                }else {
                    make.leading.equalToSuperview()
                }
            })
        }
    }
}
