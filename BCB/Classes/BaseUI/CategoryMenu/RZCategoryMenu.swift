//
//  RZCategoryMenu.swift
//  BCB
//
//  Created by Zrocky on 2018/4/2.
//  Copyright © 2018年 zrocky. All rights reserved.
//

import UIKit
import SnapKit

protocol RZCategoryMenuDelegate: class {
    func categoryMenu(_ menu: RZCategoryMenu, selectIndex: Int)
}

class RZCategoryMenu: UIView {
    // MARK: - property
    var delegate: RZCategoryMenuDelegate?
    private(set) var items: [String] = []
    
    fileprivate var btns: [UIButton] = []
    fileprivate var selectedIndex: Int = 0
    fileprivate var lineCenterXConstraint: Constraint?
    
    // MARK: - life cycle
    init(items: [String]) {
        super.init(frame: CGRect.zero)
        
        guard items.count > 0 else { return }
        self.items = items
        for item in items {
            setupBtn(item: item)
        }
        
        setupSubviews()
        setupLayouts()
        btnClick(btns.first!)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - event response
    func btnClick(_ btn: UIButton) {
        let preBtn = btns[selectedIndex]
        preBtn.isSelected = false
        preBtn.titleLabel?.font = UIFont.normal(size: 16)
        btn.isSelected = true
        btn.titleLabel?.font = UIFont.bold(size: 16)
        selectedIndex = btn.tag
        lineCenterXConstraint?.deactivate()
        lineView.snp.makeConstraints { (make) in
            lineCenterXConstraint = make.centerX.equalTo(btn).constraint
        }
        delegate?.categoryMenu(self, selectIndex: btn.tag)
    }
    // MARK: - public methods
    
    // MARK: - private methods
    fileprivate func setupBtn(item: String) {
        let btn = UIButton(type: .custom)
        btn.setTitle(item, for: .normal)
        btn.setTitleColor(UIColor.title, for: UIControlState.normal)
        btn.setTitleColor(UIColor.darkBlue, for: UIControlState.selected)
        btn.setTitleColor(UIColor.darkBlue, for: [UIControlState.selected, UIControlState.highlighted])
        btn.titleLabel?.font = UIFont.normal(size: 16)
        btn.addTarget(self, action: #selector(btnClick(_:)), for: .touchUpInside)
        btn.tag = btns.count
        btns.append(btn)
    }
    
    private func setupSubviews() {
        for btn in btns {
            addSubview(btn)
        }
        addSubview(lineView)
    }
    
    private func setupLayouts() {
        for btn in btns {
            let hasPreBtn = btns.indices.contains(btn.tag - 1)
            btn.snp.makeConstraints({ (make) in
                make.top.bottom.equalToSuperview()
                make.width.equalToSuperview().multipliedBy(1/btns.count.float())
                if hasPreBtn {
                    let preBtn = btns[btn.tag - 1]
                    make.leading.equalTo(preBtn.snp.trailing)
                }else {
                    make.leading.equalToSuperview()
                }
            })
        }
        lineView.snp.makeConstraints { (make) in
            make.height.equalTo(3)
            make.width.equalTo(16)
            lineCenterXConstraint = make.centerX.equalTo(btns.first!).constraint
            make.bottom.equalToSuperview()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        lineView.layer.cornerRadius = min(lineView.width, lineView.height) / 2
    }
    
    // MARK: - UI property
    fileprivate lazy var lineView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.darkBlue
        return view
    }()
}
