//
//  BCTabBarController.swift
//  BCB
//
//  Created by Zrocky on 2018/3/27.
//  Copyright © 2018年 zrocky. All rights reserved.
//

import UIKit


class BCTabBarController: ESTabBarController {
    // MARK: - life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initializeTabBarVC()
    }
    
    // MARK: - private method
    fileprivate func initializeTabBarVC() {
        if let tabBar = tabBar as? ESTabBar {
            tabBar.itemCustomPositioning = ESTabBarItemPositioning.automatic
            tabBar.backgroundImage = UIImage(color: UIColor.barBackground)
            tabBar.shadowImage = UIImage(color: UIColor.white)
            tabBar.layer.shadowColor = UIColor.shadow.cgColor
            tabBar.layer.shadowOpacity = 1
            tabBar.layer.shadowOffset = CGSize(width: 0, height: -2)
        }
        let newsVC = generateNaviVC(rootController: BCNewsController())
        let tickerVC = generateNaviVC(rootController: BCTickerController())
        let momentsVC = generateNaviVC(rootController: BCMomentsController())
        let marketVC = generateNaviVC(rootController: BCMarketController())
        let icoVC = generateNaviVC(rootController: BCICOController())
        
        newsVC.tabBarItem = generateTabBarItem(title: "首页", image: #imageLiteral(resourceName: "News_gray"), selectedImage: #imageLiteral(resourceName: "News_black"))
        tickerVC.tabBarItem = generateTabBarItem(title: "自选", image: #imageLiteral(resourceName: "Ticker_gray"), selectedImage: #imageLiteral(resourceName: "Ticker_black"))
        momentsVC.tabBarItem = generateTabBarItem(title: "动态", image: #imageLiteral(resourceName: "Moments_gray"), selectedImage: #imageLiteral(resourceName: "Moments_black"))
        marketVC.tabBarItem = generateTabBarItem(title: "行情", image: #imageLiteral(resourceName: "Market_gray"), selectedImage: #imageLiteral(resourceName: "Market_black"))
        icoVC.tabBarItem = generateTabBarItem(title: "新币", image: #imageLiteral(resourceName: "ICO_gray"), selectedImage: #imageLiteral(resourceName: "ICO_black"))
        
        viewControllers = [newsVC, tickerVC, momentsVC, marketVC, icoVC]
    }
    
    fileprivate func generateNaviVC(rootController: UIViewController) -> UINavigationController {
        return BCNavigationController(rootViewController: rootController)
    }

    fileprivate func generateTabBarItem(title: String?, image: UIImage, selectedImage: UIImage) -> UITabBarItem {
        return ESTabBarItem(RZTabBarContentView(), title: title, image: image, selectedImage: selectedImage)
    }
}
