//
//  JZTabBarContentView.swift
//  YJZ
//
//  Created by Zrocky on 2017/6/30.
//  Copyright © 2017年 Zrocky. All rights reserved.
//

import UIKit


class RZTabBarContentView: ESTabBarItemContentView {
    var duration = 0.3
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        textColor = UIColor.disable
        highlightTextColor = UIColor.title
        renderingMode = UIImageRenderingMode.alwaysOriginal
        backdropColor = UIColor.clear
        highlightBackdropColor = UIColor.clear
        
        let transform = CGAffineTransform.identity
        imageView.transform = transform.scaledBy(x: 1.15, y: 1.15)
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func highlightAnimation(animated: Bool, completion: (() -> ())?) {
//        UIView.beginAnimations("small", context: nil)
//        UIView.setAnimationDuration(0.2)
//        let transform = imageView.transform.scaledBy(x: 0.95, y: 0.95)
//        imageView.transform = transform
//        UIView.commitAnimations()
//        completion?()
    }
    
    override func dehighlightAnimation(animated: Bool, completion: (() -> ())?) {
        UIView.beginAnimations("big", context: nil)
        UIView.setAnimationDuration(0.2)
        let transform = CGAffineTransform.identity
        imageView.transform = transform.scaledBy(x: 1.15, y: 1.15)
        UIView.commitAnimations()
        completion?()
    }
    
    override func badgeChangedAnimation(animated: Bool, completion: (() -> ())?) {
        super.badgeChangedAnimation(animated: animated, completion: nil)
        notificationAnimation()
    }
    
    func notificationAnimation() {
        let impliesAnimation = CAKeyframeAnimation(keyPath: "transform.scale")
        impliesAnimation.values = [1.0 ,1.4, 0.9, 1.15, 0.95, 1.02, 1.0]
        impliesAnimation.duration = duration * 2
        impliesAnimation.calculationMode = kCAAnimationCubic
        self.badgeView.layer.add(impliesAnimation, forKey: nil)
    }
}
