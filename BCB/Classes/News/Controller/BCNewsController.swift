//
//  BCNewsController.swift
//  BCB
//
//  Created by Zrocky on 2018/3/27.
//  Copyright © 2018年 zrocky. All rights reserved.
//

import UIKit
import Kingfisher

class BCNewsController: UIViewController {
    // MARK: - property
    
    // MARK: - life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSubviews()
        setupLayouts()
        
        if let firstVC = childContentControllers.first {
            addContentControler(firstVC, to: contentView)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setupNavi()
    }
    
    private func setupNavi() {
        navigationItem.titleView = segmentControl
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: portraitView)
    }
    
    // MARK: - event response
    
    // MARK: - public methods
    
    // MARK: - private methods
    private func setupSubviews() {
        view.addSubview(contentView)
    }
    
    private func setupLayouts() {
        portraitView.snp.makeConstraints { (make) in
            make.width.height.equalTo(34)
        }
        
        segmentControl.snp.makeConstraints { (make) in
            make.width.equalTo(115)
            make.height.equalTo(44)
        }
        
        contentView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        portraitView.layer.cornerRadius = min(portraitView.width, portraitView.height) / 2
    }
    
    // MARK: - UI property
    fileprivate lazy var segmentControl: RZTextSegmentControl = {
        let view = RZTextSegmentControl(items: ["头条", "快讯"])
        view.delegate = self
        return view
    }()
    
    fileprivate lazy var childContentControllers = [BCNewsHotController(),
                                                    BCNewRealTimeController()]
    
    fileprivate lazy var portraitView = BCUserControl.sharedInstance
    
    fileprivate lazy var contentView: UIView = {
        let view = UIView()
        return view
    }()
}


// MARK: - RZTextSegmentControlDelegate
extension BCNewsController: RZTextSegmentControlDelegate {
    func segmentControl(_ segment: RZTextSegmentControl, selectIndex: Int) {
        if let preVC = childViewControllers.first {
            removeContentController(preVC)
        }
        if childContentControllers.indices.contains(selectIndex) {
            addContentControler(childContentControllers[selectIndex], to: contentView)
        }
    }
}

