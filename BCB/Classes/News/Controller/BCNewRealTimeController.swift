//
//  BCNewRealTimeController.swift
//  BCB
//
//  Created by Zrocky on 2018/3/30.
//  Copyright © 2018年 zrocky. All rights reserved.
//

import UIKit

class BCNewRealTimeController: UIViewController {
    // MARK: - property
    
    // MARK: - life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        
        setupSubviews()
        setupLayouts()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setupNavi()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
    }
    
    private func setupNavi() {
        
    }
    
    // MARK: - event response
    
    // MARK: - public methods
    
    // MARK: - private methods
    private func setupSubviews() {
        view.addSubview(lineView)
        view.addSubview(tableView)
    }
    
    private func setupLayouts() {
        lineView.snp.makeConstraints { (make) in
            make.width.equalTo(2)
            make.bottom.equalToSuperview()
            make.top.equalTo(view.snp.centerY)
            make.centerX.equalTo(60)
        }
        
        tableView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    }
    
    // MARK: - UI property
    fileprivate lazy var lineView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.background
        return view
    }()
    
    fileprivate lazy var tableView: UITableView = {
        let view = UITableView()
        view.backgroundColor = UIColor.clear
        view.separatorStyle = UITableViewCellSeparatorStyle.none
        view.separatorInset = UIEdgeInsets.zero
        view.separatorColor = UIColor.background
        view.delegate = self
        view.dataSource = self
        view.estimatedRowHeight = 64
        view.rowHeight = UITableViewAutomaticDimension
        return view
    }()
}

// MARK: - UITableViewDataSource, UITableViewDelegate
extension BCNewRealTimeController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = BCNewsRealTimeCell.generateCell(tableView: tableView)
        cell.backgroundColor = UIColor.clear
        cell.data = [:]
        cell.isFirst = indexPath.section == 0 && indexPath.row == 0
        return cell
    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 120
//    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 32
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = BCNewsRealTimeHeaderView.generateHeader()
        header.data = [:]
        return header
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}
