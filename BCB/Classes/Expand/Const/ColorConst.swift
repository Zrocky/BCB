//
//  ColorConst.swift
//  YJZ
//
//  Created by Zrocky on 2017/6/12.
//  Copyright © 2017年 Zrocky. All rights reserved.
//

import UIKit

extension UIColor {
    /// #000000
    static let title: UIColor = RGB(0x000000)
    
    /// #555555
    static let subTitle: UIColor = RGB(0x555555)
    
    /// #7E7E7E
    static let content: UIColor = RGB(0x7E7E7E)
    
    /// #A1A1A1
    static let disable: UIColor = RGB(0xA1A1A1)
    
    /// #EFEFF4
    static let background: UIColor = RGB(0xEFEFF4)
    
    /// #FFFFFF, alpha 0.95
    static let barBackground: UIColor = RGB(0xFFFFFF).alpha(0.98)
    
    /// #000000, alpha 0.05
    static let shadow: UIColor = RGB(0x000000).alpha(0.05)
    
    /// #4A90E2
    static let darkBlue: UIColor = RGB(0x4A90E2)
    
    /// #695EBA
    static let darkPurple: UIColor = RGB(0x695EBA)
    
    /// #00AC2C
    static let lightGreen: UIColor = RGB(0x00AC2C)
    
    /// #F03034
    static let lightRed: UIColor = RGB(0xF03034)
    
    /// #2BC1CD
    static let lightCyan: UIColor = RGB(0x2BC1CD)
    
    /// #D0021B
    static let darkRed: UIColor = RGB(0xD0021B)
}
