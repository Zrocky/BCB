//
//  LayoutConst.swift
//  YJZ
//
//  Created by Zrocky on 2017/6/12.
//  Copyright © 2017年 Zrocky. All rights reserved.
//

import UIKit
import SnapKit

struct Layout {
    static let window = UIApplication.shared.keyWindow
    static let px = 1/UIScreen.main.scale
    static let width = UIScreen.main.bounds.width
    static let height = UIScreen.main.bounds.height
    
    /**
     *  StatusBar height
     */
    static var status: CGFloat {
        if AppInfo.isiPhoneX {
            return 44
        }else {
            return 20
        }
    }
    
    /**
     *  NavigationBar height
     */
    static let navi: CGFloat = 44.0
    
    /**
     *  StatusBar height + NavigationBar height
     */
    static var top: CGFloat {
        if AppInfo.isiPhoneX {
            return 88
        }else {
            return 64
        }
    }
    
    /**
     *  Bottom SafeArea
     */
    static var bottomSafe: CGFloat {
        if AppInfo.isiPhoneX {
            return 34
        }else {
            return 0
        }
    }
    
    /**
     *  TabBar height + Bottom SafeArea
     */
    static var bottom: CGFloat {
        if AppInfo.isiPhoneX {
            return 83
        }else {
            return 49
        }
    }
}

