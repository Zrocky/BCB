//
//  FontConst.swift
//  YJZ
//
//  Created by Zrocky on 2017/6/12.
//  Copyright © 2017年 Zrocky. All rights reserved.
//

import UIKit

struct FontName {
    static let normal: String = "PingFangSC-Regular"
    static let light: String = "PingFangSC-Light"
    static let bold: String = "PingFangSC-Medium"
    static let icon: String = "BCB"
}


