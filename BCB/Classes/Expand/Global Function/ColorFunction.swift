//
//  ColorFunction.swift
//  YJZ
//
//  Created by Zrocky on 2017/7/18.
//  Copyright © 2017年 Zrocky. All rights reserved.
//

import UIKit

func RGB(_ RGBValue: Int, _ alpha: CGFloat = 1.0) -> UIColor {
    return UIColor(RGB: RGBValue, alpha: alpha)
}

