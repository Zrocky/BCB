//
//  LogFunction.swift
//  YJZ
//
//  Created by Zrocky on 2017/7/28.
//  Copyright © 2017年 Zrocky. All rights reserved.
//

import Foundation

func printLog<T>(_ message: T,
               file: String = #file,
               method: String = #function,
               line: Int = #line) {
    #if DEBUG
        print("\((file as NSString).lastPathComponent)[\(line)], \(method): \(message)")
    #endif
}

