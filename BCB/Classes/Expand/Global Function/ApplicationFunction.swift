//
//  ApplicationFunction.swift
//  YJZ
//
//  Created by Zrocky on 2017/9/13.
//  Copyright © 2017年 Zrocky. All rights reserved.
//

import UIKit

class ApplicationFunction {
    static func canOpen(urlString: String) -> Bool {
        if let url = URL(string: urlString) {
            return UIApplication.shared.canOpenURL(url)
        }
        return false
    }
    
    static func call(_ phoneNumber: String) {
        let number = phoneNumber.components(separatedBy: CharacterSet.decimalDigits.inverted).joined(separator: "")
        guard let telURL = URL(string: "tel://" + number) else { return }
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(telURL, options: [:])
        } else {
            UIApplication.shared.openURL(telURL)
        }
    }
    
    static func openLink(_ webLink: String) {
        guard let openURL = URL(string: webLink) else { return }
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(openURL, options: [:])
        } else {
            UIApplication.shared.openURL(openURL)
        }
    }
}
