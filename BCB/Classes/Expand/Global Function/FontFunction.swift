//
//  FontFunction.swift
//  YJZ
//
//  Created by Zrocky on 2017/6/12.
//  Copyright © 2017年 Zrocky. All rights reserved.
//

import UIKit

extension UIFont {
    static func normal(size: CGFloat) -> UIFont {
        return UIFont(name: FontName.normal, size: size) ?? UIFont.systemFont(ofSize: size)
    }
    
    static func light(size: CGFloat) -> UIFont {
        return UIFont(name: FontName.light, size: size) ?? UIFont.systemFont(ofSize: size)
    }
    
    static func bold(size: CGFloat) -> UIFont {
        return UIFont(name: FontName.bold, size: size) ?? UIFont.systemFont(ofSize: size)
    }
    
    static func icon(size: CGFloat) -> UIFont {
        return UIFont(name: FontName.icon, size: size) ?? UIFont.systemFont(ofSize: size)
    }
}


