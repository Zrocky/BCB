//
//  AppStoreFunction.swift
//  YJZ
//
//  Created by Zrocky on 2017/11/6.
//  Copyright © 2017年 Zrocky. All rights reserved.
//

import UIKit

struct AppStoreFunction {
    /**
     *  HomePage
     */
    static let `default`: String = "https://itunes.apple.com/us/app/apple-store/id\(AppStoreConst.id)"
    
    /**
     *  Review App
     */
    static let review: String = "itms-apps://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=\(AppStoreConst.id)"
    
}
